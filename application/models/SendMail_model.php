<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class SendMail_model extends CI_Model {

    public function sendMail($data){
        $this->load->library('email');

        $this->email->from("it@grandcentralbandung.com", $data["nama_staff"]);
        $this->email->to($data["email_staff"]);

        $this->email->subject('Reminder Dokumen');
        $this->email->message($data["message"]);

        $this->email->send();
    }

}
?>