<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class master_proyek_model extends CI_Model {
 
    var $table = 'tblmenu';
    var $column_order = array('urut_menu', 'id_menu', 'nm_menu', 'level_menu', null); //set column field database for datatable orderable
    var $column_search = array('urut_menu', 'id_menu', 'nm_menu', 'level_menu'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('urut_menu' => 'asc'); // default order 
 
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
    private function _get_datatables_query(){
         
        $this->db->from($this->table);
 
        $i = 0;
        $where = '';
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $where = '('.$item." like '%".$_POST['search']['value']."%'";
                }
                else
                {
                    $where .= ' OR '.$item." like '%".$_POST['search']['value']."%'";
                }
            }
            $i++;
        }

        if(!empty($where)){
            $where .= ")";
            $this->db->where($where);
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    public function get_maxid(){
        $this->db->select_max('id_menu');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->row();
    }
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
    public function get_by_id($id){
        $this->db->from($this->table);
        $this->db->where('id_menu',$id);
        $query = $this->db->get();
 
        return $query->row();
    }
 
    public function save($data){
        $this->db->insert($this->table, $data);
        if($this->db->affected_rows()>0){
            $IDMENU = $data['id_menu'];

            $this->db->from("tbluser");
            $query = $this->db->get();
            foreach ($query->result() as $row)
            {
                $userid = $row->userid;
                $data_insert = array(
                    'userid' => $userid,
                    'id_menu' => $IDMENU
                );
                $this->db->insert("tblrole", $data_insert);
            }
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
 
    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id){
        $this->db->where('id_menu', $id);
        $this->db->delete($this->table);
        $this->db->where('id_menu', $id);
        $this->db->delete('tblrole');
    } 
}