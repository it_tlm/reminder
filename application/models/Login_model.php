<?php
	class login_model extends CI_Model {
		public function __construct(){
			$this->load->database();	
		}
		
		public function cekUser($USERID, $PASS){
			$query = $this->db->get_where('tbluser',array('userid' => trim($USERID),'lock' => 0,'userlvl >= ' => 1));
			if($query->num_rows()>0){
				$row = $query->row();
				$hash = $row->password;
				if (password_verify($PASS, $hash)) {
				    return $query;
				} else {
					$query = '';
				    return $query;
				}
			}else{
				$query = '';
				return $query;	
			}
		}

		public function getmenu($USERID)
		{
			$this->db->select('b.urut_menu');
			$this->db->from('tblrole a');
			$this->db->join('tblmenu b', 'a.id_menu = b.id_menu', 'inner');
			$this->db->where(array('a.userid' => $USERID, 'a.lread' => 1));
			$this->db->order_by('b.urut_menu', 'ASC');
			$query = $this->db->get();
			return $query;
		}
	}
?>