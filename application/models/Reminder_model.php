<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Reminder_model extends CI_Model {
 
    public function __construct(){
        parent::__construct();
        $this->load->database();
    } 

    var $table = 'tblreminder';
    var $primaryKey = 'acak_reminder';
    var $column_order = array('no_dokumen','acak_departemen','tgl_reminder','masa_berlaku','tgl_berlaku_dokumen','acak_jenis_produk','acak_jenis_dokumen','keterangan','acak_proyek'); //set column field database for datatable orderable
    var $column_search = array('no_dokumen','tbldepartement.nama_departement','tgl_reminder','masa_berlaku','tgl_berlaku_dokumen','tbljenisproduk.nama_jenis_produk','acak_jenis_dokumen','tblreminder.keterangan','tblproyek.nm_pry'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('acak_reminder' => 'asc'); // default order 

    var $fillable = [
        'acak_jenis_dok',
        'nama_jenis_dokumen',
        'keterangan',
        'acak_jenis_produk'
    ];

    public function reminderQuery(){
        $this->db->select('
        tblreminder.no_dokumen as no_dokumen,
        tbldepartement.nama_departement as acak_departemen,
        tblreminder.acak_reminder as acak_reminder,
        tblreminder.tgl_reminder as tgl_reminder,
        tblreminder.masa_berlaku as masa_berlaku,
        tblreminder.tgl_berlaku_dokumen as tgl_berlaku_dokumen,
        tbljenisproduk.nama_jenis_produk as acak_jenis_produk,
        tbljenisdokumen.nama_jenis_dok as acak_jenis_dokumen,
        tblreminder.opsi_reminder as opsi_reminder,
        tblreminder.keterangan as keterangan,
        tbluser.userid as create_by,
        tblreminder.create_date as create_date,
        tbluserapprove.nama_staff as nama_staff,
        tblproyek.nm_pry as acak_proyek,
        tblpic.acak_pic as acak_pic,
        tblpic.approve_by_pic as approve_by_pic,
        tblpic.approve_by_admin as approve_by_admin,
        tbldokumen.acak_dokumen as acak_dokumen,
        tbldokumen.path_dokumen as path_dokumen
        ');
        
        $this->db->from($this->table);
        $this->db->join('tbldepartement','tbldepartement.acak_departement = tblreminder.acak_departemen');
        $this->db->join('tbljenisproduk','tbljenisproduk.acak_jenis_produk = tblreminder.acak_jenis_produk');
        $this->db->join('tbljenisdokumen','tbljenisdokumen.acak_jenis_dok = tblreminder.acak_jenis_dokumen');
        $this->db->join('tbluser','tbluser.userid = tblreminder.create_by');
        $this->db->join('tbluserapprove','tbluserapprove.acak_user_approve = tblreminder.acak_admin');
        $this->db->join('tblproyek','tblproyek.id_pry = tblreminder.acak_proyek');
        $this->db->join('tblpic','tblpic.acak_reminder = tblreminder.acak_reminder','left');
        $this->db->join('tbldokumen','tbldokumen.acak_reminder = tblreminder.acak_reminder');

        return $this->db;
    }

    private function _get_datatables_query(){
         
        $this->db->select('
        tblreminder.no_dokumen as no_dokumen,
        tbldepartement.nama_departement as acak_departemen,
        tblreminder.acak_reminder as acak_reminder,
        tblreminder.tgl_reminder as tgl_reminder,
        tblreminder.masa_berlaku as masa_berlaku,
        tblreminder.tgl_berlaku_dokumen as tgl_berlaku_dokumen,
        tbljenisproduk.nama_jenis_produk as acak_jenis_produk,
        tbljenisdokumen.nama_jenis_dok as acak_jenis_dokumen,
        tblreminder.opsi_reminder as opsi_reminder,
        tblreminder.keterangan as keterangan,
        tbluser.userid as create_by,
        tblreminder.create_date as create_date,
        tbluserapprove.nama_staff as nama_staff,
        tblproyek.nm_pry as acak_proyek,
        tblpic.acak_pic as acak_pic,
        tblpic.approve_by_pic as approve_by_pic,
        tblpic.approve_by_admin as approve_by_admin,
        ');
        
        $this->db->from($this->table);
        $this->db->join('tbldepartement','tbldepartement.acak_departement = tblreminder.acak_departemen');
        $this->db->join('tbljenisproduk','tbljenisproduk.acak_jenis_produk = tblreminder.acak_jenis_produk');
        $this->db->join('tbljenisdokumen','tbljenisdokumen.acak_jenis_dok = tblreminder.acak_jenis_dokumen');
        $this->db->join('tbluser','tbluser.userid = tblreminder.create_by');
        $this->db->join('tbluserapprove','tbluserapprove.acak_user_approve = tblreminder.acak_admin');
        $this->db->join('tblproyek','tblproyek.id_pry = tblreminder.acak_proyek');
        $this->db->join('tblpic','tblpic.acak_reminder = tblreminder.acak_reminder','left');
        // $this->db->join('tbljenisproduk', 'tbljenisdokumen.acak_jenis_produk = tbljenisproduk.acak_jenis_produk','left');
         
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function reportReminderQuery($data){
        $tgl_end_expired=$data['tgl_end_expired'];
            $tgl_start_reminder=$data['tgl_start_reminder'];
            $tgl_end_reminder=$data['tgl_end_reminder'];
            $tgl_start_expired=$data['tgl_start_expired'];
            $status = $data['status'];
    
            $this->db->select('
            tblreminder.no_dokumen as no_dokumen,
            tbldepartement.nama_departement as acak_departemen,
            tblreminder.acak_reminder as acak_reminder,
            tblreminder.tgl_reminder as tgl_reminder,
            tblreminder.masa_berlaku as masa_berlaku,
            tblreminder.tgl_berlaku_dokumen as tgl_berlaku_dokumen,
            tbljenisproduk.nama_jenis_produk as acak_jenis_produk,
            tbljenisdokumen.nama_jenis_dok as acak_jenis_dokumen,
            tblreminder.opsi_reminder as opsi_reminder,
            tblreminder.keterangan as keterangan,
            tbluser.userid as create_by,
            tblreminder.create_date as create_date,
            tbluserapprove.nama_staff as nama_staff,
            tblproyek.nm_pry as acak_proyek,
            tblpic.acak_pic as acak_pic,
            tblpic.approve_by_pic as approve_by_pic,
            tblpic.approve_by_admin as approve_by_admin
            ');
            
            $this->db->from('tblreminder');
            $this->db->join('tbldepartement','tbldepartement.acak_departement = tblreminder.acak_departemen');
            $this->db->join('tbljenisproduk','tbljenisproduk.acak_jenis_produk = tblreminder.acak_jenis_produk');
            $this->db->join('tbljenisdokumen','tbljenisdokumen.acak_jenis_dok = tblreminder.acak_jenis_dokumen');
            $this->db->join('tbluser','tbluser.userid = tblreminder.create_by');
            $this->db->join('tbluserapprove','tbluserapprove.acak_user_approve = tblreminder.acak_admin');
            $this->db->join('tblproyek','tblproyek.id_pry = tblreminder.acak_proyek');
            $this->db->join('tblpic','tblpic.acak_reminder = tblreminder.acak_reminder','left');
            if($status == 0){
                $this->db->where(' tblpic.approve_by_pic =',NULL);
                $this->db->where(' tblpic.approve_by_admin =',NULL);
            }
    
            if($status == 1){
                $this->db->where(' tblpic.approve_by_pic !=',NULL);
                $this->db->where(' tblpic.approve_by_admin =',NULL);
            }
    
    
            if($status == 2){
                $this->db->where(' tblpic.approve_by_pic !=',NULL);
                $this->db->where(' tblpic.approve_by_admin !=',NULL);
            }
            
            $this->db->group_start();
            $this->db->or_group_start();
            // between date reminder
            $this->db->where('tblreminder.tgl_reminder >=',$tgl_start_reminder);
            $this->db->where('tblreminder.tgl_reminder <=',$tgl_end_reminder);
            $this->db->group_end();

            $this->db->or_group_start();
            // between date expired
            $this->db->where('tblreminder.tgl_berlaku_dokumen >=',$tgl_start_expired);
            $this->db->where('tblreminder.tgl_berlaku_dokumen <=',$tgl_end_expired);
            $this->db->group_end();
            $this->db->group_end();

            $query = $this->db->get();

            return $query->result();
    }

    public function sortQuery(){
        $this->db->select('
            tblreminder.no_dokumen as no_dokumen,
            tbldepartement.nama_departement as acak_departemen,
            tblreminder.acak_reminder as acak_reminder,
            tblreminder.tgl_reminder as tgl_reminder,
            tblreminder.masa_berlaku as masa_berlaku,
            tblreminder.tgl_berlaku_dokumen as tgl_berlaku_dokumen,
            tbljenisproduk.nama_jenis_produk as acak_jenis_produk,
            tbljenisdokumen.nama_jenis_dok as acak_jenis_dokumen,
            tblreminder.opsi_reminder as opsi_reminder,
            tblreminder.keterangan as keterangan,
            tbluser.userid as create_by,
            tbluser.usernm as usernm,
            tblreminder.create_date as create_date,
            tbluserapprove.nama_staff as nama_staff,
            tblproyek.nm_pry as acak_proyek,
            tblpic.acak_pic as acak_pic,
            tblpic.approve_by_pic as approve_by_pic,
            tblpic.approve_by_admin as approve_by_admin
            ');
            
            $this->db->from('tblreminder');
            $this->db->join('tbldepartement','tbldepartement.acak_departement = tblreminder.acak_departemen');
            $this->db->join('tbljenisproduk','tbljenisproduk.acak_jenis_produk = tblreminder.acak_jenis_produk');
            $this->db->join('tbljenisdokumen','tbljenisdokumen.acak_jenis_dok = tblreminder.acak_jenis_dokumen');
            $this->db->join('tbluser','tbluser.userid = tblreminder.create_by');
            $this->db->join('tbluserapprove','tbluserapprove.acak_user_approve = tblreminder.acak_admin');
            $this->db->join('tblproyek','tblproyek.id_pry = tblreminder.acak_proyek');
            $this->db->join('tblpic','tblpic.acak_reminder = tblreminder.acak_reminder','left');
            $query = $this->db->get();
            return $query->result();
    }
 
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_by_id($id){
        // $this->db->from($this->table);
        // $this->db->where($this->primaryKey,$id);

        $query = $this->reminderQuery()->where('tblreminder.acak_reminder',$id)->get();
 
        return $query->row();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
    }

    public function update($id,$data){
        $this->db->where($this->primaryKey,$id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function delete($id){
        // $hasil = 'GAGAL';
        // $this->db->where($this->primaryKey, $id);
        // $this->db->delete($this->table);
        // if($this->db->affected_rows() > 0)
        // {
        //     $hasil =  array("status" => TRUE, "msg" => 'Berhasil Hapus Data');;
        // }
        // return json_encode($hasil);
        echo "delete";
    }

    public function getLastId(){
        $last = $this->db->order_by($this->primaryKey,"desc")
        ->select("acak_reminder")
		->limit(1)
        ->from($this->table)
		->get()
		->row();

        if($last == NULL){
            $last = 0;
        }else{
            $last = $last->acak_reminder;
        }

        return $last;
    }
}
?>