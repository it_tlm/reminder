<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Master_Role_model extends CI_Model {
 
    var $table = 'tblrole';
    var $tabled = 'tblmenu';
    var $column_order = array('ID_PREFIXD','NM_PREFIXD','DESC1','DESC2',null); //set column field database for datatable orderable
    var $column_search = array('ID_PREFIXD','NM_PREFIXD','DESC1','DESC2'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('ID_PREFIXD' => 'desc'); // default order 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function syncM()
    {
        $sql="select USERID from tbluser order by USERID ";
        $sql1= $this->db->query($sql);
        if($sql1->num_rows()>0){                              
            foreach ($sql1->result() as $row){
                $userid = trim($row->USERID);
                $queryP = $this->db->query('SELECT a.ID_MENU FROM tblmenu as a ORDER BY a.ID_MENU');
                foreach ($queryP->result() as $rowP){
                   $idm = trim($rowP->ID_MENU);
                    $queryCheckUP = $this->db->query('SELECT a.USERID FROM tblrole as a WHERE a.USERID=? and a.id_menu=?',array($userid,$idm));
                    if(count($queryCheckUP->result_array())==0){
                        $data_insert = array(
                            'USERID' => $userid,
                            'ID_MENU' => $idm
                        );
                        $this->db->insert('tblrole', $data_insert);
                    }
                }
            }
            return "OK";
        }
    }

    public function get_role($id)
    {
        // $this->db->select('tblrole.ID_MENU,tblmenu.NAMA_MENU,tblmenu.LEVEL_MENU,tblrole.[READ] as readx,tblrole.[WRITE] as writex,tblrole.[DELETE] as deletex,tblrole.[ADD] as addx');
        $this->db->select('tblrole.id_menu,tblmenu.nm_menu,tblmenu.level_menu,tblrole.lread as readx,tblrole.lwrite as writex,tblrole.ldelete as deletex,tblrole.ladd as addx');
        $this->db->from($this->table);
        $this->db->join($this->tabled, 'tblmenu.id_menu = tblrole.id_menu');
        $this->db->where('tblrole.userid', $id);
        $this->db->order_by("tblmenu.urut_menu","ASC");
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_datauser()
    {
        $this->db->select('userid,usernm');
        $this->db->from("tbluser");
        $query = $this->db->get();
        return $query->result();
    }

    public function saveall($data,$userid)
    {
        for($i=0;$i<count($data);$i++){
            // $data_update = array(
            //     '[READ]' => $data[$i]->read,
            //     'WRITE' => $data[$i]->write,
            //     '[DELETE]' => $data[$i]->delete_,
            //     '[ADD]' => $data[$i]->add
            // );
            $data_update = array(
                'lread' => $data[$i]->read,
                'lwrite' => $data[$i]->write,
                'ldelete' => $data[$i]->delete_,
                'ladd' => $data[$i]->add
            );
            $where = array(
                'userid' => $userid,
                'id_menu' => $data[$i]->idm
            );
            $this->db->update($this->table, $data_update, $where);
        }
        return "OK";
    }
}