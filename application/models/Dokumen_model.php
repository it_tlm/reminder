<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Dokumen_model extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->database();
    } 

    var $table = 'tbldokumen';
    var $primaryKey = 'acak_dokumen';

    public function saveDokumen($data){
        $this->db->insert($this->table, $data);
    }

    public function getLastId(){
        $last = $this->db->order_by($this->primaryKey,"desc")
		->limit(1)
        ->from($this->table)
		->get()
		->row();

        return $last->acak_dokumen;
    }

    public function uploadDokumen($file){
          
            
          $nama_dokumen = time().'-'.date("Y-m-d").'-'.$_FILES['dokumen']['name'];

          $config['upload_path']          = './uploads/reminder/';
          $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
          $config['overwrite']			  = false;
          $config['file_name']            = $nama_dokumen;
          // $config['max_size']             = 1024; // 1MB
          // $config['max_width']            = 1024;
          // $config['max_height']           = 768;
          $this->load->library('upload', $config);

          if ( ! $this->upload->do_upload('dokumen'))
          {
                  $error = array('error' => $this->upload->display_errors());

                  return json_encode($error);
          }
          else
          {
                  $data = array('upload_data' => $this->upload->data());

                  return $config['upload_path'].$data['upload_data']['file_name'];
          }
  
          
        //   $path_db = $config['upload_path'].$nama_dokumen;
  
        //   $data['acak_dokumen']=
        //   $data['acak_reminder']=
        //   $data['path_dokumen']=
        //   $data['acak_departement']=
  
        //   $this->dokumen->addDokumen();
        return var_dump($file['dokumen']);
    }
}
?>
 