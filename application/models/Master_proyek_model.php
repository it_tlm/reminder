<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Master_proyek_model extends CI_Model {
 
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
 
    var $table = 'tblproyek';
    var $column_order = array(null, 'pt.nm_pt', 'pry.nm_pry', 'pry.aktif', null); //set column field database for datatable orderable
    var $column_search = array('pry.nm_pry', null); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('pry.nm_pry' => 'asc'); // default order 
    
    private function _get_datatables_query(){
         
        $this->db->select("pt.nm_pt, pry.nm_pry, pry.id_pry, pry.id_pt, pry.aktif, pry.urut") ; 
        $this->db->from('tblproyek pry' );
        $this->db->join('tblpt pt','pt.id_pt=pry.id_pt');
        if($this->session->userdata("validlevel")<=8)
        {
            $this->db->where('rtrim(pry.id_pt)',trim($this->session->userdata("validpt")));
            $this->db->where('rtrim(pry.id_pry)',trim($this->session->userdata("validpry")));
        }
 
        $i = 0;
        $where = '';
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $where = '('.$item." like '%".$_POST['search']['value']."%'";
                }
                else
                {
                    $where .= ' OR '.$item." like '%".$_POST['search']['value']."%'";
                }
            }
            $i++;
        }

        if(!empty($where)){
            $where .= ")";
            $this->db->where($where);
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
    public function get_by_id($idpry,$idpt){
        $this->db->select('pt.nm_pt, tblproyek.nm_pry, tblproyek.id_pry, tblproyek.id_pt, tblproyek.svr_sams, tblproyek.dt_sams, tblproyek.svr_sales, tblproyek.dt_sales, tblproyek.aktif, tblproyek.path_gambar'); 
        $this->db->from($this->table);
        $this->db->join('tblpt pt','pt.id_pt=tblproyek.id_pt');
        $this->db->where('tblproyek.id_pry',$idpry);
        $this->db->where('tblproyek.id_pt',$idpt);
        $query = $this->db->get();
 
        return $query->row();
    }
 
    public function savept($data1)
    {
        $hasil = 'GAGAL';
        $this->db->insert('tblpt', $data1);
        if($this->db->affected_rows()>0)
        {
            $hasil = 'OK';
        }
        return $hasil;
    }

    public function savepry($data2)
    {
        $hasil = 'GAGAL';
        $this->db->insert($this->table, $data2);
        if($this->db->affected_rows()>0)
        {
            $hasil = 'OK';
        }
        return $hasil;
    }

    public function delpt($id)
    {
        $this->db->where('id_pt', $id);
        $this->db->delete('tblpt');
    } 
 
    public function updatept($where, $data1){
        $hasil = 'GAGAL';
        $this->db->update('tblpt', $data1, $where);
        if($this->db->affected_rows()>0)
        {
            $hasil = 'OK';
        }
        return $hasil;
    }

    public function updatepry($where2, $data2){
        $hasil = 'GAGAL';
        $this->db->update('tblproyek', $data2, $where2);
        if($this->db->affected_rows()>0)
        {
            $hasil = 'OK';
        }
        return $hasil;
    }

    public function delete_gambar($idpry)
    {
        $hasil = 'GAGAL';
        $query = $this->db->query("select * from tblproyek where id_pry ='".$idpry."' ");
        $getarray='';
        foreach ($query->result() as $row)
        {
            $getarray = trim($row->path_gambar);
        }

        if($getarray == '-' || $getarray == '')
        {
            $hasil = 'OK';
        }
        else
        {
            $delete = unlink(FCPATH.$getarray);
            if($delete){
                $hasil = 'OK';
            }
        }
        return $hasil;
    }

    public function delete_by_id($idpry,$idpt)
    {
        $hasil = 'GAGAL';
        $this->db->where('id_pry', $idpry);
        $this->db->delete($this->table);
        if($this->db->affected_rows()>0)
        {
            $this->db->where('id_pt', $idpt);
            $this->db->delete('tblpt');
            if($this->db->affected_rows()>0)
            {
                $hasil = 'OK';
            }
        }
        return $hasil;
    } 

    public function getProyeks(){
        $this->db->select('*');
        $this->db->from($this->table);
        $query = $this->db->get();
        
        return $query->result();
    }
}