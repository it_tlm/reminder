<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Pic_model extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->database();
    } 

    var $table = 'tblpic';
    var $primaryKey = 'acak_pic';

    

    public function save($data){
        $data['acak_pic'] = intval($this->getLastId())+1;
        $this->db->insert($this->table,$data);
    }

    public function update($id,$data){
        $this->db->where($this->primaryKey,$id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function get_by_id($id){
        $this->db->from($this->table);
        $this->db->where($this->primaryKey,$id);
        $query = $this->db->get();
 
        return $query->row();
    }

    public function get_by_acak_reminder($acaK_reminder){
        $this->db->from($this->table);
        $this->db->where('acak_reminder',$acaK_reminder);
        $query = $this->db->get();
 
        return $query->row();
    }

    public function getLastId(){
        $last = $this->db->order_by($this->primaryKey,"desc")
        ->select($this->primaryKey)
		->limit(1)
        ->from($this->table)
		->get()
		->row();

        if($last == NULL){
            $last = 0;
        }else{
            $last = $last->acak_pic;
        }

        return $last;
    }

}
?>