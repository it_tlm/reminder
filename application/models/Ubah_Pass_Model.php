<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Ubah_Pass_Model extends CI_Model {
 
    var $table = 'tbluser';

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
    function checkPass($userid,$passwordlama)
    {
        $query = $this->db->get_where($this->table,array('userid' => trim($userid),'lock' => 0));
        if($query->num_rows()>0){
            $row = $query->row();
            $hash = $row->password;
            if (password_verify($passwordlama, $hash)) {
                return true;
            } else {
                return false;
            }
        }else{
            return false;   
        }
    }
 
    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        if($this->db->affected_rows()>0){
            return "OK";
        }else{
            return "FAILED_UPDATE";
        }
    }
    
}