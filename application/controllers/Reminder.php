<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Reminder extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','download'));	
        $this->load->library('session');
        $this->load->model('Master_Jenis_Dokumen_model','jenis_dokumen');
        $this->load->model('Master_Jenis_Produk_model','jenis_produk');
        $this->load->model('Master_Departement_model','departement');
        $this->load->model('Master_Proyek_model','proyek');
        $this->load->model('Master_User_Approve_model','user_approve');
        $this->load->model('Reminder_model','reminder');
        $this->load->model('SendMail_model','send_mail');
        $this->load->model('Pic_model','pic');
        $this->load->model('Dokumen_model','dokumen');
    }
 
    public function index()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        $validUser = $this->session->userdata("validuser");
        $validNama = $this->session->userdata("validnama");
        $validLevel = $this->session->userdata("validlevel");
        $validMenu = $this->session->userdata("validmenu");

        if(!$isLoggedIn){
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
            
        }else{
            $data['title'] = 'List Reminder';
            $data['menu'] = 'List Reminder';
            $data['judul'] = 'List Reminder';
            $data['submenu'] = 'List Reminder';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $data["validmenu"] = $validMenu;
            $data["jenis_produks"] =  $this->jenis_produk->getJenisProduk()->result();
            $data["departements"] =  $this->departement->getDepartement();
            $data["proyeks"] = $this->proyek->getProyeks();
            $data['user_approves'] = $this->user_approve->getUserApproves();
            $this->load->view('templates/header',$data);
            $this->load->view('Frm_Reminder',$data);
            $this->load->view('templates/footer',$data);
        }
    }

    //datatable 
    public function ajax_list()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $list = $this->reminder->get_datatables();
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $reminder) {
                $no++;
                $row = array();
                $row[] = '<center>'.$no.'</center>';
                $row[] = trim($reminder->no_dokumen);
                $row[] = trim($reminder->acak_departemen);
                $row[] = trim($reminder->acak_reminder);
                $row[] = trim($reminder->tgl_berlaku_dokumen);
                $row[] = trim($reminder->acak_jenis_produk);
                $row[] = trim($reminder->acak_jenis_dokumen);
                $row[] = trim($reminder->keterangan);
                $row[] = trim($reminder->acak_proyek);
                
                //cek approve pic dan admin

                if($reminder->approve_by_pic === NULL){
                    $btn_approve_pic = '<a class="btn btn-sm btn-success btnapprove" href="javascript:void(0)" title="Approve PIC" onclick="approve_pic('."'".$reminder->acak_reminder."'".')" >Approve PIC</a>'; 
                }else{
                    $btn_approve_pic = null;
                }

                if($reminder->approve_by_admin === NULL AND $reminder->approve_by_pic !== NUll){
                    $btn_approve_admin = '<a class="btn btn-sm btn-success btnapprove" href="javascript:void(0)" title="Approve PIC" onclick="approve_admin('."'".$reminder->acak_pic."'".')" >Approve Admin</a>'; 
                }else{
                    $btn_approve_admin = null;
                }

                //label satus approve
                $badge_pic ='';
                $badge_admin='';
                if($reminder->approve_by_pic == NULL && $reminder->approve_by_admin == NULL ){
                    $badge_pic ='<span class="badge badge-danger"><i class="fas fa-times"></i></span>';
                    $badge_admin='<span class="badge badge-danger"><i class="fas fa-times"></i></span>';
                }elseif($reminder->approve_by_pic != NULL && $reminder->approve_by_admin == NULL){
                    $badge_pic ='<span class="badge badge-success"><i class="fas fa-check"></i></span>';
                    $badge_admin='<span class="badge badge-danger"><i class="fas fa-times"></i></span>';
                }elseif($reminder->approve_by_pic != NULL && $reminder->approve_by_admin != NULL){
                    $badge_pic ='<span class="badge badge-success"><i class="fas fa-check"></i></span>';
                    $badge_admin='<span class="badge badge-success"><i class="fas fa-check"></i></span>';
                }

                $row[] = '<center>'.$badge_pic.'</center>'; 

                $row[] = '<center>'.$badge_admin.'</center>'; 
                
                //add html for action
                $row[] = '<center><a class="btn btn-sm btn-primary btnedit" href="javascript:void(0)" title="Edit" onclick="edit_reminder('."'".$reminder->acak_reminder."'".')"> Edit</a>
                    '.$btn_approve_pic." ".$btn_approve_admin.'
                      <a class="btn btn-sm btn-danger btndelete" href="javascript:void(0)" title="Hapus" onclick="delete_Menu('."'".$reminder->acak_reminder."'".')"> Delete</a></center><a href="'.site_url("/reminder/view/".$reminder->acak_reminder).' " class="btn btn-xs btn-primary text-white">View</a>';     
                
                $data[] = $row;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->reminder->count_all(),
                            "recordsFiltered" => $this->reminder->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }

    public function ajax_list_report(){
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $list = $this->reminder->get_datatables();
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $reminder) {
                $no++;
                $row = array();
                $row[] = '<center>'.$no.'</center>';
                $row[] = trim($reminder->no_dokumen);
                $row[] = trim($reminder->acak_departemen);
                $row[] = trim($reminder->acak_reminder);
                $row[] = trim($reminder->tgl_berlaku_dokumen);
                $row[] = trim($reminder->acak_jenis_produk);
                $row[] = trim($reminder->acak_jenis_dokumen);
                $row[] = trim($reminder->keterangan);
                $row[] = trim($reminder->acak_proyek);
                
                //cek approve pic dan admin

                if($reminder->approve_by_pic === NULL){
                    $btn_approve_pic = '<a class="btn btn-sm btn-success btnapprove" href="javascript:void(0)" title="Approve PIC" onclick="approve_pic('."'".$reminder->acak_reminder."'".')" >Approve PIC</a>'; 
                }else{
                    $btn_approve_pic = null;
                }

                if($reminder->approve_by_admin === NULL AND $reminder->approve_by_pic !== NUll){
                    $btn_approve_admin = '<a class="btn btn-sm btn-success btnapprove" href="javascript:void(0)" title="Approve PIC" onclick="approve_admin('."'".$reminder->acak_pic."'".')" >Approve Admin</a>'; 
                }else{
                    $btn_approve_admin = null;
                }

                //label satus approve
                $badge_pic ='';
                $badge_admin='';
                if($reminder->approve_by_pic == NULL && $reminder->approve_by_admin == NULL ){
                    $badge_pic ='<span class="badge badge-danger"><i class="fas fa-times"></i></span>';
                    $badge_admin='<span class="badge badge-danger"><i class="fas fa-times"></i></span>';
                }elseif($reminder->approve_by_pic != NULL && $reminder->approve_by_admin == NULL){
                    $badge_pic ='<span class="badge badge-success"><i class="fas fa-check"></i></span>';
                    $badge_admin='<span class="badge badge-danger"><i class="fas fa-times"></i></span>';
                }elseif($reminder->approve_by_pic != NULL && $reminder->approve_by_admin != NULL){
                    $badge_pic ='<span class="badge badge-success"><i class="fas fa-check"></i></span>';
                    $badge_admin='<span class="badge badge-success"><i class="fas fa-check"></i></span>';
                }

                $row[] = '<center>'.$badge_pic.'</center>'; 

                $row[] = '<center>'.$badge_admin.'</center>'; 
                
                //add html for action
                $row[] = '<center><a class="btn btn-sm btn-primary btnedit" href="javascript:void(0)" title="Edit" onclick="edit_reminder('."'".$reminder->acak_reminder."'".')"> Edit</a>
                    '.$btn_approve_pic." ".$btn_approve_admin.'
                      <a class="btn btn-sm btn-danger btndelete" href="javascript:void(0)" title="Hapus" onclick="delete_Menu('."'".$reminder->acak_reminder."'".')"> Delete</a></center>';     
                
                $data[] = $row;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->reminder->count_all(),
                            "recordsFiltered" => $this->reminder->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }


    public function ajax_add()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data = array(
                    'no_dokumen' => $this->input->post('no_dokumen'),
                    'acak_proyek' => $this->input->post('acak_proyek'),
                    'tgl_reminder' => $this->input->post('tgl_reminder'),
                    'masa_berlaku' => $this->input->post('masa_berlaku'),
                    'acak_jenis_produk' => $this->input->post('acak_jenis_produk'),
                    'tgl_berlaku_dokumen' => $this->input->post('tgl_berlaku_dokumen'),
                    'acak_jenis_dokumen' => $this->input->post('acak_jenis_dokumen'),
                    'opsi_reminder' => $this->input->post('opsi_reminder'),
                    'keterangan' => $this->input->post('keterangan'),
                    'acak_admin' => $this->input->post('acak_admin'),
                    'acak_proyek' => $this->input->post('acak_proyek'),
                    'acak_departemen' => $this->input->post('acak_departemen'),
                    'acak_reminder'=>intval($this->reminder->getLastId()) + 1,
                    'create_by'=>$this->session->userdata('validuser'),
                    'create_date'=>date('Y-m-d H:i:s')
                );
            $data_pic = [
                'acak_pic'=>intval($this->pic->getLastId()) + 1,
                'acak_reminder' => $data['acak_reminder'],
                'acak_user_approve' => $data['acak_admin']
            ];
          

            $user_approve = $this->user_approve->get_by_id($data['acak_admin']);

            //validasi
            $this->load->helper(array('form'));

            $this->load->library('form_validation');
            $this->form_validation->set_rules('no_dokumen', 'No Dokumen', 'required|is_unique[tblreminder.no_dokumen]',array('is_unique' => 'No Dokumen Sudah ada','required'=>'No Dokumen Mohon di Isi'));

            //upload dokumen
            $file = $this->dokumen->uploadDokumen($_FILES);
            $data_dokumen = [
                'acak_dokumen'=>intval($this->dokumen->getLastId()) + 1,
                'acak_reminder'=>$data['acak_reminder'],
                'acak_departemen'=>$data['acak_departemen'],
                'path_dokumen'=>$file
            ];
            $this->dokumen->saveDokumen($data_dokumen);

            //send email
            $reminder = $this->reminder->get_by_id($data['acak_reminder']);
            $data_mail=[
                'nama_staff'=>$user_approve->nama_staff,
                'email_staff'=>$user_approve->email_staff,
                'message'=>"No Dokumen :".$data["no_dokumen"]." Departement :".$reminder->acak_departement." User :".$reminder->usernm."  Mohon Segera di Tindak lanjuti"
            ];

            if($this->form_validation->run())
            {
             
                $insert = $this->reminder->save($data);
                $return_result = array("status" => TRUE);
            }
            else
            {
             $return_result = array(
              'status'   => false,
              'no_dokumen' => form_error('no_dokumen')
             );
            }
                        
            echo json_encode($return_result);

            if($return_result["status"] == true){
                $this->send_mail->sendMail($data_mail);
            }
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }

    public function ajax_edit($id)
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data = $this->reminder->get_by_id($id);
            echo json_encode($data);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
    
    public function ajax_update()
    {
        $validUser = $this->session->userdata("validuser");
        $return_result = array("status" => FALSE, "msg" => 'Gagal update data');
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            
            $acak_reminder = $this->input->post('acak_reminder');
            $data = array(
                'no_dokumen' => $this->input->post('no_dokumen'),
                'acak_proyek' => $this->input->post('acak_proyek'),
                'tgl_reminder' => $this->input->post('tgl_reminder'),
                'masa_berlaku' => $this->input->post('masa_berlaku'),
                'acak_jenis_produk' => $this->input->post('acak_jenis_produk'),
                'tgl_berlaku_dokumen' => $this->input->post('tgl_berlaku_dokumen'),
                'acak_jenis_dokumen' => $this->input->post('acak_jenis_dokumen'),
                'opsi_reminder' => $this->input->post('opsi_reminder'),
                'keterangan' => $this->input->post('keterangan'),
                'acak_admin' => $this->input->post('acak_admin'),
                'acak_proyek' => $this->input->post('acak_proyek'),
                'acak_departemen' => $this->input->post('acak_departemen')
            );
            // echo $updatept;die;
            $return_result = $this->reminder->update($acak_reminder,$data);
            if($return_result > 0){
                $result  = array("status" => TRUE, "msg" => 'Berhasil Update Data');
            }else{
                $result = array("status" => FALSE, "msg" => 'Gagal update data');
            }
            echo json_encode($result);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }

    public function ceklevel(){
        $lvl = trim($this->session->userdata("validlevel"));
        echo $lvl;
    }

    public function ajax_delete()
    {
        $acak_departement =  $this->input->post('acak_reminder');
        $hasil = $this->reminder->delete($acak_reminder);
        return $hasil;
    } 

    public function download($id){
        $reminder = $this->reminder->get_by_id($id);
        force_download($reminder->path_dokumen,NULL);
    }

    public function cekReminder(){
        // get reminder where tglreminder == now and tgl_berlaku_dokumen > now
        $this->db->select('*');
        $this->db->from('tblreminder');
        $this->db->where('tgl_berlaku_dokumen =>',date('Y-m-d'));
        $result = $this->db->get();
        $reminders = $result->result();

        if($reminders != NULL){
              foreach($reminders as $reminder){
                    $pic = $this->user_approve->get_by_id($reminder->acak_admin);
        
                    $data['email_staff'] = $pic->email_staff;
                    $data['nama_staff'] = $pic->nama_staff;
                    $data['message'] ="No Dokumen :".$reminder->no_dokumen." Mohon segera di approve";
                    $this->send_mail->sendMail($data);
                }
        }
      
    }

    public function ajax_approve_pic($acak_reminder){
      $isLoggedIn = $this->session->userdata("isLoggedIn");
      $validUser = $this->session->userdata("validuser");
      $row = $this->pic->get_by_acak_reminder($acak_reminder);
      $reminder = $this->reminder->get_by_id($acak_reminder);
      $data=[
          'acak_reminder'=>$acak_reminder,
          'acak_user_approve'=>$reminder->acak_admin,
          'approve_by_pic'=>$validUser,
          'approve_time_pic'=>date('Y-m-d H:i:s'),
          'status_approve'=>0
      ];
      if($isLoggedIn){
        if($row === NULL){
            $this->pic->save($data);
            $return_result = array("status" => TRUE);
            echo json_encode($return_result);
        }
      }else{
        $data['title'] = 'INDEX';
        $data['baseurl'] = base_url();
        $data['siteurl'] = site_url();
        $this->load->view('frmindex', $data);
    }
    }

    public function ajax_approve_admin($acak_pic){
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        $validUser = $this->session->userdata("validuser");
        $row = $this->pic->get_by_id($acak_pic);
        $data = [
            'approve_by_admin'=>$validUser,
            'approve_time_admin'=>date('Y-m-d H:i:s'),
            'status_approve'=>1
        ];
        if($isLoggedIn){
            if($row->approve_by_admin === NULL){
                $update = $this->pic->update($acak_pic,$data);
                if($update > 0){
                    $return_result = array("status" => TRUE);
                    echo json_encode($return_result);
                }
                
            }
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
    
    public function report(){
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        $validUser = $this->session->userdata("validuser");
        $validNama = $this->session->userdata("validnama");
        $validLevel = $this->session->userdata("validlevel");
        $validMenu = $this->session->userdata("validmenu");

        if(!$isLoggedIn){
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
            
        }else{
            $data['title'] = 'Report Reminder';
            $data['menu'] = 'Report Reminder';
            $data['judul'] = 'Report Reminder';
            $data['submenu'] = 'Report Reminder';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $data["validmenu"] = $validMenu;
            $data["jenis_produks"] =  $this->jenis_produk->getJenisProduk()->result();
            $data["departements"] =  $this->departement->getDepartement();
            $data["proyeks"] = $this->proyek->getProyeks();
            $data['user_approves'] = $this->user_approve->getUserApproves();
            $this->load->view('templates/header',$data);
            $this->load->view('frmReminder_report',$data);
            $this->load->view('templates/footer',$data);
        }
    }

    public function sort_reminder(){
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $tgl_end_expired=$this->input->post('tgl_end_expired');
            $tgl_start_reminder=$this->input->post('tgl_start_reminder');
            $tgl_end_reminder=$this->input->post('tgl_end_reminder');
            $tgl_start_expired=$this->input->post('tgl_start_expired');
            $status = $this->input->post('status');

            $param['tgl_end_expired']=$this->input->post('tgl_end_expired');
            $param['tgl_start_reminder']=$this->input->post('tgl_start_reminder');
            $param['tgl_end_reminder']=$this->input->post('tgl_end_reminder');
            $param['tgl_start_expired']=$this->input->post('tgl_start_expired');
            $param['status'] = $this->input->post('status');
    
            $this->db->select('
            tblreminder.no_dokumen as no_dokumen,
            tbldepartement.nama_departement as acak_departemen,
            tblreminder.acak_reminder as acak_reminder,
            tblreminder.tgl_reminder as tgl_reminder,
            tblreminder.masa_berlaku as masa_berlaku,
            tblreminder.tgl_berlaku_dokumen as tgl_berlaku_dokumen,
            tbljenisproduk.nama_jenis_produk as acak_jenis_produk,
            tbljenisdokumen.nama_jenis_dok as acak_jenis_dokumen,
            tblreminder.opsi_reminder as opsi_reminder,
            tblreminder.keterangan as keterangan,
            tbluser.userid as create_by,
            tblreminder.create_date as create_date,
            tbluserapprove.nama_staff as nama_staff,
            tblproyek.nm_pry as acak_proyek,
            tblpic.acak_pic as acak_pic,
            tblpic.approve_by_pic as approve_by_pic,
            tblpic.approve_by_admin as approve_by_admin
            ');
            
            $this->db->from('tblreminder');
            $this->db->join('tbldepartement','tbldepartement.acak_departement = tblreminder.acak_departemen');
            $this->db->join('tbljenisproduk','tbljenisproduk.acak_jenis_produk = tblreminder.acak_jenis_produk');
            $this->db->join('tbljenisdokumen','tbljenisdokumen.acak_jenis_dok = tblreminder.acak_jenis_dokumen');
            $this->db->join('tbluser','tbluser.userid = tblreminder.create_by');
            $this->db->join('tbluserapprove','tbluserapprove.acak_user_approve = tblreminder.acak_admin');
            $this->db->join('tblproyek','tblproyek.id_pry = tblreminder.acak_proyek');
            $this->db->join('tblpic','tblpic.acak_reminder = tblreminder.acak_reminder','left');
            if($status == 0){
                $this->db->where(' tblpic.approve_by_pic =',NULL);
                $this->db->where(' tblpic.approve_by_admin =',NULL);
            }
    
            if($status == 1){
                $this->db->where(' tblpic.approve_by_pic !=',NULL);
                $this->db->where(' tblpic.approve_by_admin =',NULL);
            }
    
    
            if($status == 2){
                $this->db->where(' tblpic.approve_by_pic !=',NULL);
                $this->db->where(' tblpic.approve_by_admin !=',NULL);
            }
    
            // if($status == 3){
            //     $this->db->where(' tblpic.approve_by_pic !=',NULL);
            //     $this->db->where(' tblpic.approve_by_admin !=',NULL);
            // }
            
            $this->db->group_start();
            $this->db->or_group_start();
            // between date reminder
            $this->db->where('tblreminder.tgl_reminder >=',$tgl_start_reminder);
            $this->db->where('tblreminder.tgl_reminder <=',$tgl_end_reminder);
            $this->db->group_end();

            $this->db->or_group_start();
            // between date expired
            $this->db->where('tblreminder.tgl_berlaku_dokumen >=',$tgl_start_expired);
            $this->db->where('tblreminder.tgl_berlaku_dokumen <=',$tgl_end_expired);
            $this->db->group_end();
            $this->db->group_end();

            $query = $this->db->get();

            $list = $this->reminder->reportReminderQuery($param);
            $data = array();
            $no = 0;
            foreach ($list as $reminder) {
                $no++;
                $row = array();
                $row[] = '<center>'.$no.'</center>';
                $row[] = trim($reminder->no_dokumen);
                $row[] = trim($reminder->acak_departemen);
                $row[] = trim($reminder->acak_reminder);
                $row[] = trim($reminder->tgl_berlaku_dokumen);
                $row[] = trim($reminder->acak_jenis_produk);
                $row[] = trim($reminder->acak_jenis_dokumen);
                $row[] = trim($reminder->keterangan);
                $row[] = trim($reminder->acak_proyek);
                
                //cek approve pic dan admin

                if($reminder->approve_by_pic === NULL){
                    $btn_approve_pic = '<a class="btn btn-sm btn-success btnapprove" href="javascript:void(0)" title="Approve PIC" onclick="approve_pic('."'".$reminder->acak_reminder."'".')" >Approve PIC</a>'; 
                }else{
                    $btn_approve_pic = null;
                }

                if($reminder->approve_by_admin === NULL AND $reminder->approve_by_pic !== NUll){
                    $btn_approve_admin = '<a class="btn btn-sm btn-success btnapprove" href="javascript:void(0)" title="Approve PIC" onclick="approve_admin('."'".$reminder->acak_pic."'".')" >Approve Admin</a>'; 
                }else{
                    $btn_approve_admin = null;
                }

                //label satus approve
                $badge_pic ='';
                $badge_admin='';
                if($reminder->approve_by_pic == NULL && $reminder->approve_by_admin == NULL ){
                    $badge_pic ='<span class="badge badge-danger"><i class="fas fa-times"></i></span>';
                    $badge_admin='<span class="badge badge-danger"><i class="fas fa-times"></i></span>';
                }elseif($reminder->approve_by_pic != NULL && $reminder->approve_by_admin == NULL){
                    $badge_pic ='<span class="badge badge-success"><i class="fas fa-check"></i></span>';
                    $badge_admin='<span class="badge badge-danger"><i class="fas fa-times"></i></span>';
                }elseif($reminder->approve_by_pic != NULL && $reminder->approve_by_admin != NULL){
                    $badge_pic ='<span class="badge badge-success"><i class="fas fa-check"></i></span>';
                    $badge_admin='<span class="badge badge-success"><i class="fas fa-check"></i></span>';
                }

                $row[] = '<center>'.$badge_pic.'</center>'; 

                $row[] = '<center>'.$badge_admin.'</center>'; 
                
                //add html for action
                $row[] = '<center><a class="btn btn-sm btn-primary btnedit" href="javascript:void(0)" title="Edit" onclick="edit_reminder('."'".$reminder->acak_reminder."'".')"> Edit</a>
                    '.$btn_approve_pic." ".$btn_approve_admin.'
                      <a class="btn btn-sm btn-danger btndelete" href="javascript:void(0)" title="Hapus" onclick="delete_Menu('."'".$reminder->acak_reminder."'".')"> Delete</a></center>';     
                
                $data[] = $row;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->reminder->count_all(),
                            "recordsFiltered" => $this->reminder->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }


       
    }

    public function exportReminder(){
    include APPPATH.'third_party/PHPExcel/Classes/PHPExcel.php';
    
    $excel = new PHPExcel();

    // Settingan awal fil excel
    $excel->getProperties()->setCreator('My Notes Code')
                 ->setLastModifiedBy('My Notes Code')
                 ->setTitle("Data Reminder")
                 ->setSubject("Reminder")
                 ->setDescription("Laporan Semua Data Reminder")
                 ->setKeywords("Data Reminder");
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    // // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = array(
      'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA REMINDER"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A1:J1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO");
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "NO DOKUMEN");
    $excel->setActiveSheetIndex(0)->setCellValue('C3', "DEPARTEMENT");
    $excel->setActiveSheetIndex(0)->setCellValue('D3', "EXPIRED");
    $excel->setActiveSheetIndex(0)->setCellValue('E3', "JENIS PERIZINAN");
    $excel->setActiveSheetIndex(0)->setCellValue('F3', "DETAIL PERIZINAN");
    $excel->setActiveSheetIndex(0)->setCellValue('G3', "KETERANGAN");
    $excel->setActiveSheetIndex(0)->setCellValue('H3', "NAMA PROYEK");
    $excel->setActiveSheetIndex(0)->setCellValue('I3', "APPROVE PIC");
    $excel->setActiveSheetIndex(0)->setCellValue('J3', "APPROVE ADMIN");
    
    // Apply style header yang telah kita buat tadi ke masing-masing kolom header

    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);

    $reminder = $this->reminder->sortQuery();
    $no = 1; // Untuk penomoran tabel, di awal set dengan 1
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    
    foreach($reminder as $data){ 
      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->no_dokumen);
      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->acak_departemen);
      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->tgl_berlaku_dokumen);
      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->acak_jenis_produk);
      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->acak_jenis_dokumen);
      $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->keterangan);
      $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->acak_proyek);
      $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data->approve_by_pic == NULL ? "TIDAK" : "YA");
      $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow,  $data->approve_by_admin == NULL ? "TIDAK" : "YA");
      
      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
      
      $no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); 
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); 
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); 
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(16);
    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
    $excel->getActiveSheet()->getColumnDimension('I')->setWidth(12); 
    $excel->getActiveSheet()->getColumnDimension('J')->setWidth(16); 
    
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Laporan Data Reminder");
    $excel->setActiveSheetIndex(0);
    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data Reminder_'.date('Y-m-d').'.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');
    }


    // view reminder
    public function view($id){
        $reminder = $this->reminder->get_by_id($id);
        
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        $validUser = $this->session->userdata("validuser");
        $validNama = $this->session->userdata("validnama");
        $validLevel = $this->session->userdata("validlevel");
        $validMenu = $this->session->userdata("validmenu");

        if(!$isLoggedIn){
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
            
        }else{
            $data['title'] = 'List Reminder';
            $data['menu'] = 'List Reminder';
            $data['judul'] = 'List Reminder';
            $data['submenu'] = 'List Reminder';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $data["validmenu"] = $validMenu;
            $data["reminder"] =  $reminder;
            $this->load->view('templates/header',$data);
            $this->load->view('View_Reminder',$data);
            $this->load->view('templates/footer',$data);
        }
    }

}