<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Master_Role extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Master_Role_model','role');
    }
    public function index()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        $validUser = $this->session->userdata("validuser");
        $validNama = $this->session->userdata("validnama");
        $validLevel = $this->session->userdata("validlevel");
        $validMenu = $this->session->userdata("validmenu");

        if(!$isLoggedIn){
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
            
        }else{
            $data['title'] = 'Master Role';
            $data['menu'] = 'Master Role';
            $data['judul'] = 'Master Role';
            $data['submenu'] = 'Master Role';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $data["validmenu"] = $validMenu;
            $data['listuser'] = $this->role->get_datauser();
            $this->load->view('templates/header',$data);
            $this->load->view('FrmMaster_Role',$data);
            $this->load->view('templates/footer',$data);
        }
    }
    public function setSpace($param)
    {
        $space = '';
        for($i=1;$i<$param;$i++){
            $space = $space."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        return $space;
    }
    public function listRole($id)
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $list = $this->role->get_role($id);
            $output="<table class='table table-striped'>";
            $output=$output."<tr>
                                    <th>NO</th>
                                    <th>MENU</th>
                                    <th>READ</th>
                                    
                                </tr>";
                                
            $i=1;
            foreach ($list as $role) {
                $idm = $role['id_menu'];
                $nmmenu = $this->setSpace($role['level_menu']).trim($role['nm_menu']);
                $read = $role['readx'];
                $add = $role['addx'];
                $write = $role['writex'];
                $delete = $role['deletex'];
                $rem = '<i class="fas fa-times" style="color:red"></i>';
                $ok = '<i class="fas fa-check" style="color:green"></i>';

                $readicon = $rem;
                if($read==1){
                    $readicon = $ok;
                }

                $addicon = $rem;
                if($add==1){
                    $addicon = $ok;
                }

                $writeicon = $rem;
                if($write==1){
                    $writeicon = $ok;
                }

                $deleteicon = $rem;
                if($delete==1){
                    $deleteicon = $ok;
                }
                //add html for action
                $output=$output."<tr>
                                    <td><input id='idm$i' type='hidden' class='form-control clslist' value='".$idm."'>$i</td>
                                    <td>$nmmenu</td>
                                    <td><input id='read$i' type='hidden' value='".$read."'><span id='spanread$i' onclick='click_r($i,\"read\")'>$readicon</span></td>
                                    <td style='display:none;'><input id='add$i' type='hidden' value='".$add."'><span id='spanadd$i' onclick='click_r($i,\"add\")'>$addicon</span></td>
                                    <td style='display:none;'><input id='write$i' type='hidden' value='".$write."'><span id='spanwrite$i' onclick='click_r($i,\"write\")'>$writeicon</span></td>
                                    <td style='display:none;'><input id='delete$i' type='hidden' value='".$delete."'><span id='spandelete$i' onclick='click_r($i,\"delete\")'>$deleteicon</span></td>
                                </tr>";
                $i++;
            }
            $output=$output."</table>";
            echo $output;
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }

    public function proses_SyncM()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $sync_result = $this->role->syncM();
            echo $sync_result;
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }

    public function save_all()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data = json_decode($this->input->post('data'));
            $userid = $this->input->post('userid');
            $save = $this->role->saveall($data,$userid);
            
            echo $save;
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
}