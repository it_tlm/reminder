<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Master_Menu extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Master_Menu_model','menu');
    }
 
    public function index()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        $validUser = $this->session->userdata("validuser");
        $validNama = $this->session->userdata("validnama");
        $validLevel = $this->session->userdata("validlevel");
        $validMenu = $this->session->userdata("validmenu");

        if(!$isLoggedIn){
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
            
        }else{
            $data['title'] = 'Master Menu';
            $data['menu'] = 'Master Menu';
            $data['judul'] = 'Master Menu';
            $data['submenu'] = 'Master Menu';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $data["validmenu"] = $validMenu;
            $this->load->view('templates/header',$data);
            $this->load->view('FrmMaster_Menu',$data);
            $this->load->view('templates/footer',$data);
        }
    }
    public function getMax()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $list = $this->menu->get_maxid();
            echo json_encode($list);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
    public function setSpace($param){
        $space = '';
        for($i=1;$i<$param;$i++){
            $space = $space."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        return $space;
    }
    public function ajax_list()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $list = $this->menu->get_datatables();
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $menu) {
                $no++;
                $row = array();
                $row[] = '<center>'.trim($menu->urut_menu).'</center>';
                $row[] = '<center>'.trim($menu->id_menu).'</center>';
                $row[] = $this->setSpace($menu->level_menu).trim($menu->nm_menu);
                $row[] = '<center>'.$menu->level_menu.'</center>';
     
                //add html for action
                $row[] = '<center><a class="btn btn-sm btn-primary btnedit" href="javascript:void(0)" title="Edit" onclick="edit_Menu('."'".$menu->id_menu."'".')"> Edit</a>
                      <a class="btn btn-sm btn-danger btndelete" href="javascript:void(0)" title="HapusX" onclick="delete_Menu('."'".$menu->id_menu."'".')"> Delete</a></center>';
     
                $data[] = $row;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->menu->count_all(),
                            "recordsFiltered" => $this->menu->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_edit($id)
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data = $this->menu->get_by_id($id);
            echo json_encode($data);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_add()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data = array(
                    'id_menu' => $this->input->post('idmenu'),
                    'nm_menu' => $this->input->post('nmmenu'),
                    'urut_menu' => $this->input->post('urutmenu'),
                    'level_menu' => $this->input->post('level')
                );
            $insert = $this->menu->save($data);
            $return_result = array("status" => TRUE);
            
            echo json_encode($return_result);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_update()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data = array(
                    'nm_menu' => $this->input->post('nmmenu'),
                    'urut_menu' => $this->input->post('urutmenu'),
                    'level_menu' => $this->input->post('level')
                );
            $this->menu->update(array('id_menu' => $this->input->post('idmenu')), $data);
            $return_result = array("status" => TRUE);
            
            echo json_encode($return_result);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_delete($id)
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $this->menu->delete_by_id($id);
            $return_result = array("status" => TRUE);
            
            echo json_encode($return_result);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
}