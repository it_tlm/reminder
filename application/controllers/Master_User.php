<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Master_User extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Master_User_Model','user');
    }
 
    public function index()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        $validUser = $this->session->userdata("validuser");
        $validNama = $this->session->userdata("validnama");
        $validLevel = $this->session->userdata("validlevel");
        $validMenu = $this->session->userdata("validmenu");

        if(!$isLoggedIn){
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
            
        }else{
            $data['title'] = 'Master User';
            $data['menu'] = 'Master User';
            $data['judul'] = 'Master User';
            $data['submenu'] = 'Master User';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $data["validmenu"] = $validMenu;
            $data['s_proyek'] = $this->user->get_proyek('nm_pry','ASC');
            $this->load->view('templates/header',$data);
            $this->load->view('FrmMaster_User',$data);
            $this->load->view('templates/footer',$data);
        }
    }

    public function ajax_list()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $list = $this->user->get_datatables();
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $user) {
                $no++;
                $row = array();
                $row[] = trim($user->userid);
                $row[] = trim($user->usernm);
                $status = 'UNLOCK';
                if(trim($user->lock)==1){
                    $status = 'LOCK';
                }
                $row[] = trim($user->userlvl);
                $row[] = $status;
     
                //add html for action
                $row[] = '<a class="btn btn-sm btn-primary btnedit" href="javascript:void(0)" title="Edit" onclick="edit_User('."'".$user->userid."'".')"><i class="fas fa-edit"></i> Ubah</a>
                      <a class="btn btn-sm btn-danger btndelete" href="javascript:void(0)" title="HapusX" onclick="delete_User('."'".$user->userid."'".')"><i class="fas fa-trash"></i> Hapus</a>';
     
                $data[] = $row;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->user->count_all(),
                            "recordsFiltered" => $this->user->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_edit($id)
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data = $this->user->get_by_id($id);
            echo json_encode($data);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_add()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $options = [
                'cost' => 12,
            ];
            $data = array(
                    'userid' => $this->input->post('iduser'),
                    'usernm' => $this->input->post('nmuser'),
                    'userlvl' => $this->input->post('level'),
                    'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options),
                    // 'password' => md5($this->input->post('password')),
                    'lock' => $this->input->post('lock'),
                    'kodeproyek' => trim($this->input->post('proyek'))
                );
            $insert = $this->user->save($data);
            echo json_encode(array("status" => TRUE));
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_update()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data = array(
                    'usernm' => $this->input->post('nmuser'),
                    'userlvl' => $this->input->post('level'),
                    'lock' => $this->input->post('lock'),
                    'kodeproyek' => trim($this->input->post('proyek'))
                );
            $this->user->update(array('USERID' => $this->input->post('iduser')), $data);
            echo json_encode(array("status" => TRUE));
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_delete($id)
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $this->user->delete_by_id($id);
            echo json_encode(array("status" => TRUE));
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
}