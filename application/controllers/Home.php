<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, POST');

	
	class home extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('home_model', 'home');
			$this->load->helper('url');
			$this->load->library('session');
			// $this->load->library('Pdf');
		}
		
		public function index(){
			$isLoggedIn = $this->session->userdata("isLoggedIn");
			$validUser = $this->session->userdata("validuser");
			$validNama = $this->session->userdata("validnama");
			$validLevel = $this->session->userdata("validlevel");
			$validMenu = $this->session->userdata("validmenu");
			
			if($isLoggedIn)
			{
				$data['title'] = 'Home';
				$data['menu'] = 'Home';
				$data['submenu'] = '';
				$data['baseurl'] = base_url();
				$data['siteurl'] = site_url();
				$data["validmenu"] = $validMenu;
				$this->load->view('templates/header',$data);
				$this->load->view('frmhome',$data);
				$this->load->view('templates/footer',$data);
			}
			else
			{
				$data['title'] = 'INDEX';
				$data['baseurl'] = base_url();
				$data['siteurl'] = site_url();
				$this->load->view('frmindex', $data);
			}
		}
	}
?>