<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, POST');

	class index extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->helper('url');
			$this->load->library('session');
		}
		
		public function index(){
			$isLoggedIn = $this->session->userdata("isLoggedIn");
			// $validJenis = $this->session->userdata("validjenis");

			$data['title'] = 'Index';
			$data['baseurl'] = base_url();
			$data['siteurl'] = site_url();
			// $data['validJenis'] = $validJenis;
			$this->load->view('templates/header_index',$data);
			$this->load->view('frmindex',$data);
		}

		public function checkSession(){
			$isLoggedIn = $this->session->userdata("isLoggedIn");
			if($isLoggedIn)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}
?>