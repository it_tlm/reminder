<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Master_Proyek extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('master_proyek_model','menu');
    }
 
    public function index()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        $validUser = $this->session->userdata("validuser");
        $validNama = $this->session->userdata("validnama");
        $validLevel = $this->session->userdata("validlevel");
        $validMenu = $this->session->userdata("validmenu");

        if(!$isLoggedIn){
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
            
        }else{
            $data['title'] = 'Master Proyek';
            $data['menu'] = 'Master Proyek';
            $data['judul'] = 'Master Proyek';
            $data['submenu'] = 'Master Proyek';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $data["validmenu"] = $validMenu;
            $this->load->view('templates/header',$data);
            $this->load->view('FrmMaster_Proyek',$data);
            $this->load->view('templates/footer',$data);
        }
    }

    public function ceklevel(){
        $lvl = trim($this->session->userdata("validlevel"));
        echo $lvl;
    }

    public function ajax_list()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $list = $this->menu->get_datatables();
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $menu) {
                $no++;
                $row = array();
                $row[] = '<center>'.$no.'</center>';
                $row[] = trim($menu->nm_pt);
                $row[] = trim($menu->nm_pry);
                if($menu->aktif==1)
                {
                    $aktip='AKTIF';
                }
                else
                {
                    $aktip='TIDAK AKTIF';
                }
                $row[] = '<center>'.$aktip.'</center>';
                
                //add html for action
                $row[] = '<center><a class="btn btn-sm btn-primary btnedit" href="javascript:void(0)" title="Edit" onclick="edit_Menu('."'".$menu->id_pry."'".', '."'".$menu->id_pt."'".')"> Edit</a>
                      <a class="btn btn-sm btn-danger btndelete" href="javascript:void(0)" title="Hapus" onclick="delete_Menu('."'".$menu->id_pry."'".', '."'".$menu->id_pt."'".')"> Delete</a></center>';     
                $data[] = $row;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->menu->count_all(),
                            "recordsFiltered" => $this->menu->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_edit()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $idpry = $this->input->post('idpry');
            $idpt = $this->input->post('idpt');
            $data = $this->menu->get_by_id($idpry,$idpt);
            echo json_encode($data);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }

    public function ajax_add()
    {
        $validUser = $this->session->userdata("validuser");
        $return_result = array("status" => FALSE, "msg" => 'Gagal tambah data');
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data1 = array(
                    'id_pt' => trim($this->input->post('idpt')),
                    'nm_pt' => $this->input->post('nmpt'),
                    'modify' => date('Y-m-d').'_'.'ADD_'.trim($validUser)
                );
            $data2 = array(
                    'id_pry' => trim($this->input->post('idpry')),                    
                    'nm_pry' => $this->input->post('nmpry'),
                    'id_pt' => trim($this->input->post('idpt')),
                    'svr_sams' => $this->input->post('ipsams'),
                    'dt_sams' => $this->input->post('dbsams'),
                    'svr_sales' => $this->input->post('ipsales'),
                    'dt_sales' => $this->input->post('dbsales'),
                    'aktif' => $this->input->post('status'),
		        	'flagproject' => $this->input->post('statuspro'),
                    'path_gambar' => $this->input->post('imgInp'),
                    'modify' => date('Y-m-d').'_'.'ADD_'.trim($validUser)
                );
            $insertpt = $this->menu->savept($data1);
            if($insertpt=='OK')
            {
                $insertpry = $this->menu->savepry($data2);
                if($insertpry=='OK')
                {
                    $return_result = array("status" => TRUE);
                }
                else
                {
                    $delete_pt = $this->menu->delpt($this->input->post('idpt'));
                }
            }
            echo json_encode($return_result);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_update()
    {
        $validUser = $this->session->userdata("validuser");
        $return_result = array("status" => FALSE, "msg" => 'Gagal update data');
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data1 = array(
                    'nm_pt' => $this->input->post('nmpt'),
                    'modify' => date('Y-m-d').'_EDIT_'.trim($validUser)
                );
            $data2 = array(
                    'nm_pry' => $this->input->post('nmpry'),
                    'svr_sams' => $this->input->post('ipsams'),
                    'dt_sams' => $this->input->post('dbsams'),
                    'svr_sales' => $this->input->post('ipsales'),
                    'dt_sales' => $this->input->post('dbsales'),
                    'aktif' => $this->input->post('status'),
                    'flagproject' => $this->input->post('statuspro'),
                    'path_gambar' => $this->input->post('imgInp'),
                    'modify' => date('Y-m-d').'_EDIT_'.trim($validUser)
                );
            $updatept = $this->menu->updatept(array('id_pt' => trim($this->input->post('idpt'))),$data1);
            // echo $updatept;die;
            if($updatept=='OK')
            {
                $delgmbr = $this->menu->delete_gambar(trim($this->input->post('idpry')));
                if($delgmbr=='OK')
                {  
                    $updatepry = $this->menu->updatepry(array('id_pry' => trim($this->input->post('idpry'))),$data2);
                    if($updatepry=='OK')
                    {
                        $return_result = array("status" => TRUE);
                    }
                }
            }
            echo json_encode($return_result);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_delete()
    {
        $return_result = array("status" => FALSE);
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $idpry = $this->input->post('idpry');
            $idpt = $this->input->post('idpt');            
            $delgmbr = $this->menu->delete_gambar($idpry);
            if($delgmbr=='OK')
            {
                $del = $this->menu->delete_by_id($idpry,$idpt);
                if($del=='OK')
                {
                    $return_result = array("status" => TRUE);
                }            
            }
            echo json_encode($return_result);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }

    public function upload_file($elemname,$folder)
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn)
        {
            $status = "";
            $datainput = array();
            $data = array();
            $msg = "";
            $file_name = "";
            $abs_path = "";
            $orig_name = "";
            $path_gambar = "";
            $file_element_name = $elemname;            
            $fullpath = $config['upload_path'] = './uploads/';            

            if (!file_exists("uploads/".$folder."")) {
                mkdir("uploads/".$folder."",0777,true);
            }
            if ($status != "error")
            {
                $config['upload_path'] = './uploads/'.$folder.'';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size']  = 1024 * 2;
                $config['encrypt_name'] = FALSE;
                $config['file_name'] =  $folder."_background_".date("Ymd")."_".uniqid();
                
                $this->load->library('upload', $config);
                $this->upload->overwrite=true;
                if (!$this->upload->do_upload($file_element_name))
                {
                    $status = 'error';
                    $msg = $this->upload->display_errors('', '');
                }
                else
                {
                    $data = $this->upload->data();
                    $file_name = $data['file_name'];
                    $abs_path = $data['full_path'];
                    $orig_name = $data['orig_name'];
                    $path_gambar = 'uploads/'.$folder.'/'.$file_name;
                }
                @unlink($_FILES[$file_element_name]);
            }                        
            echo json_encode(array('status' => $status,'msg' => $msg,'filename' => $file_name,'filepath' => $abs_path,'origname' => $orig_name,'path_gambar' => $path_gambar));
        }        
    } 

    public function get_proyeks(){
        $proyeks = $this->menu->getProyeks();
        echo $proyeks;
    }
}