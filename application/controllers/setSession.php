<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, POST');

	class setSession extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->helper('url');
			$this->load->library('session');
		}
		
		function sessions(){
			$value = $this->input->post("value");
			$this->session->set_userdata('sample',$value);
		}
	}
?>