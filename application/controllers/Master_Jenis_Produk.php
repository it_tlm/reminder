<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Master_Jenis_Produk extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Master_Jenis_Produk_model','jenis_produk');
    }
 
    public function index()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        $validUser = $this->session->userdata("validuser");
        $validNama = $this->session->userdata("validnama");
        $validLevel = $this->session->userdata("validlevel");
        $validMenu = $this->session->userdata("validmenu");

        if(!$isLoggedIn){
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
            
        }else{
            $data['title'] = 'Master Jenis Produk';
            $data['menu'] = 'Master Jenis Produk';
            $data['judul'] = 'Master Jenis Produk';
            $data['submenu'] = 'Master Jenis Produk';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $data["validmenu"] = $validMenu;
            $this->load->view('templates/header',$data);
            $this->load->view('FrmMaster_jenis_produk',$data);
            $this->load->view('templates/footer',$data);
        }
    }

    //datatable 
    public function ajax_list()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $list = $this->jenis_produk->get_datatables();
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $menu) {
                $no++;
                $row = array();
                $row[] = '<center>'.$no.'</center>';
                $row[] = trim($menu->nama_jenis_produk);
                $row[] = trim($menu->keterangan);
                
                //add html for action
                $row[] = '<center><a class="btn btn-sm btn-primary btnedit" href="javascript:void(0)" title="Edit" onclick="edit_Menu('."'".$menu->acak_jenis_produk."'".')"> Edit</a>
                      <a class="btn btn-sm btn-danger btndelete" href="javascript:void(0)" title="Hapus" onclick="delete_Menu('."'".$menu->acak_jenis_produk."'".')"> Delete</a></center>';     
                $data[] = $row;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->jenis_produk->count_all(),
                            "recordsFiltered" => $this->jenis_produk->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }


    public function ajax_add()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data = array(
                    'acak_jenis_produk' => $this->input->post('acak_jenis_produk'),
                    'nama_jenis_produk' => $this->input->post('nama_jenis_produk'),
                    'keterangan' => $this->input->post('ket_jenis_produk')
                );
            $insert = $this->jenis_produk->save($data);
            $return_result = array("status" => TRUE);
            
            echo json_encode($return_result);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }

    public function ajax_edit()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $acak_jenis_produk = $this->input->post('acak_jenis_produk');
            $data = $this->jenis_produk->get_by_id($acak_jenis_produk);
            echo json_encode($data);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
    
    public function ajax_update()
    {
        $validUser = $this->session->userdata("validuser");
        $return_result = array("status" => FALSE, "msg" => 'Gagal update data');
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            
            $acak_jenis_produk = $this->input->post('acak_jenis_produk');
            $data = array(
                    'nama_jenis_produk' => $this->input->post('nama_jenis_produk'),
                    'keterangan' => $this->input->post('ket_jenis_produk'),
         );
            // echo $updatept;die;
            $return_result = $this->jenis_produk->update($acak_jenis_produk,$data);
            if($return_result > 0){
                $result  = array("status" => TRUE, "msg" => 'Berhasil Update Data');
            }else{
                $result = array("status" => FALSE, "msg" => 'Gagal update data');
            }
            echo json_encode($result);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }

    public function ceklevel(){
        $lvl = trim($this->session->userdata("validlevel"));
        echo $lvl;
    }

    public function ajax_delete()
    {
        $acak_jenis_produk =  $this->input->post('acak_jenis_produk');
        $hasil = $this->jenis_produk->delete($acak_jenis_produk);
        echo $hasil;
    } 

    public function getJenisDokumenByAcakProduk(){
        $acak_jenis_produk = $this->input->get('acak_jenis_produk');
        $query = $this->jenis_produk->getJenisDokumenByAcakProduk($acak_jenis_produk);
        $jenis_dokumen = $query->result();
        echo json_encode($jenis_dokumen);
    }

    

    
}