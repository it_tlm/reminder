<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Ubah_Pass extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Ubah_Pass_Model','pass');
    }
 
    public function index()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        $validUser = $this->session->userdata("validuser");
        $validNama = $this->session->userdata("validnama");
        $validLevel = $this->session->userdata("validlevel");
        $validMenu = $this->session->userdata("validmenu");

        if(!$isLoggedIn){
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
            
        }else{
            $data['title'] = 'Ubah Password';
            $data['menu'] = 'Ubah Password';
            $data['judul'] = 'Ubah Password';
            $data['submenu'] = 'Ubah Password';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $data['validUser'] = $validUser;
            $data['validNama'] = $validNama;
            $data["validmenu"] = $validMenu;
            $this->load->view('templates/header',$data);
            $this->load->view('FrmUbah_Pass',$data);
            $this->load->view('templates/footer',$data);
        }
    }

    public function ajax_change()
    {
        $userid = trim($this->input->post('iduser'));
        $passwordlama = $this->input->post('passwordlama');
        $passwordbaru = $this->input->post('passwordbaru');

        $checkPass = $this->pass->checkPass($userid,$passwordlama);
        if($checkPass){
            $options = [
                'cost' => 12,
            ];
            $data = array(
                'password' => password_hash($this->input->post('passwordbaru'), PASSWORD_BCRYPT, $options)
            );
            $updateresult = $this->pass->update(array('USERID' => $userid), $data);
            if($updateresult=="OK"){
                echo "OK";
            }else{
                echo "FAILED_UPDATE";
            }
        }else{
            echo "FAILED_PASSAWAL";
        }
    }

 
}