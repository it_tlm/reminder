<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, POST');

	class login extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('login_model', 'login');
			$this->load->helper('url');
			$this->load->library('session');
		}
		
		public function index(){
			$data['title'] = 'Login';
			$data['baseurl'] = base_url();
			$data['siteurl'] = site_url();
			$this->load->view('templates/header_index',$data);
			$this->load->view('frmlogin',$data);
		}
		
		public function userlogin(){
			$USERID = $this->input->post('id');
			$PASS = $this->input->post('pass');
			$arrHasil = array();

			$cekuser = $this->login->cekUser($USERID, $PASS);
			if($cekuser!='')
			{
				$ruser = $cekuser->row();
				$USERNM = trim($ruser->usernm);
				$USERLVL = $ruser->userlvl;
				$kdpt = $ruser->kodept;
				$kdpry = $ruser->kodeproyek;

				$mnuser = $this->login->getmenu($USERID);
				if($mnuser->num_rows() > 0)
				{
					$arrMenu = array();
					foreach($mnuser->result() as $rmn)
					{
						$urrMenu = trim($rmn->urut_menu);
						array_push($arrMenu, $urrMenu);
					}

					$arrUser = array(
						"isLoggedIn" => true,
						"validuser" => trim(strtoupper($USERID)),
						"validnama" => trim(strtoupper($USERNM)),
						"validlevel" => $USERLVL,
						"validmenu" => $arrMenu,
						"validpt" => $kdpt,
						"validpry" => $kdpry,
					);
					$this->session->set_userdata($arrUser);

					$arrHasil = array('STATUS' => TRUE, 'MSG' => "");
				}
				else
				{
					$arrHasil = array('STATUS' => FALSE, 'MSG' => 'SETTING MENU PENGGUNA BELUM DI ATUR');
				}
			}
			else
			{
				$arrHasil = array('STATUS' => FALSE, 'MSG' => 'USERID / PASSWORD YANG ANDA MASUKKAN SALAH');
			}
			echo json_encode($arrHasil);
		}
	}
?>