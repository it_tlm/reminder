<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Master_User_Approve extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Master_User_Approve_Model','user_approve');
        $this->load->model('Master_Departement_model','departement');
        $this->load->model('Master_User_model','user');
    }
 
    public function index()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        $validUser = $this->session->userdata("validuser");
        $validNama = $this->session->userdata("validnama");
        $validLevel = $this->session->userdata("validlevel");
        $validMenu = $this->session->userdata("validmenu");
        
        $pts = $this->db->select('*')->from('tblpt')->get();
        

        if(!$isLoggedIn){
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
            
        }else{
            $data['title'] = 'Master User Approve';
            $data['menu'] = 'Master User Approve';
            $data['judul'] = 'Master User Approve';
            $data['submenu'] = 'Master User Approve';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $data["validmenu"] = $validMenu;
            $data["pts"]    = $pts;
            $data["departements"] = $this->departement->getDepartement();
            $data["users"] = $this->user->getUsers();
            $this->load->view('templates/header',$data);
            $this->load->view('frmMaster_User_Approve',$data);
            $this->load->view('templates/footer',$data);
        }
    }

    public function ajax_list()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $list = $this->user_approve->get_datatables();
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $user_approve) {
                $no++;
                $row = array();
                $row[] = trim($user_approve->usernm);
                $row[] = trim($user_approve->nama_staff);
                $row[] = trim($user_approve->email_staff);
                $row[] = trim($user_approve->phone_staff);
                $row[] = trim($user_approve->nm_pt);
                $row[] = trim($user_approve->nama_departement);
     
                //add html for action
                $row[] = '<a class="btn btn-sm btn-primary btnedit" href="javascript:void(0)" title="Edit" onclick="edit_user_approve('."'".$user_approve->acak_user_approve."'".')"><i class="fas fa-edit"></i> Ubah</a>
                      <a class="btn btn-sm btn-danger btndelete" href="javascript:void(0)" title="HapusX" onclick="delete_user_approve('."'".$user_approve->acak_user_approve."'".')"><i class="fas fa-trash"></i> Hapus</a>';
     
                $data[] = $row;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->user_approve->count_all(),
                            "recordsFiltered" => $this->user_approve->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_edit($id)
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data = $this->user_approve->get_by_id($id);
            echo json_encode($data);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_add()
    {
        $last_acak_user_approve = $this->user_approve->getLastId();
        if($last_acak_user_approve === NULL){
            $tmp_id = 1;
        }else{
            $tmp_id = intval($last_acak_user_approve) + 1;
        }
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data = array(
                    'acak_user_approve'=>strval($tmp_id),
                    'acak_user' => $this->input->post('acak_user'),
                    'nama_staff' => $this->input->post('nama_staff'),
                    'email_staff' => $this->input->post('email_staff'),
                    'phone_staff' => $this->input->post('phone_staff'),
                    'acak_departement' => $this->input->post('acak_departement'),
                    'acak_pt' => $this->input->post('acak_pt'),
                );
            $insert = $this->user_approve->save($data);
            echo json_encode(array("status" => TRUE));
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_update()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $data = array(
                'acak_user' => $this->input->post('acak_user'),
                'nama_staff' => $this->input->post('nama_staff'),
                'email_staff' => $this->input->post('email_staff'),
                'phone_staff' => $this->input->post('phone_staff'),
                'acak_departement' => $this->input->post('acak_departement'),
                'acak_pt' => $this->input->post('acak_pt'),
                );
            $this->user_approve->update($this->input->post('acak_user_approve'), $data);
            echo json_encode(array("status" => TRUE));
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
 
    public function ajax_delete($id)
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $this->user_approve->delete_by_id($id);
            echo json_encode(array("status" => TRUE));
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }

    public function ceklevel(){
        $lvl = trim($this->session->userdata("validlevel"));
        echo $lvl;
    }

    public function test(){
        var_dump($this->user_approve->getLastId());
    }
 
}