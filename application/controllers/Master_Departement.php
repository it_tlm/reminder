<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Master_Departement extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Master_departement_model','departement');
    }
 
    public function index()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        $validUser = $this->session->userdata("validuser");
        $validNama = $this->session->userdata("validnama");
        $validLevel = $this->session->userdata("validlevel");
        $validMenu = $this->session->userdata("validmenu");

        if(!$isLoggedIn){
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
            
        }else{
            $data['title'] = 'Master Departement';
            $data['menu'] = 'Master Departement';
            $data['judul'] = 'Master Departement';
            $data['submenu'] = 'Master Departement';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $data["validmenu"] = $validMenu;
            $this->load->view('templates/header',$data);
            $this->load->view('FrmMaster_Departement',$data);
            $this->load->view('templates/footer',$data);
        }
    }

    //datatable 
    public function ajax_list()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $list = $this->departement->get_datatables();
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $menu) {
                $no++;
                $row = array();
                $row[] = '<center>'.$no.'</center>';
                $row[] = trim($menu->nama_departement);
                $row[] = trim($menu->ket_departement);
                
                //add html for action
                $row[] = '<center><a class="btn btn-sm btn-primary btnedit" href="javascript:void(0)" title="Edit" onclick="edit_Menu('."'".$menu->acak_departement."'".')"> Edit</a>
                      <a class="btn btn-sm btn-danger btndelete" href="javascript:void(0)" title="Hapus" onclick="delete_Menu('."'".$menu->acak_departement."'".')"> Delete</a></center>';     
                $data[] = $row;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->departement->count_all(),
                            "recordsFiltered" => $this->departement->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }


    public function ajax_add()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $this->load->helper(array('form'));

            $this->load->library('form_validation');
            $this->form_validation->set_rules('acak_departement', 'ID Departemen', 'required|is_unique[tbldepartement.acak_departement]');

            $data = array(
                    'acak_departement' => $this->input->post('acak_departement'),
                    'nama_departement' => $this->input->post('nama_departement'),
                    'ket_departement' => $this->input->post('ket_departement')
                );
            
                if($this->form_validation->run())
                {
                 
                    $insert = $this->departement->save($data);
                    $return_result = array("status" => TRUE);
                }
                else
                {
                 $return_result = array(
                  'error'   => true,
                  'acak_departement' => form_error('acak_departement')
                 );
                }
            
            echo json_encode($return_result);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }

    public function ajax_edit()
    {
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            $acak_departement = $this->input->post('acak_departement');
            $data = $this->departement->get_by_id($acak_departement);
            echo json_encode($data);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }
    
    public function ajax_update()
    {
        $validUser = $this->session->userdata("validuser");
        $return_result = array("status" => FALSE, "msg" => 'Gagal update data');
        $isLoggedIn = $this->session->userdata("isLoggedIn");
        if($isLoggedIn){
            
            $acak_departement = $this->input->post('acak_departement');
            $data = array(
                    'nama_departement' => $this->input->post('nama_departement'),
                    'ket_departement' => $this->input->post('ket_departement'),
         );
            // echo $updatept;die;
            $return_result = $this->departement->update($acak_departement,$data);
            if($return_result > 0){
                $result  = array("status" => TRUE, "msg" => 'Berhasil Update Data');
            }else{
                $result = array("status" => FALSE, "msg" => 'Gagal update data');
            }
            echo json_encode($result);
        }else{
            $data['title'] = 'INDEX';
            $data['baseurl'] = base_url();
            $data['siteurl'] = site_url();
            $this->load->view('frmindex', $data);
        }
    }

    public function ceklevel(){
        $lvl = trim($this->session->userdata("validlevel"));
        echo $lvl;
    }

    public function ajax_delete()
    {
        $acak_departement =  $this->input->post('acak_departement');
        $hasil = $this->departement->delete($acak_departement);
        return $hasil;
    } 

    

    
}