<div class="container">
        <h3><?php echo $judul; ?></h3>
        <br />
        
        <div class="container">
<form class="needs-validation" novalidate>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom01">Tanggal Reminder</label>
      <input type="text" class="form-control" id="tgl_start_reminder" placeholder="Tanggal Awal">
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">&nbsp</label>
      <input type="text" class="form-control" id="tgl_end_reminder" placeholder="Tanggal Akhir">
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom01">Tanggal Expired</label>
      <input type="text" class="form-control" id="tgl_start_expired" placeholder="Tanggal Awal">
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">&nbsp</label>
      <input type="text" class="form-control" id="tgl_end_expired" placeholder="Tanggal Akhir">
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom01">Status</label>
      <select name="status" id="status" class="form-control">
        <option value="0">Belum di Approve</option>
        <option value="1">Approve PIC</option>
        <option value="2">Approve Admin</option>
        <option value="3">Approve Semua</option>
      </select>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <input type="button" class="btn btn-primary" value="Proses" id="proses"></input>
      <input type="button" class="btn btn-success" value="Export Excel" id="export"></input>
    </div>
  </div>
</form>
        <!-- <div class="row">
            <div class="col-sm">
            <div class="form-group">
                <label for="exampleInputEmail1">Tanggal Reminder</label>
                <input type="text" class="form-control" id="tgl_reminder" aria-describedby="emailHelp" placeholder="Tanggal Reminder">
                <input type="text" class="form-control" id="tgl_reminder" aria-describedby="emailHelp" placeholder="Tanggal Reminder">
            </div>
            </div>
            <div class="col-sm">
            <div class="form-group">
                <label for="exampleInputEmail1">Tanggal Expired</label>
                <input type="text" class="form-control" id="tgl_expired" aria-describedby="emailHelp" placeholder="Tanggal expired">
            </div>
            </div>
            <div class="col-sm">
            </div>
        </div> -->
        </div>


        <br />
        <br />
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                <th>NO</th>
                    <th>No Dokumen</th>
                    <th>Departement</th>
                    <th>Reminder</th>
                    <th>expired</th>
                    <th>Jenis Perizinan</th>
                    <th>Detail Perizinan</th>
                    <th>Keterangan</th>
                    <th>Nama Proyek</th>
                    <th>Approve PIC</th>
                    <th>Approve Admin</th>
                    <th style="width:160px;">Tindakan</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
            <th>NO</th>
            <th>No Dokumen</th>
                    <th>Departement</th>
                    <th>Reminder</th>
                    <th>expired</th>
                    <th>Jenis Perizinan</th>
                    <th>Detail Perizinan</th>
                    <th>Keterangan</th>
                    <th>Nama Proyek</th>
                    <th>Approve PIC</th>
                    <th>Approve Admin</th>
                    <th>Tindakan</th>
            </tr>
            </tfoot>
        </table>
    </div>
    
 <script type="text/javascript" src="<?php echo $baseurl;?>script/bootstrap/bootstrap-validator.js"></script> 
 <script type="text/javascript" src="<?php echo $baseurl;?>script/master/report_reminder.js?v=<?php echo uniqid(); ?>"></script>

  </body>  
</html>