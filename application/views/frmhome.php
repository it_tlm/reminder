			<!-- Page Heading -->
			<style type="text/css">
				#pagehome {
					background-color: black;
					background-image: url('<?php echo base_url()?>images/PROJECT_LOGO.png');
					background-position: center;
					background-repeat: no-repeat;
					background-size: 50% 30%;
				}

				.card {
					overflow: hidden;
					color:white;
				}

				.card-block .rotate {
					z-index: 8;
					float: right;
					height: 100%;
				}

				.card-block .rotate i {
					color: rgba(20, 20, 20, 0.15);
					position: absolute;
					left: 0;
					left: auto;
					right: -10px;
					bottom: 0;
					display: block;
					-webkit-transform: rotate(-44deg);
					-moz-transform: rotate(-44deg);
					-o-transform: rotate(-44deg);
					-ms-transform: rotate(-44deg);
					transform: rotate(-44deg);
				}
			</style>
			<div data-role="page" class="container mt-5 h-100 mw-100">
				<div class="alert alert-warning fade collapse" role="alert" id="myAlert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
						<span class="sr-only">Close</span>
					</button>
					<strong>Holy guacamole!</strong> It's free.. this is an example theme.
				</div>

				<div class="row mb-3">
					<div class="col-xl-3 col-lg-6">
						<div class="card card-inverse card-success">
							<div class="card-block bg-success">
								<div class="rotate">
									<i class="fa fa-user fa-5x"></i>
								</div>
								<h6 class="text-uppercase">Total Dokumen</h6>
								<h1 class="display-1">134</h1>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-6">
						<div class="card card-inverse card-danger">
							<div class="card-block bg-danger">
								<div class="rotate">
									<i class="fa fa-list fa-4x"></i>
								</div>
								<h6 class="text-uppercase">Jumlah Reminder</h6>
								<h1 class="display-1">87</h1>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-6">
						<div class="card card-inverse card-info">
							<div class="card-block bg-info">
								<div class="rotate">
									<i class="fa fa-twitter fa-5x"></i>
								</div>
								<h6 class="text-uppercase">APPROVE PIC</h6>
								<h1 class="display-1">125</h1>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-6">
						<div class="card card-inverse card-warning">
							<div class="card-block bg-warning">
								<div class="rotate">
									<i class="fa fa-share fa-5x"></i>
								</div>
								<h6 class="text-uppercase">APPROVE ADMIN</h6>
								<h1 class="display-1">36</h1>
							</div>
						</div>
					</div>
				</div>
				<!--/row-->

				<hr>
			</div>