<div class="container">
        <h3><?php echo $judul; ?></h3>
        <br />
        
        <div class="container">
        <div class="row">
            <div class="col-sm">
            
            </div>
            <div class="col-sm">
            <button class="btn btn-success btnadd" onclick="add_data()"><i class="fas fa-plus"></i> Tambah Reminder</button>
            </div>
            <div class="col-sm">
            <input type="text" class="form-control">
            </div>
        </div>
        </div>
        <br />
        <br />
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                <th>NO</th>
                    <th>No Dokumen</th>
                    <th>Departement</th>
                    <th>Reminder</th>
                    <th>expired</th>
                    <th>Jenis Perizinan</th>
                    <th>Detail Perizinan</th>
                    <th>Keterangan</th>
                    <th>Nama Proyek</th>
                    <th>Approve PIC</th>
                    <th>Approve Admin</th>
                    <th style="width:160px;">Tindakan</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
            <th>NO</th>
            <th>No Dokumen</th>
                    <th>Departement</th>
                    <th>Reminder</th>
                    <th>expired</th>
                    <th>Jenis Perizinan</th>
                    <th>Detail Perizinan</th>
                    <th>Keterangan</th>
                    <th>Nama Proyek</th>
                    <th>Approve PIC</th>
                    <th>Approve Admin</th>
                    <th>Tindakan</th>
            </tr>
            </tfoot>
        </table>
    </div>
    
 <script type="text/javascript" src="<?php echo $baseurl;?>script/bootstrap/bootstrap-validator.js"></script> 
 <script type="text/javascript" src="<?php echo $baseurl;?>script/master/master_reminder.js?v=<?php echo uniqid(); ?>"></script>
 
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" data-toggle="validator">
                    <div class="form-body">
                        <div class="form-group row">
                            <label class="control-label col-md-3">No Dokumen</label>
                            <div class="col-md-9">
                            <input type="hidden" name="acak_reminder" id="acak_reminder">
                                <input name="no_dokumen" id="no_dokumen" placeholder="No Dokumen" class="form-control" type="text" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Departement</label>
                            <div class="col-md-9">
                                <select id="acak_departemen" name="acak_departemen" class="form-control" required>
                                    <option value="">--Select Departement--</option>
                                    <?php foreach($departements as $departement){
                                        ?><option value="<?php echo trim($departement->acak_departement); ?>"><?php echo $departement->nama_departement; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal Reminder</label>
                            <div class="col-md-9">
                                <input name="tgl_reminder" id="tgl_reminder" type="text" class="form-control"  value="<?= date('Y-m-d') ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Masa Berlaku</label>
                            <div class="col-md-9">
                                <input name="masa_berlaku" id="masa_berlaku" type="number" min="1" class="form-control" value="1" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal Berlaku</label>
                            <div class="col-md-9">
                                <input name="tgl_berlaku_dokumen" id="tgl_berlaku_dokumen" type="text" class="form-control" readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Jenis Produk</label>
                            <div class="col-md-9">
                                <select id="acak_jenis_produk" name="acak_jenis_produk" class="form-control" required>
                                    <option value="">--Select Jenis Produk--</option>
                                    <?php foreach($jenis_produks as $produk){
                                        ?><option value="<?php echo trim($produk->acak_jenis_produk); ?>"><?php echo $produk->nama_jenis_produk; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="proyek" class="control-label col-md-3">Jenis Dokumen</label>
                            <div class="col-sm-9">
                                <select id="acak_jenis_dokumen" name="acak_jenis_dokumen" class="form-control" required>
                                    <option value="">--Select Jenis Dokumen--</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="proyek" class="control-label col-md-3">Dokumen</label>
                            <div class="col-sm-9">
                                <input type="file" name="dokumen" id="dokumen"></input>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Opsi Reminder</label>
                            <div class="col-md-9">
                                <select name="opsi_reminder" id="opsi_reminder" class="form-control" required>
                                    <option value="">--Select Status--</option>
                                    <option value="0">UNLOCK</option>
                                    <option value="1">LOCK</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Keterangan</label>
                            <div class="col-md-9">
                                <textarea name="keterangan" id="keterangan" class="form-control" id="" cols="30" rows="10"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Admin</label>
                            <div class="col-md-9">
                            <select id="acak_admin" name="acak_admin" class="form-control" required>
                                    <option value="">--Select Admin--</option>
                                    <?php foreach($user_approves as $user_approve){
                                        ?><option value="<?php echo trim($user_approve->acak_user_approve); ?>"><?php echo $user_approve->nama_staff; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Proyek</label>
                            <div class="col-md-9">
                            <select id="acak_proyek" name="acak_proyek" class="form-control" required>
                                    <option value="">--Select Proyek--</option>
                                    <?php foreach($proyeks as $proyek){
                                        ?><option value="<?php echo trim($proyek->id_pry); ?>"><?php echo $proyek->nm_pry; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary"><i class="far fa-save"></i> Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
  </body>  
</html>