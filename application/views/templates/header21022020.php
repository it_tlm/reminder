<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Taruma City</title>
  <script type="text/javascript" src="<?php echo $baseurl;?>script/jquery-3.4.1.min.js"></script>
  <script src="<?php echo $baseurl;?>script/datatables/js/jquery.dataTables.min.js"></script> 
  <script src="<?php echo $baseurl;?>script/datatables/js/dataTables.bootstrap.min.js"></script>
  <script src="<?php echo $baseurl;?>script/datatables/js/dataTables.bootstrap4.min.js"></script>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url()?>assets/startbootstrap-sb-admin-2-gh-pages/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="<?php echo $baseurl;?>script/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="<?php echo $baseurl;?>script/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo $baseurl;?>script/datatables/css/dataTables.bootstrap4.min.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url()?>assets/startbootstrap-sb-admin-2-gh-pages/css/sb-admin-2.min.css" rel="stylesheet">
  <link href="<?php echo $baseurl;?>script/bootstrap/dist/css/bootstrap-select.min.css" rel="stylesheet">
  <script src="<?php echo $baseurl;?>script/bootstrap/dist/js/bootstrap-select.min.js"></script>

</head>

<script type="text/javascript">
  $(document).ready(function(){
    $(".menus").hide();

    var appmenu = <?php echo json_encode($validmenu); ?>;
    $.each(appmenu, function( index, value ) {
      $("#M"+value).show();
    });
  });
</script>

<body id="page-top">
  <input type="hidden" id="txtbase" value="<?php echo $baseurl;?>">
  <input type="hidden" id="txtsite" value="<?php echo $siteurl;?>">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-image:linear-gradient(to bottom, #800000 40%, #ff3300 100%);">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="home">
        <div class="sidebar-brand-icon">
          Taruma City
        </div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        menu
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item menus <?php echo ($menu == 'Master Menu')?'active':'';?>" id="M001">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <i class="fas fa-bookmark"></i>
          <span>Master</span>
        </a>
        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Menu Master :</h6>
            <a class="collapse-item menus <?php echo ($submenu == 'Master Menu')?'active':'';?>" href="Master_Menu" id="M002">Master Menu</a>
            <a class="collapse-item menus <?php echo ($submenu == 'Master User')?'active':'';?>" href="Master_User" id="M003">Master User</a>
            <a class="collapse-item menus <?php echo ($submenu == 'Master Role')?'active':'';?>" href="Master_Role" id="M004">Master Role</a>
            <a class="collapse-item menus <?php echo ($submenu == 'Master Prefix')?'active':'';?>" href="Master_Prefix" id="M005">Master Prefix</a>
            <a class="collapse-item menus <?php echo ($submenu == 'Master News')?'active':'';?>" href="Master_News" id="M006">Master News</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item menus <?php echo ($menu == 'Master Notif')?'active':'';?>" id="M007">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-donate"></i>
          <span>Billing</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Menu Billing :</h6>
            <a class="collapse-item menus <?php echo ($submenu == 'Master Notif')?'active':'';?>" href="Data_Notif" id="M008">Data Notifikasi</a>
            <a class="collapse-item menus <?php echo ($submenu == 'Kirim Billing')?'active':'';?>" href="Kirim_Billing" id="M009">Kirim Billing</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
          Logout
        </a>
      </li>

      <li class="nav-item menus <?php echo ($menu == 'Traffic')?'active':'';?>" id="M011">
        <a class="nav-link" href="traffic">
          <i class="fas fa-parking"></i>
          <span>Info Traffic & Man Power</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">