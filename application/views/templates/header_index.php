<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reminder App</title>
    <link rel="shortcut icon" href="<?php echo base_url()?>aplico.ico">
    
    <!-- NEW PLUGIN DATETIMEPICKER FOR JQUERY MOBILE -->
    <link rel="stylesheet" href="<?php echo $baseurl;?>style/jquery.mobile-1.5.0-pre.css">
    <link rel="stylesheet" href="<?php echo $baseurl;?>assets/datetimepicker/jquery.mobile.datepicker.css" />
    <link rel="stylesheet" href="<?php echo $baseurl;?>assets/datetimepicker/jquery.mobile.datepicker.theme.css" />
    <link rel="stylesheet" href="<?php echo $baseurl;?>assets/datetimepicker/theme-template.css" />
    <script type="text/javascript" src="<?php echo $baseurl;?>script/jquery-1.10.2.js"></script>
    <script src="<?php echo $baseurl;?>assets/datetimepicker/external/jquery-ui/datepicker.js"></script>
    <script type="text/javascript" src="<?php echo $baseurl;?>script/jquery.mobile-1.5.0-pre.js"></script>
    <script src="<?php echo $baseurl;?>assets/datetimepicker/jquery.mobile.datepicker.js"></script>

    <!-- VALIDATION ENGINE -->
    <link rel="stylesheet" href="<?php echo $baseurl;?>style/jquery.validate.form.css">
    <script type="text/javascript" src="<?php echo $baseurl;?>script/jquery.validate.js?v=1"></script>

    <!-- FILE UPLOAD -->
    <script type="text/javascript" src="<?php echo $baseurl;?>assets/js/ajaxfileupload.js"></script>

    <!-- NEW FONT AWESOME - VERSION 5.9.0 - KIT VERSION -->
    <link href="<?php echo base_url()?>assets/startbootstrap-sb-admin-2-gh-pages/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- BOOTSTRAP 4.4.1 -->
    <link rel="stylesheet" href="<?php echo $baseurl;?>assets/bootstrap-4.4.1-dist/css/bootstrap.min.css">    
    <script src="<?php echo $baseurl;?>assets/bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>

    <!-- BOOTSTRAP TOUR -->
    <link href="<?php echo $baseurl;?>assets/bootstrap-tour-0.12.0/css/bootstrap-tour-standalone.min.css" rel="stylesheet">
    <script src="<?php echo $baseurl;?>assets/bootstrap-tour-0.12.0/js/bootstrap-tour-standalone.min.js"></script>
    
    <!-- FONT FOR WEBSITE -->
    <link rel="stylesheet" href="<?php echo $baseurl;?>style/uifont.css">

    <!-- OWN CSS AND JS -->
    <link rel="stylesheet" href="<?php echo $baseurl;?>style/header.css">
</head>

<body>

<input type="hidden" id="txtbase" value="<?php echo $baseurl;?>">
<input type="hidden" id="txtsite" value="<?php echo $siteurl;?>">