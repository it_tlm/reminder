<div class="container">
        <h3><?php echo $judul; ?></h3>
        <br />
        <table width="100%">
            <tr>
                <td width="50%">
                    <table>
                        <tr>
                            <td>Pengguna</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td><select name="cmbuser" class="form-control selectpicker" data-live-search="true" required>
                                    <option data-tokens="--Select User--" value="">--Pilih Pengguna--</option>
                                    <?php for($i=0;$i<count($listuser);$i++){
                                            ?><option data-tokens="<?php echo trim($listuser[$i]->usernm); ?>" value="<?php echo trim($listuser[$i]->userid); ?>"><?php echo trim($listuser[$i]->usernm); ?></option>
                                        <?php } ?>
                                </select>
                            </td>
                            <!-- <td>&nbsp;<button class="btn btn-success btnsave btnadd" id="btnsync" onclick="sync_data()"><i class="fas fa-sync"></i> Sync Data</button>&nbsp;</td> -->
                            <td>&nbsp;<button class="btn btn-success" id="btnsave" onclick="save()"><i class="far fa-save"></i> Simpan</button>&nbsp;</td>
                            <td>&nbsp;<button class="btn btn-primary btnedit" id="btnedit" onclick="edit_data()"><i class="fas fa-edit"></i> Ubah</button>&nbsp;</td>
                            <td>&nbsp;<button class="btn btn-danger"  id="btncancel" onclick="cancel()"><i class="fas fa-times"></i> Batal</button>&nbsp;</td>
                        </tr>
                    </table>
                </td>
                <td width="50%">
                    <table align="right">
                        <tr>
                            <td><button class="btn btn-info" id="btncheckall" onclick="checkall(1)"><i class="far fa-check-square"></i> Check All</button>&nbsp;</td>
                            <td>&nbsp;<button class="btn btn-warning" id="btnuncheckall" onclick="checkall(0)"><i class="far fa-window-close"></i> Uncheck All</button>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br>
        <div id="divrole"></div>
    </div>
    
 <script type="text/javascript" src="<?php echo $baseurl;?>script/bootstrap/bootstrap-validator.js"></script> 
 <script type="text/javascript" src="<?php echo $baseurl;?>script/master/master_role.js?v=<?php echo uniqid(); ?>"></script>
  </body>  
</html>