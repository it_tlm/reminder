   <div class="container">
        <h3><?php echo $judul; ?></h3>
        <br />
        <button class="btn btn-success btnadd" onclick="add_user_approve()"><i class="fas fa-plus"></i> Tambah User Approve</button>
        <button class="btn btn-default" onclick="reload_table()"><i class="fas fa-sync"></i> Reload</button>
        <br />
        <br />
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>User ID</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>PT</th>
                    <th>Departement</th>
                    <th style="width:160px;">Tindakan</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
            <th>User ID</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>PT</th>
                    <th>Departement</th>
                    <th style="width:160px;">Tindakan</th>
            </tr>
            </tfoot>
        </table>
    </div>
 <script type="text/javascript" src="<?php echo $baseurl;?>script/bootstrap/bootstrap-validator.js"></script> 
 <script type="text/javascript" src="<?php echo $baseurl;?>script/master/master_user_approve.js?v=<?php echo uniqid(); ?>"></script>
 
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" data-toggle="validator">
                
                <input type="hidden" name="acak_user_approve" id="acak_user_approve">
                    <div class="form-body">
                        <div class="form-group row" id="master_user_approve" style="display">
                            <label class="control-label col-md-3">User</label>
                            <div class="col-md-9">
                            <select name="acak_user" id="acak_user" class="form-control">
                                    <option value="">Select User</option>
                                    <?php foreach($users as $user){ ?>
                                    <option value="<?= $user->userid ?>"><?php echo $user->usernm ?></option>
                                    <?php }?>
                                </select>  
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">NAMA</label>
                            <div class="col-md-9">
                                <input name="nama_staff" id="nama_staff" placeholder="Nama Staff" class="form-control" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9">
                                <input type="email" name="email_staff" id="email_staff" placeholder="Email Staff" class="form-control" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Phone</label>
                            <div class="col-md-9">
                                <input name="phone_staff" id="phone_staff" placeholder="Phone Staff" class="form-control" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="acak_pt" class="col-sm-3 col-form-label col-form-label-sm">PT</label>
                            <div class="col-sm-9"> 
                                <select name="acak_pt" id="acak_pt" class="form-control">
                                    <option value="">Select PT</option>
                                    <?php foreach($pts->result() as $pt){ ?>
                                    <option value="<?= $pt->id_pt ?>"><?php echo $pt->nm_pt ?></option>
                                    <?php }?>
                                </select>                               
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="acak_departement" class="col-sm-3 col-form-label col-form-label-sm">Departement</label>
                            <div class="col-sm-9"> 
                            <select name="acak_departement" id="acak_departement" class="form-control">
                                <option value="">Departement</option>
                                <?php foreach($departements as $departement){ ?>
                                    <option value="<?= $departement->acak_departement ?>"><?php echo $departement->nama_departement ?></option>
                                    <?php }?>
                                </select>                               
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary"><i class="far fa-save"></i> Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
  </body>  
</html>