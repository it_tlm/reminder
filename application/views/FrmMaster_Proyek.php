
    <div class="container">
        <h3><?php echo $judul; ?></h3>
        <br />
        <?php if ($this->session->userdata("validlevel")=="9"): ?> 
            <button class="btn btn-success btnadd" onclick="add_proyek()"><i class="glyphicon glyphicon-plus"></i> Add New Proyek</button>
        <?php  endif; ?> 
        
        <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
        <br />
        <br />
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th><center>NO</center></th>
                    <th><center>NAMA PT</center></th>
                    <th><center>NAMA PROYEK</center></th>
                    <th><center>STATUS</center></th>
                    <th><center>ACTION</center></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th><center>NO</center></th>
                <th><center>NAMA PT</center></th>
                <th><center>NAMA PROYEK</center></th>
                <th><center>STATUS</center></th>
                <th><center>ACTION</center></th>
            </tr>
            </tfoot>
        </table>
    </div>
<script type="text/javascript" src="<?php echo $baseurl;?>script/bootstrap/bootstrap-validator.js"></script> 
<script type="text/javascript" src="<?php echo $baseurl;?>script/master/master_proyek.js?v=<?php echo uniqid()?>"></script>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" data-toggle="validator">
                    <div class="form-body">
                        <div id="masterpry" style="display: none;">
                            <div class="form-group row">
                                <label for="idpt" class="col-sm-3 col-form-label col-form-label-sm">ID PT</label>
                                <div class="col-sm-9">
                                    <input id="idpt" name="idpt" class="form-control form-control-sm" maxlength="5" placeholder="Max. 5 Character" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="idpry" class="col-sm-3 col-form-label col-form-label-sm">ID Proyek</label>
                                <div class="col-sm-9">
                                    <input id="idpry" name="idpry" class="form-control form-control-sm" maxlength="5" placeholder="Max. 5 Character" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>                            
                        <div class="form-group row">
                            <label for="nmpt" class="col-sm-3 col-form-label col-form-label-sm">Nama PT</label>
                            <div class="col-sm-9">
                                <input id="nmpt" name="nmpt" class="form-control form-control-sm" placeholder="Nama PT"  required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nmpry" class="col-sm-3 col-form-label col-form-label-sm">Nama Proyek</label>
                            <div class="col-sm-9">
                                <input id="nmpry" name="nmpry"class="form-control form-control-sm" placeholder="Nama Proyek" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div id="detailpry" style="display: none;">
                            <div class="form-group row">
                                <label for="ipsams" class="col-sm-3 col-form-label col-form-label-sm">Server SAMS</label>
                                <div class="col-sm-9">
                                    <input id="ipsams" name="ipsams"class="form-control form-control-sm" placeholder="IP SAMS" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="dbsams" class="col-sm-3 col-form-label col-form-label-sm">DB SAMS</label>
                                <div class="col-sm-9">
                                    <input id="dbsams" name="dbsams"class="form-control form-control-sm" placeholder="DB SAMS" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="ipsales" class="col-sm-3 col-form-label col-form-label-sm">Server SALES</label>
                                <div class="col-sm-9">
                                    <input id="ipsales" name="ipsales"class="form-control form-control-sm" placeholder="IP SALES" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="dbsales" class="col-sm-3 col-form-label col-form-label-sm">DB SALES</label>
                                <div class="col-sm-9">
                                    <input id="dbsales" name="dbsales"class="form-control form-control-sm" placeholder="DB SALES" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="status" class="col-sm-3 col-form-label col-form-label-sm">STATUS</label>
                                <div class="col-sm-9">
                                    <select id="status" name="status" class="form-control">
                                    <option value='1'>AKTIF</option>
                                    <option value='0'>TIDAK AKTIF</option></select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="statuspro" class="col-sm-3 col-form-label col-form-label-sm">TENTANG PROJECT </label>
                                <div class="col-sm-9">
                                    <select id="statuspro" name="statuspro" class="form-control">
                                    <option value='1'>AKTIF</option>
                                    <option value='0'>TIDAK AKTIF</option></select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label style="cursor: pointer;" for="imgInp" class="col-sm-3 col-form-label col-form-label-sm">Background</label>
                                <div class="col-sm-9">
                                    <input style="cursor: pointer;" type="file" id="imgInp" name="imgInp" class="form-control form-control-sm" required>
                                    <input type="hidden" class="form-control form-control-sm" readonly>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <label class="col-sm-3 col-form-label col-form-label-sm"></label>
                                <div class="col-sm-9">
                                    <img id='img-upload'/>
                                </div>
                            </div>
                        </div>                            
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary btnsave">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
  </body>  
</html>