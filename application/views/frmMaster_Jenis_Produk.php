
    <div class="container">
        <h3><?php echo $judul; ?></h3>
        <br />
        <?php if ($this->session->userdata("validlevel")=="9"): ?> 
            <button class="btn btn-success btnadd" onclick="add_jenis_produk()"><i class="glyphicon glyphicon-plus"></i> Add New Jenis Produk</button>
        <?php  endif; ?> 
        
        <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
        <br />
        <br />
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th><center>NO</center></th>
                    <th><center>NAMA JENIS PRODUK</center></th>
                    <th><center>KET JENIS PRODUK</center></th>
                    <th><center>ACTION</center></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                    <th><center>NO</center></th>
                    <th><center>NAMA JENIS PRODUK</center></th>
                    <th><center>KET JENIS PRODUK</center></th>
                    <th><center>ACTION</center></th>
            </tr>
            </tfoot>
        </table>
    </div>
<script type="text/javascript" src="<?php echo $baseurl;?>script/bootstrap/bootstrap-validator.js"></script> 
<script type="text/javascript" src="<?php echo $baseurl;?>script/master/master_jenis_produk.js?v=<?php echo uniqid()?>"></script>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" data-toggle="validator">
                    <div class="form-body">
                        <div id="master_jenis_produk" style="display">
                            <div class="form-group row">
                                <label for="acak_jenis_produk" class="col-sm-3 col-form-label col-form-label-sm">ID JENIS PRODUK</label>
                                <div class="col-sm-9">
                                    <input id="acak_jenis_produk" type="text" name="acak_jenis_produk" class="form-control form-control-sm" maxlength="5" placeholder="Max. 5 Character" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>                            
                        <div class="form-group row">
                            <label for="nama_jenis_produk" class="col-sm-3 col-form-label col-form-label-sm">Nama Jenis Produk</label>
                            <div class="col-sm-9">
                                <input id="nama_jenis_produk" name="nama_jenis_produk" class="form-control form-control-sm" placeholder="Nama Jenis Produk"  required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ket_jenis_produk" class="col-sm-3 col-form-label col-form-label-sm">Ket Jenis Produk</label>
                            <div class="col-sm-9"><textarea name="" class="form-control" placeholder="Keterangan Jenis Produk" id="ket_jenis_produk" cols="30" rows="10"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>                       
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary btnsave">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
  </body>  
</html>