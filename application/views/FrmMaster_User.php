
    <div class="container">
        <h3><?php echo $judul; ?></h3>
        <br />
        <button class="btn btn-success btnadd" onclick="add_data()"><i class="fas fa-plus"></i> Tambah Pengguna</button>
        <button class="btn btn-default" onclick="reload_table()"><i class="fas fa-sync"></i> Reload</button>
        <br />
        <br />
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Kode Pengguna</th>
                    <th>Nama</th>
                    <th>Level</th>
                    <th>Status</th>
                    <th style="width:160px;">Tindakan</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>Kode Pengguna</th>
                <th>Nama</th>
                <th>Level</th>
                <th>Status</th>
                <th>Tindakan</th>
            </tr>
            </tfoot>
        </table>
    </div>
 <script type="text/javascript" src="<?php echo $baseurl;?>script/bootstrap/bootstrap-validator.js"></script> 
 <script type="text/javascript" src="<?php echo $baseurl;?>script/master/master_user.js?v=<?php echo uniqid(); ?>"></script>
 
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" data-toggle="validator">
                    <div class="form-body">
                        <div class="form-group row">
                            <label class="control-label col-md-3">KODE PENGGUNA</label>
                            <div class="col-md-9">
                                <input name="iduser" placeholder="KODE PENGGUNA" class="form-control" type="text" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">NAMA</label>
                            <div class="col-md-9">
                                <input name="nmuser" placeholder="NAMA PENGGUNA" class="form-control" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">LEVEL</label>
                            <div class="col-md-9">
                                <input name="level" type="number" min="1" max="9" class="form-control" value="1" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="proyek" class="control-label col-md-3">PROYEK</label>
                            <div class="col-sm-9">
                                <select id="proyek" name="proyek" class="form-control" required>
                                    <option value="">--Select Proyek--</option>
                                    <?php for($i=0;$i<count($s_proyek);$i++){
                                        ?><option value="<?php echo trim($s_proyek[$i]->id_pry); ?>"><?php echo $s_proyek[$i]->nm_pry; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">STATUS</label>
                            <div class="col-md-9">
                                <select name="lock" class="form-control" required>
                                    <option value="">--Select Status--</option>
                                    <option value="0">UNLOCK</option>
                                    <option value="1">LOCK</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row" id="divpass">
                            <label class="control-label col-md-3">PASSWORD</label>
                            <div class="col-md-9">
                                <input name="password" type="password" placeholder="PASSWORD" class="form-control" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary"><i class="far fa-save"></i> Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
  </body>  
</html>