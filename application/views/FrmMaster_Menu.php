
    <div class="container">
        <h3><?php echo $judul; ?></h3>
        <br />
        <button class="btn btn-success btnadd" onclick="add_menu()"><i class="glyphicon glyphicon-plus"></i> Add New Menu</button>
        <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
        <br />
        <br />
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>URUT MENU</th>
                    <th>ID MENU</th>
                    <th>NAMA MENU</th>
                    <th>LEVEL MENU</th>
                    <th >Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>URUT MENU</th>
                <th>ID MENU</th>
                <th>NAMA MENU</th>
                <th>LEVEL MENU</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
 <script type="text/javascript" src="<?php echo $baseurl;?>script/bootstrap/bootstrap-validator.js"></script> 
 <script type="text/javascript" src="<?php echo $baseurl;?>script/master/master_menu.js?v=<?php echo uniqid()?>"></script>
 
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="index">Logout</a>
        </div>
    </div>
</div>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" data-toggle="validator">
                    <div class="form-body">
                        <div class="form-group row">
                            <label for="urutmenu" class="col-sm-3 col-form-label col-form-label-sm">NO URUTAN</label>
                            <div class="col-sm-9">
                                <input id="urutmenu" name="urutmenu" class="form-control form-control-sm" placeholder="URUTAN MENU" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="idmenu" class="col-sm-3 col-form-label col-form-label-sm">ID MENU</label>
                            <div class="col-sm-9">
                                <input id="idmenu" name="idmenu" class="form-control form-control-sm" required disabled>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nmmenu" class="col-sm-3 col-form-label col-form-label-sm">NAMA MENU</label>
                            <div class="col-sm-9">
                                <input id="nmmenu" name="nmmenu" class="form-control form-control-sm" placeholder="NAMA MENU" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="level" class="col-sm-3 col-form-label col-form-label-sm">LEVEL MENU</label>
                            <div class="col-sm-9">
                                <input id="level" name="level" type="number" min="1" max="9" value="1" class="form-control form-control-sm"  required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary btnsave">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
  </body>  
</html>