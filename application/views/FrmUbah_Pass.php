<style>

.field-icon {
    float: right;
    margin-left: -25px;
    margin-top: -25px;
    position: relative;
    z-index: 2;
}

</style>
<div class="container">
    <h3><?php echo $judul; ?></h3><br />

        
        <form action="#" id="form" class="form-horizontal" data-toggle="validator">
            <div class="form-body">
                <div class="form-group row">
                    <label class="control-label col-md-3">KODE PENGGUNA</label>
                    <div class="col-md-9">
                        <input name="iduser" placeholder="KODE PENGGUNA" class="form-control" type="text" value="<?php echo $validUser;?>" readonly="readonly" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3">NAMA</label>
                    <div class="col-md-9">
                        <input name="nmuser" placeholder="NAMA PENGGUNA" class="form-control" value="<?php echo $validNama;?>" readonly="readonly" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3">PASSWORD LAMA</label>
                    <div class="col-md-9">
                        <input name="passwordlama" type="password" placeholder="PASSWORD LAMA" class="form-control" required>
                         
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3">PASSWORD BARU</label>
                    <div class="col-md-9">
                        <input name="passwordbaru" type="password" placeholder="PASSWORD BARU" class="form-control" required>
                         
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3">CONFIRM PASSWORD BARU</label>
                    <div class="col-md-9">
                        <input name="repasswordbaru" type="password" placeholder="CONFIRM PASSWORD BARU" class="form-control" required>
                         
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div align="center">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Ubah</button>
                <button type="button" class="btn btn-danger" onclick="cancel()">Batal</button>
            </div>
        </form>
    </div>


 <script type="text/javascript" src="<?php echo $baseurl;?>script/bootstrap/bootstrap-validator.js"></script> 
 <script type="text/javascript" src="<?php echo $baseurl;?>script/changepass.js?v=<?php echo uniqid(); ?>"></script>
