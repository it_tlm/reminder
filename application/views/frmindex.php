<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Booking Online</title>

    <!-- NEW PLUGIN DATETIMEPICKER FOR JQUERY MOBILE -->
    <link rel="stylesheet" href="<?php echo $baseurl;?>style/jquery.mobile-1.5.0-pre.css">
    <link rel="stylesheet" href="<?php echo $baseurl;?>/assets/bootstrap-datepicker-1.9.0-dist/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="<?php echo $baseurl;?>assets/datetimepicker/jquery.mobile.datepicker.theme.css" />
    <link rel="stylesheet" href="<?php echo $baseurl;?>assets/datetimepicker/theme-template.css" />
    <script type="text/javascript" src="<?php echo $baseurl;?>script/jquery-1.10.2.js"></script>
    <script src="<?php echo $baseurl;?>/assets/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?php echo $baseurl;?>script/jquery.mobile-1.5.0-pre.js"></script>
    <script src="<?php echo $baseurl;?>assets/datetimepicker/jquery.mobile.datepicker.js"></script>

    <!-- VALIDATION ENGINE -->
    <link rel="stylesheet" href="<?php echo $baseurl;?>style/jquery.validate.form.css">
    <script type="text/javascript" src="<?php echo $baseurl;?>script/jquery.validate.js?v=1"></script>

    <!-- FILE UPLOAD -->
    <script type="text/javascript" src="<?php echo $baseurl;?>assets/js/ajaxfileupload.js"></script>

    <!-- NEW FONT AWESOME - VERSION 5.9.0 - KIT VERSION -->
    <script src="https://kit.fontawesome.com/f327898faa.js"></script>

    <!-- BOOTSTRAP 4.3.1 -->
    <link rel="stylesheet" href="<?php echo $baseurl;?>assets/bootstrap-4.3.1-dist/css/bootstrap.min.css">    
    <script src="<?php echo $baseurl;?>assets/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>

    <!-- BOOTSTRAP TOUR -->
    <link href="<?php echo $baseurl;?>assets/bootstrap-tour-0.12.0/css/bootstrap-tour-standalone.min.css" rel="stylesheet">
    <script src="<?php echo $baseurl;?>assets/bootstrap-tour-0.12.0/js/bootstrap-tour-standalone.min.js"></script>
    
    <!-- FONT FOR WEBSITE -->
    <link rel="stylesheet" href="<?php echo $baseurl;?>style/uifont.css">

    <!-- OWN CSS AND JS -->
    <link rel="stylesheet" href="<?php echo $baseurl;?>style/header.css">

    <link href="https://fonts.googleapis.com/css?family=Molle:400i&display=swap" rel="stylesheet">
</head>

<body>
	<input type="hidden" id="txtbase" value="<?php echo $baseurl;?>">
	<input type="hidden" id="txtsite" value="<?php echo $siteurl;?>">
	<!-- <input type="hidden" id="txtjnsuser" value="<?php echo $validJenis;?>"> -->
	
	<style type="text/css">
		body {
			font-family: 'Molle', cursive;
		}

        .bg-loading {
            /*background-image:linear-gradient(-35deg, #da251e, #1a3787, #1a3787);*/
            background-color: black;
        }

        .content-middle {
            background-color: transparent;
            border: none;
        }
	</style> 

    <div class="container-fluid bg-loading h-100">
 		<div class="row align-items-center h-100">
            <div class="col-6 mx-auto text-center text-white" style="height: 100vh">
                <div class="card h-100 border-primary justify-content-center content-middle">
                    <div >
                        <img src="<?php echo base_url()?>images/PROJECT_LOGO4.png">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="<?php echo $baseurl;?>script/frmindex.js?v=<?php echo uniqid();?>"></script>
    
</body>
</html>