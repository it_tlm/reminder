    <link href="https://fonts.googleapis.com/css?family=Work+Sans&display=swap" rel="stylesheet">
    <link href="<?php echo $baseurl;?>style/frmlogin.css" rel="stylesheet">

    <script src="<?php echo $baseurl;?>script/master/frmlogin.js"></script>

    <style type="text/css">
        #pagelogin
        {
                background-image: url('<?php echo base_url()?>images/bckground.jpg');
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
        }

        .brand {
            font-family: 'Work Sans', sans-serif;
            font-size: 26px;
        }
    </style> 

    <div data-role="page" id="pagelogin" class="container h-100 mw-100">
        <div role="main" id="pagelogin2">
            <div class="row pt-3 pb-4">
                <div class="col-6 col-md-6 col-lg-6 text-left">
                    <img src="<?php echo base_url()?>images/PROJECT_LOGO.png" width="auto" height="50px">
                </div>   
            </div>

            <div class="row align-items-center h-100">
                <div class="col-10 col-lg-5 mx-auto text-center">
                    <div class="jumbotron">
                        

                        <span class="brand">
                            Login
                        </span>

                        <form id="frmlogin" class="pt-2">
                            <div class="Input p-0 m-0 pt-4">
                                <input type="text" data-role="none" id="txtusername" class="Input-text shadow-sm bg-white" placeholder="Nama Pengguna">
                                <label for="input" class="Input-label text-dark">Nama Pengguna</label>
                            </div>

                            <div class="Input  p-0 m-0 pt-4">
                                <input type="password" data-role="none" id="txtpass" class="Input-text shadow-sm bg-white" placeholder="Kata Sandi">
                                <label for="input" class="Input-label text-dark">Kata Sandi</label>
                            </div>

                            <div class="row pt-1" style="font-size: 14px;">
                                <div class="col-12 col-sm-12 col-lg-12 text-right">
                                    <button data-role="none" class="btn btn-link text-primary" style="font-size: 13px; font-weight: bold;">Lupa kata sandi ?</button>
                                </div>
                            </div>

                            <div class="pt-4">
                                <button type="submit" data-role="none" class="btn btn-sm btn-block p-2" style="background-image:linear-gradient(-35deg, #3eca13, #2e7419, #3eca13, #2e7419); border-radius: 40px; color: #FFF;">LOGIN</button>
                            </div>

                            <div id="alertLogin" class="alert alert-danger mt-2" role="alert">
                                <i class="fas fa-times"></i> Kombinasi Nama Pengguna dan Kata Sandi anda salah
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>
    
</body>
</html>
