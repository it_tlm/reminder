var save_method; //for save method string
var table;
var siteurlzz = $("#txtsite").val();

$(document).ready(function() {
    // setRole('.btnadd',"ADD");

    //datatables
    table = $('#table').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": siteurlzz+'/master_Menu/ajax_list',
            "type": "POST"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });
});
 
function add_menu()
{   
    save_method = 'add';
    $.ajax({
        url : siteurlzz+'/master_Menu/getMax',
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            maxid = data.id_menu;
            if(maxid==null){
                maxid = 1;
            }else{
                maxid = eval(maxid)+1; 
            }
            save_method = 'add';
            $('#form')[0].reset(); // reset form on modals
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
            $('#idmenu').val(maxid);
            $('#modal_form').modal('show'); // show bootstrap modal
            $('.modal-title').text('Add New Menu'); // Set Title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get max id number');
        }
    });
}
 
function edit_Menu(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#idmenu').attr("disabled", false);
    //Ajax Load data from ajax
    $.ajax({
        url : siteurlzz+'/master_Menu/ajax_edit/' + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        { 
            $('[name="idmenu"]').val(data.id_menu);
            $('[name="nmmenu"]').val(data.nm_menu.trim());
            $('[name="urutmenu"]').val(data.urut_menu.trim());
            $('[name="level"]').val(data.level_menu);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Menu'); // Set title to Bootstrap modal title
            $('#idmenu').attr("disabled", true);
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    setRole('.btnsave',"ADD");
}
 
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function formValidator(form){
        //vaildate first
        form.validator('validate');

        var hasErr = form.find(".has-error").length;
        return hasErr;
    }
function save()
{
    if ($('#form').validator('validate').has('.has-error').length) {
        alert('Mohon lengkapi semua Data...!');
    }else{
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 
        $('#idmenu').attr("disabled", false);

        var url;
     
        if(save_method == 'add') {
            url = siteurlzz+'/master_Menu/ajax_add';
        } else {
            url = siteurlzz+'/master_Menu/ajax_update';
        }
    
        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                }
                else
                {
                    alert(data.msg);
                }
     
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable
                $('#idmenu').attr("disabled", true);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable     
            }
        });
    }
}
 
function delete_Menu(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : siteurlzz+'/master_Menu/ajax_delete/'+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                }
                else
                {
                    alert(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
    }
}