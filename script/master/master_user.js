var save_method; //for save method string
var table;
var siteurlzz = $("#txtsite").val();

$(document).ready(function() {
	// insertLog('Menu Back End Utility - Master User',navigator.userAgent);

    //datatables
    table = $('#table').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": siteurlzz+'/master_User/ajax_list',
            "type": "POST"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });

});
 
function add_data()
{
    save_method = 'add';
    $('#divpass').show();
    $('[name="password"]').prop("disabled", false);
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="iduser"]').prop("readonly", false);
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Pengguna'); // Set Title to Bootstrap modal title
}
 
function edit_User(id)
{
    save_method = 'update';
     $('#divpass').hide();
    $('[name="password"]').prop("disabled", true);
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="iduser"]').prop("readonly", true);
    //Ajax Load data from ajax
    $.ajax({
        url : siteurlzz+'/master_User/ajax_edit/' + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        { 
            $('[name="iduser"]').val(data.userid.trim());
            $('[name="nmuser"]').val(data.usernm.trim());
            $('[name="level"]').val(data.userlvl);
            $('[name="lock"]').val(data.lock);
            $('[name="proyek"]').val(data.kodeproyek.trim());
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Ubah Pengguna'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
 
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}
 function formValidator(form){
        //vaildate first
        form.validator('validate');

        var hasErr = form.find(".has-error").length;
        return hasErr;
    }
function save()
{
    if ($('#form').validator('validate').has('.has-error').length) {
        alert('Mohon lengkapi semua Data...!');
    }else{
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 
        var url;
     
        if(save_method == 'add') {
            url = siteurlzz+'/master_User/ajax_add';
        } else {
            url = siteurlzz+'/master_User/ajax_update';
        }
    
        // ajax adding data to database
        if(save_method == 'add') 
		{
            $.ajax({
                url : url,
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function(data)
                {
                    if(data.status) //if success close modal and reload ajax table
                    {
                    
                        $('#modal_form').modal('hide');
                        reload_table();
                    }
        
                    $('#btnSave').text('Simpan').addClass('glyphicon glyphicon-floppy-saved'); //change button text
                    $('#btnSave').attr('disabled',false); //set button enable 
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled',false); //set button enable 
        
                }
            });

        }else{

            $.ajax({
                url : url,
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function(data)
                {
                    if(data.status) //if success close modal and reload ajax table
                    {
                        
                        $('#modal_form').modal('hide');
                        reload_table();
                    }
        
                    $('#btnSave').text('Simpan').addClass('glyphicon glyphicon-floppy-saved'); //change button text
                    $('#btnSave').attr('disabled',false); //set button enable 
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled',false); //set button enable 
        
                }
            });

        }
    }
}
 
function delete_User(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : siteurlzz+'/master_User/ajax_delete/'+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
    }
}