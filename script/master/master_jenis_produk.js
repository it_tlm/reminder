var save_method; //for save method string
var table;
var siteurlzz = $("#txtsite").val();
var baseurlxx = $("#txtbase").val();

$(document).ready(function () {
    // setRole('.btnadd',"ADD");
    //datatables
    table = $('#table').DataTable({
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "pageLength": 25,
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": siteurlzz + '/Master_Jenis_Produk/ajax_list',
            "type": "POST"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [0, -1], //last column
                "orderable": false, //set not orderable
            },
        ],
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result).attr('class', 'thumbnail').attr('style', 'max-width:40%;');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change', "#imgInp", function () {
        readURL(this);
    });

    // $('#img-upload').on('click', function() {
    //     if(confirm('Apakah Anda akan menghapus gambar ini?'))
    //     {
    //         $('[name="imgInp"]').val("");
    //         $('#img-upload').attr('src', '');
    //     }
    // });
});

function reload_table() {
    table.ajax.reload(null, false); //reload datatable ajax 
}

function formValidator(form) {
    //vaildate first
    form.validator('validate');

    var hasErr = form.find(".has-error").length;
    return hasErr;
}

function add_jenis_produk() {
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add New Jenis Produk'); // Set Title to Bootstrap modal title
    document.getElementById("acak_jenis_produk").disabled = false;
    $.post(siteurlzz + '/Master_Jenis_Produk/ceklevel', {}, function (data) {
        if (data != '9') {
            document.getElementById("master_jenis_produk").style.display = "none";
        }
        else {
            document.getElementById("master_jenis_produk").removeAttribute("style");
        }
    });
    $('[name="statuspro"]').val("0");
}

function edit_Menu(acak_jenis_produk) {
    document.getElementById("acak_jenis_produk").disabled = true;
    $.post(siteurlzz + '/Master_Proyek/ceklevel', {}, function (data) {
        if (data != '9') {
            document.getElementById("master_jenis_produk").style.display = "none";
        }
        else {
            document.getElementById("master_jenis_produk").removeAttribute("style");
        }
    });

    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="imgInp"]').removeAttr("required");
    //Ajax Load data from ajax
    var postvarx = {
        'acak_jenis_produk': acak_jenis_produk
    };
    $.ajax({
        url: siteurlzz + '/Master_Jenis_Produk/ajax_edit/',
        type: "POST",
        data: postvarx,
        dataType: "JSON",
        success: function (data) {
            $('[name="acak_jenis_produk"]').val(data.acak_jenis_produk.trim());
            $('[name="nama_jenis_produk"]').val(data.nama_jenis_produk.trim());
            $('[id="ket_jenis_produk"]').val(data.keterangan.trim());

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Departement'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function save() {
    if ($('#form').validator('validate').has('.has-error').length) {
        alert('Mohon lengkapi semua Data...!');
    } else {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled', true); //set button disable 
        $('#acak_jenis_produk').attr("disabled", false);
        $('#idpry').attr("disabled", false);

        var url;
        if (save_method == 'add') {
            url = siteurlzz + '/Master_Jenis_Produk/ajax_add';

            var postvarx = {
                'acak_jenis_produk': $('[name="acak_jenis_produk"]').val(),
                'nama_jenis_produk': $('[name="nama_jenis_produk"]').val(),
                'ket_jenis_produk': $('[id="ket_jenis_produk"]').val(),
            };
            console.log(postvarx)

            // ajax adding data to database
            $.ajax({
                url: url,
                type: "POST",
                // data: $('#form').serialize(),
                data: postvarx,
                dataType: "JSON",
                success: function (data) {
                    if (data.status) //if success close modal and reload ajax table
                    {
                        $('#modal_form').modal('hide');
                        reload_table();
                    }
                    else {
                        alert(data.msg);
                    }

                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled', false); //set button enable 
                    $('#idpt').attr("disabled", false);
                    $('#idpry').attr("disabled", false);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error adding / update data');
                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled', false); //set button enable     
                }
            });
        } else {
            url = siteurlzz + '/Master_Jenis_Produk/ajax_update';
           
                var postvarx = {
                    'acak_jenis_produk': $('[name="acak_jenis_produk"]').val(),
                    'nama_jenis_produk': $('[name="nama_jenis_produk"]').val(),
                    'ket_jenis_produk': $('[id="ket_jenis_produk"]').val(),
                };
                // ajax adding data to database
                $.ajax({
                    url: url,
                    type: "POST",
                    // data: $('#form').serialize(),
                    data: postvarx,
                    dataType: "JSON",
                    success: function (data) {
                        if (data.status) //if success close modal and reload ajax table
                        {
                            $('#modal_form').modal('hide');
                            reload_table();
                        }
                        else {
                            alert(data.msg);
                        }

                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable 
                        $('#idpt').attr("disabled", false);
                        $('#idpry').attr("disabled", false);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error adding / update data');
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable     
                    }
                });
        }
    }
}

function delete_Menu(acak_jenis_produk) {
    if (confirm('Are you sure delete this data?')) {
        // ajax delete data to database
        var postvarx = {
            'acak_jenis_produk': acak_jenis_produk
        };
        $.ajax({
            url: siteurlzz + '/Master_Jenis_Produk/ajax_delete/',
            type: "POST",
            data: postvarx,
            dataType: "JSON",
            success: function (data) {
                //if success reload ajax table
                if (data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                }
                else {
                    alert(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error deleting data');
            }
        });

    }
}