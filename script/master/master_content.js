var save_method; //for save method string
var table;
var table_gambar;
var siteurlzz = $("#txtsite").val();
var baseurlxx = $("#txtbase").val();
var berhasil = 0;
var gagal = 0;
var form_data= new FormData();
var error='';
var filesToUpload = [];

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "pageLength": 25,
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": siteurlzz+'/master_Content/ajax_list',
            "type": "POST"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0, 1, -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });

    table_gambar = $('#table_gambar').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": siteurlzz+'/master_Content/ajax_list_gambar',
            "type": "POST",
            "data":function(d){
                d.idjdl=$('#juduldetail').val();
            }
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0, 1, 2, -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });

    table_vr = $('#table_vr').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": siteurlzz+'/master_Content/ajax_list_vr',
            "type": "POST",
            "data":function(d){
                d.idjdl=$('#judulvr').val();
            }
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0, -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });

    $("#jns").bind("change",function(){
        if($('[name="jns"]').val()=='JG001' || $('[name="jns"]').val()=='JG002' || $('[name="jns"]').val()=='JG003')
        {
            if($('[name="jns"]').val()=='JG001')
            {
                document.getElementById("kethp").removeAttribute("style");
                document.getElementById("kethp").style.color = "green";
                document.getElementById("ketlogo").style.display = "none";
                document.getElementById("ketusp").style.display = "none";
                document.getElementById("ketss").style.display = "none";
                document.getElementById("ketsuf").style.display = "none";
            }
            else
            {
                document.getElementById("kethp").style.display = "none";
                document.getElementById("ketlogo").removeAttribute("style");
                document.getElementById("ketlogo").style.color = "green";
                document.getElementById("ketusp").style.display = "none";
                document.getElementById("ketss").style.display = "none";
                document.getElementById("ketsuf").style.display = "none";
            }
            document.getElementById("gmbrsingle").removeAttribute("style");
            document.getElementById("gmbrmultiple").style.display = "none";
            document.getElementById("videourl").style.display = "none";
            document.getElementById("deskusp").style.display = "none";
            document.getElementById("jdlvr").style.display = "none";
            $('[name="imgInp"]').val("");
            $('#img-upload').attr('src', '');
            $('[name="urlvideo"]').val("");
            var $el = $('#imgInp2');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
            $(".gallery").html("");
        }
        else if($('[name="jns"]').val()=='JG005')
        {
            document.getElementById("kethp").style.display = "none";
            document.getElementById("ketlogo").style.display = "none";
            document.getElementById("ketusp").style.display = "none";
            document.getElementById("ketss").style.display = "none";
            document.getElementById("ketsuf").style.display = "none";
            document.getElementById("gmbrsingle").style.display = "none";
            document.getElementById("gmbrmultiple").style.display = "none";
            document.getElementById("videourl").removeAttribute("style");
            document.getElementById("deskusp").style.display = "none";
            document.getElementById("jdlvr").style.display = "none";
            $('[name="imgInp"]').val("");
            $('#img-upload').attr('src', '');
            $('[name="urlvideo"]').val("");
            var $el = $('#imgInp2');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
            $(".gallery").html("");
        }
        else if($('[name="jns"]').val()=='JG006')
        {
            document.getElementById("kethp").style.display = "none";
            document.getElementById("ketlogo").style.display = "none";
            document.getElementById("ketusp").removeAttribute("style");
            document.getElementById("ketusp").style.color = "green";
            document.getElementById("ketss").style.display = "none";
            document.getElementById("ketsuf").style.display = "none";
            document.getElementById("gmbrsingle").removeAttribute("style");
            document.getElementById("gmbrmultiple").style.display = "none";
            document.getElementById("videourl").style.display = "none";
            document.getElementById("deskusp").removeAttribute("style");
            document.getElementById("jdlvr").style.display = "none";
            $('[name="imgInp"]').val("");
            $('#img-upload').attr('src', '');
            $('[name="urlvideo"]').val("");
            var $el = $('#imgInp2');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
            $(".gallery").html("");
        }
        else if($('[name="jns"]').val()=='JG009')
        {
            document.getElementById("kethp").style.display = "none";
            document.getElementById("ketlogo").style.display = "none";
            document.getElementById("ketusp").style.display = "none";
            document.getElementById("ketusp").style.color = "green";
            document.getElementById("ketss").style.display = "none";
            document.getElementById("ketsuf").style.display = "none";
            document.getElementById("gmbrsingle").style.display = "none";
            document.getElementById("gmbrmultiple").style.display = "none";
            document.getElementById("videourl").removeAttribute("style");
            document.getElementById("deskusp").removeAttribute("style");
            document.getElementById("jdlvr").removeAttribute("style");
            $('[name="imgInp"]').val("");
            $('#img-upload').attr('src', '');
            $('[name="urlvideo"]').val("");
            var $el = $('#imgInp2');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
            $(".gallery").html("");
        }
        else
        {
            if($('[name="jns"]').val()=='JG004')
            {
                document.getElementById("kethp").style.display = "none";
                document.getElementById("ketlogo").style.display = "none";
                document.getElementById("ketusp").style.display = "none";
                document.getElementById("ketss").removeAttribute("style");
                document.getElementById("ketss").style.color = "green";
                document.getElementById("ketsuf").style.display = "none";
            }
            else
            {
                document.getElementById("kethp").style.display = "none";
                document.getElementById("ketlogo").style.display = "none";
                document.getElementById("ketusp").style.display = "none";
                document.getElementById("ketss").style.display = "none";
                document.getElementById("ketsuf").removeAttribute("style");
                document.getElementById("ketsuf").style.color = "green";
            }
            document.getElementById("gmbrsingle").style.display = "none";
            document.getElementById("gmbrmultiple").removeAttribute("style");
            document.getElementById("videourl").style.display = "none";
            document.getElementById("deskusp").style.display = "none";
            document.getElementById("jdlvr").style.display = "none";
            $('[name="imgInp"]').val("");
            $('#img-upload').attr('src', '');
            $('[name="urlvideo"]').val("");
            var $el = $('#imgInp2');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
            $(".gallery").html("");
        }
    });
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result).attr('class', 'thumbnail').attr('style','max-width:40%;');
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change',"#imgInp",function(){
        readURL(this);
    });

    var imagesPreview = function(input, placeToInsertImagePreview) {
        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function(event) {
                    $id = Math.random();
                    var gambar = '<img id="'+$id+'" class="thumbnail" src="'+event.target.result+'" style="max-width:20%; max-height:30%">&nbsp;&nbsp;';
                    $($.parseHTML(gambar)).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    };

    $('#img-upload').on('click', function() {
        if(confirm('Apakah Anda akan menghapus gambar ini?'))
        {
            $('[name="imgInp"]').val("");
            $('#img-upload').attr('src', '');
        }
    });

    $('div.gallery').on('click', 'img', function() {
        if(confirm('Apakah Anda akan menghapus semua gambar ini?'))
        {
            var $el = $('#imgInp2');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
            $(".gallery").html("");
        }
    });

    $('#imgInp2').on('click', function() {
        var $el = $('#imgInp2');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $(".gallery").html("");
    });

    $('#imgInp2').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });

    window.kembali=function()
    {
        $("#headercontent").show(1000);
        $("#detailcontent").hide(1000);
        $("#gambardoc").hide(1000);
    }

    window.back_gambar=function()
    {
        $("#headercontent").hide(1000);
        $("#detailcontent").show(1000);
        $("#gambardoc").hide(1000);
    }

    window.docgambar=function(ptpry,jns)
    {
        $.post(siteurlzz+'/master_Content/banyakdatagambar',{ptpry:ptpry,jns:jns},function(data){
            if (data!='GAGAL'){
                $("#headercontent").hide(1000);
                $("#detailcontent").hide(1000);
                $("#gambardoc").show(1000);
                $(".carousel-indicators").html(data);
                $.post(siteurlzz+'/master_Content/banyakgambar',{ptpry:ptpry,jns:jns},function(data){
                    if (data!='GAGAL'){
                      $(".carousel-inner").html(data);
                    }                    
                    else
                    {       
                        alert('Tidak ada gambar');
                        return false;
                    }
                });
            }
            else
            {
                alert('Tidak ada gambar');
                return false;
            }
        });
    }

    $("#imgInp2").change(function () {
        berhasil = 0;
        gagal = 0;
        var files= $('#imgInp2')[0].files;
        form_data= new FormData();
        error='';
        filesToUpload = [];
        var this_image = $(this);
        var img2 = document.getElementById('imgInp2').files;
        var img_len = img2.length;
        for(var i=0;i<img_len;i++)
        {
            var name= files[i].name;            
            var extension= name.split('.').pop().toLowerCase();
            if(jQuery.inArray(extension,['png','jpg','jpeg'])==-1){
                error +='Invalid '+ i +' Image File';
            }else{
                form_data.append("files"+"[]",files[i]);
                form_data.append("jngambar",$('[name="jns"]').val());
                form_data.append("idptgambar",$('[name="idptgambar"]').val().trim());
                form_data.append("idprygambar",$('[name="idprygambar"]').val().trim());
            }

            var this_img = document.getElementById('imgInp2').files[i];
            var reader = new FileReader();
            reader.readAsDataURL(document.getElementById('imgInp2').files[i]);
            reader.onload = function (e) 
            {
                console.log(reader);
                var image = new Image();
                image.src = e.target.result;

                //Validate the File Height and Width.
                image.onload = function () 
                {
                    if($('[name="jns"]').val()=='JG004')
                    {
                        var panjang='1350';
                        var tinggi='670';
                        if(this.width == panjang && this.height == tinggi)
                        {
                            berhasil++;
                        }
                        else
                        {
                            gagal++;
                        }
                    }
                    else
                    {
                        var panjang='600';
                        var tinggi='400';
                        if(this.width == panjang && this.height == tinggi)
                        {
                            berhasil++;
                        }
                        else
                        {
                            gagal++;
                        }
                    }
                };
            }
        }
   })

    // var form_data= new FormData();
    // var error='';
    // var filesToUpload = [];
    // $("#imgInp2").bind("change",function(){
    //     var files= $('#imgInp2')[0].files;
    //     var alldata = files.length;
    //     for(var count=0; count<alldata; count++){            
    //         var name= files[count].name;            
    //         var extension= name.split('.').pop().toLowerCase();
    //         if(jQuery.inArray(extension,['png','jpg','jpeg'])==-1){
    //             error +='Invalid '+ count +' Image File';
    //         }else{
    //             form_data.append("files"+"[]",files[count]);
    //             form_data.append("jngambar",$('[name="jns"]').val());
    //             form_data.append("idptgambar",$('[name="idptgambar"]').val().trim());
    //             form_data.append("idprygambar",$('[name="idprygambar"]').val().trim());
    //         }
    //     }
    // });

    var _URL = window.URL || window.webkitURL;
    $("#imgInp").change(function (e) {
        var file, img;
        berhasil = 0;
        gagal = 0;            
        if ((file = this.files[0])) 
        {
            cekdimensi = 'GAGAL';
            img = new Image();
            var objectUrl = _URL.createObjectURL(file);
            img.onload = function () 
            {
                if($('[name="jns"]').val()=='JG001')
                {
                    var panjang='600';
                    var tinggi='400';
                    if(this.width == panjang && this.height == tinggi)
                    {
                        berhasil = 1;
                    }
                    else
                    {
                        gagal = 1;
                    }
                }
                else if($('[name="jns"]').val()=='JG006')
                {
                    var panjang='200';
                    var tinggi='200';
                    if(this.width == panjang && this.height == tinggi)
                    {
                        berhasil = 1;
                    }
                    else
                    {
                        gagal = 1;                        
                    }                 
                }
                else
                {
                    var panjang='300';
                    var tinggi='100';
                    if(this.width == panjang && this.height == tinggi)
                    {
                        berhasil = 1;
                    }
                    else
                    {
                        gagal = 1;                        
                    }
                }
                _URL.revokeObjectURL(objectUrl);
            };
            img.src = objectUrl;
        }
    });

    window.save_gambar=function()
    {
        if ($('#form_gambar').validator('validate').has('.has-error').length) 
        {
            alert('Mohon lengkapi semua Data...!');
        }
        else
        {
            $('#btnSavedetail').text('saving...'); //change button text
            $('#btnSavedetail').attr('disabled',true); //set button disable 
            var url;
            var urlx;
            if(gagal==0 || $('[name="jns"]').val()=='JG005' || $('[name="jns"]').val()=='JG009')
            {
                if($('[name="jns"]').val()=='JG001' || $('[name="jns"]').val()=='JG002' || $('[name="jns"]').val()=='JG003'|| $('[name="jns"]').val()=='JG006')
                {
                    if($('[name="jns"]').val()=='JG006' && save_method == 'update' )
                    {
                        urlx = siteurlzz+'/master_Content/ajax_updategambarsingle';
                        var postvarx = {'idgambar': $('[name="idgambar"]').val(),
                                        'jenis': $('[name="jns"]').val(),
                                        'uspdeskid': $('[name="uspdeskid"]').val(),
                                        'uspdesken': $('[name="uspdesken"]').val()
                                    };

                        // ajax adding data to database
                        $.ajax({
                            url : urlx,
                            type: "POST",
                            data: postvarx,
                            dataType: "JSON",
                            success: function(data)
                            {
                                if(data.status) //if success close modal and reload ajax table
                                {
                                    $('#modal_form_gambar').modal('hide');
                                    $("#detailcontent").show(1000);
                                    $("#gambardoc").hide(1000);
                                    setTimeout(function()
                                    {
                                        reload_table_detail();
                                    },500);  
                                }
                                else
                                {
                                    alert(data.msg);
                                }
                                
                                document.getElementById("gmbrsingle").style.display = "none";
                                document.getElementById("deskusp").style.display = "none";
                                $('#btnSavedetail').text('save'); //change button text
                                $('#btnSavedetail').attr('disabled',false); //set button enable 
                            },
                            error: function (jqXHR, textStatus, errorThrown)
                            {
                                alert('Error adding / update data');
                                $('#btnSavedetail').text('save'); //change button text
                                $('#btnSavedetail').attr('disabled',false); //set button enable                                 
                            }
                        });
                    }
                    else
                    {
                        $.ajaxFileUpload
                        (
                            {
                                url:siteurlzz+'/master_Content/upload_file/'+$('#imgInp').prop('name')+'/'+$('[name="jns"]').val()+'/'+$('[name="idptgambar"]').val().trim()+$('[name="idprygambar"]').val().trim(),
                                secureuri:false,
                                fileElementId:'imgInp',
                                dataType: 'json',
                                success: function (data, status)
                                {
                                    if(typeof(data.msg) != 'undefined')
                                    {
                                        if(data.msg != '')
                                        {
                                            alert(data.msg);
                                            return;
                                        }
                                        else
                                        {
                                            var path_gambar = data.path_gambar;
                                            if(save_method == 'add') {
                                                urlx = siteurlzz+'/master_Content/ajax_savegambarsingle';
                                                var postvarx = {'idpt': $('[name="idptgambar"]').val(),
                                                                'idpry': $('[name="idprygambar"]').val(),
                                                                'jenis': $('[name="jns"]').val(),
                                                                'imgInp': path_gambar,
                                                                'uspdeskid': $('[name="uspdeskid"]').val(),
                                                                'uspdesken': $('[name="uspdesken"]').val()
                                                            };
                                            } 
                                            else
                                            {
                                                urlx = siteurlzz+'/master_Content/ajax_updategambarsingle';
                                                var postvarx = {'idgambar': $('[name="idgambar"]').val(),
                                                                'jenis': $('[name="jns"]').val(),
                                                                'imgInp': path_gambar,
                                                                'uspdeskid': $('[name="uspdeskid"]').val(),
                                                                'uspdesken': $('[name="uspdesken"]').val()
                                                            };       
                                            }

                                            // ajax adding data to database
                                            $.ajax({
                                                url : urlx,
                                                type: "POST",
                                                data: postvarx,
                                                dataType: "JSON",
                                                success: function(data)
                                                {
                                                    if(data.status) //if success close modal and reload ajax table
                                                    {
                                                        $('#modal_form_gambar').modal('hide');
                                                        $("#detailcontent").show(1000);
                                                        $("#gambardoc").hide(1000);
                                                        setTimeout(function()
                                                        {
                                                            reload_table_detail();
                                                        },500);  
                                                    }
                                                    else
                                                    {
                                                        alert(data.msg);
                                                    }
                                                    
                                                    document.getElementById("gmbrsingle").style.display = "none";
                                                    document.getElementById("deskusp").style.display = "none";
                                                    $('#btnSavedetail').text('save'); //change button text
                                                    $('#btnSavedetail').attr('disabled',false); //set button enable 
                                                },
                                                error: function (jqXHR, textStatus, errorThrown)
                                                {
                                                    alert('Error adding / update data');
                                                    $('#btnSavedetail').text('save'); //change button text
                                                    $('#btnSavedetail').attr('disabled',false); //set button enable                                 
                                                }
                                            });
                                        }
                                    }                                    
                                },
                                error: function (data2, status, e)
                                {
                                    alert(e);
                                }
                            }
                        )//end ajaxfileupaload
                    }
                }
                else if($('[name="jns"]').val()=='JG005')
                {
                    $('#btnSavedetail').text('saving...'); //change button text
                    $('#btnSavedetail').attr('disabled',true); //set button disable
                    var url;
                    var postvarx;
                    if(save_method == 'add') {
                        url = siteurlzz+'/master_Content/ajax_savevideo';
                        postvarx = {'idpt': $('[name="idptgambar"]').val(),
                                    'idpry': $('[name="idprygambar"]').val(),
                                    'jenis': $('[name="jns"]').val(),
                                    'urlvideo': $('[name="urlvideo"]').val()
                                    };
                    } 
                    else
                    {
                        url = siteurlzz+'/master_Content/ajax_updatevideo';
                        postvarx = {'idgambar': $('[name="idgambar"]').val(),
                                    'urlvideo': $('[name="urlvideo"]').val()
                                    };
                    }

                    // ajax adding data to database
                    $.ajax({
                        url : url,
                        type: "POST",
                        data: postvarx,
                        dataType: "JSON",
                        success: function(data)
                        {
                            if(data.status) //if success close modal and reload ajax table
                            {
                                $('#modal_form_gambar').modal('hide');
                                reload_table_detail();
                            }
                            else
                            {
                                alert(data.msg);
                            }
                            
                            document.getElementById("videourl").style.display = "none";
                            $('#btnSavedetail').text('save'); //change button text
                            $('#btnSavedetail').attr('disabled',false); //set button enable 
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            $('#btnSavedetail').text('save'); //change button text
                            $('#btnSavedetail').attr('disabled',false); //set button enable 
                 
                        }
                    });
                }
                else if($('[name="jns"]').val()=='JG009')
                {
                    $('#btnSavedetail').text('saving...'); //change button text
                    $('#btnSavedetail').attr('disabled',true); //set button disable
                    var url;
                    var postvarx;
                    if(save_method == 'add') {
                        url = siteurlzz+'/master_Content/ajax_savevr';
                        postvarx = {'idpt': $('[name="idptgambar"]').val(),
                                    'idpry': $('[name="idprygambar"]').val(),
                                    'jenis': $('[name="jns"]').val(),
                                    'urlvideo': $('[name="urlvideo"]').val(),
                                    'desc3': $('[name="uspjdlkid"]').val(),
                                    'desc4': $('[name="uspjdlen"]').val(),
                                    'desc1': $('[name="uspdeskid"]').val(),
                                    'desc2': $('[name="uspdesken"]').val()
                                    };
                    } 
                    else
                    {
                        url = siteurlzz+'/master_Content/ajax_updatevr';
                        postvarx = {'idgambar': $('[name="idgambar"]').val(),
                                    'urlvideo': $('[name="urlvideo"]').val(),
                                    'desc3': $('[name="uspjdlkid"]').val(),
                                    'desc4': $('[name="uspjdlen"]').val(),
                                    'desc1': $('[name="uspdeskid"]').val(),
                                    'desc2': $('[name="uspdesken"]').val()
                                    };
                    }

                    // ajax adding data to database
                    $.ajax({
                        url : url,
                        type: "POST",
                        data: postvarx,
                        dataType: "JSON",
                        success: function(data)
                        {
                            if(data.status) //if success close modal and reload ajax table
                            {
                                $('#modal_form_gambar').modal('hide');
                                table_vr.ajax.reload(null,false); //reload datatable ajax 
                            }
                            else
                            {
                                alert(data.msg);
                            }
                            
                            document.getElementById("videourl").style.display = "none";
                            document.getElementById("jdlvr").style.display = "none"; 
                            document.getElementById("deskusp").style.display = "none";
                            $('#btnSavedetail').text('save'); //change button text
                            $('#btnSavedetail').attr('disabled',false); //set button enable 
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            $('#btnSavedetail').text('save'); //change button text
                            $('#btnSavedetail').attr('disabled',false); //set button enable             
                        }
                    });
                }
                else
                {
                    $.ajax({
                        url:siteurlzz+'/master_Content/upload_file_multiple/',
                        method:"POST",
                        data:form_data,
                        contentType:false,
                        cache:false,
                        processData:false,
                        success:function(data){
                            var udin = new Array();
                            udin=eval(data);
                            if(udin[0]['msg']=='TRUE'){
                                alert('Berhasil Upload Gambar');
                                $('#modal_form_gambar').modal('hide');
                                setTimeout(function()
                                {
                                    reload_table_detail();
                                },500);  
                                
                            }else{
                                alert(udin[0]['msg']);
                            }

                            form_data = new FormData();
                            var $el = $('#imgInp2');
                            $el.wrap('<form>').closest('form').get(0).reset();
                            $el.unwrap();
                            $('.form-group').removeClass('has-error'); // clear error class
                            $('.help-block').empty(); // clear error string
                            $(".gallery").html("");
                            $("#idptgambar").val('');
                            $("#idprygambar").val('');                        
                        }
                    });
                    $('#btnSavedetail').text('save'); //change button text
                    $('#btnSavedetail').attr('disabled',false); //set button enable 
                } 
            }
            else
            {
                alert('Resolusi Tidak Sesuai, Perbaiki Dimensi Resolusi Gambar!');
                var $el = $('#imgInp2');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string
                $(".gallery").html("");
                $('#btnSavedetail').text('save'); //change button text
                $('#btnSavedetail').attr('disabled',false); //set button enable         
                return;
            }                     
        }
    }

    $.post(siteurlzz+'/master_Content/ceklevel',{},function(data){
        if (data!='9'){
            document.getElementById("urutan").style.display = "none";
        }
        else
        {            
            document.getElementById("urutan").removeAttribute("style");
        }
    });
});

function formValidator(form)
{
    //vaildate first
    form.validator('validate');

    var hasErr = form.find(".has-error").length;
    return hasErr;
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function reload_table_detail()
{
    table_gambar.ajax.reload(null,false); //reload datatable ajax 
}

function reload_table_vr()
{
    table_vr.ajax.reload(null,false); //reload datatable ajax 
}

function add_det(judul,ptpry,pry,pt)
{
    $("#headercontent").hide(1000);
    $("#detailcontent").show(1000);
    $('[name="juduldetail"]').text(judul);
    $('[name="juduldetail"]').val(ptpry);
    $('[name="idptgambar"]').val(pt);
    $('[name="idprygambar"]').val(pry);
    table_gambar.ajax.reload(null,false); //reload datatable ajax 
}

function edit(idpry,idpt)
{
    $('#formPref')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#btnSavecontent').text('save'); //change button text
    $('#btnSavecontent').attr('disabled',false); //set button enable 
    var postvarx = {'idpry': idpry,
                    'idpt': idpt
                    };

    //Ajax Load data from ajax
    $.ajax({
        url : siteurlzz+'/master_Content/ajax_edit/',
        type: "POST",
        data: postvarx,
        dataType: "JSON",
        success: function(data)
        {
            $('[name="idpry"]').val(data.id_pry.trim());
            $('[name="idpt"]').val(data.id_pt.trim());
            $('[name="nmpt"]').val(data.nm_pt.trim());
            $('[name="nmpry"]').val(data.nm_pry.trim());
            $('[name="status_proyek"]').val(data.status.trim());
            $('[name="tipe"]').val(data.type.trim());
            $('[name="lokasi"]').val(data.lokasi.trim());
            $('[name="gmaps"]').val(data.iframe.trim());
            $('[name="urut"]').val(data.urut.trim());
            if(data.alamat)
            {
                $('[name="alamat"]').val(CKEDITOR.instances['alamat'].setData(data.alamat.trim()));       
            }
            else
            {
                $('[name="alamat"]').val(CKEDITOR.instances['alamat'].setData());                
            }
            if(data.desc1)
            {
                $('[name="desc1_P"]').val(CKEDITOR.instances['desc1_P'].setData(data.desc1.trim()));       
            }
            else
            {
                $('[name="desc1_P"]').val(CKEDITOR.instances['desc1_P'].setData());                
            }
            if(data.desc2)
            {
                $('[name="desc2_P"]').val(CKEDITOR.instances['desc2_P'].setData(data.desc2.trim()));       
            }
            else
            {
                $('[name="desc2_P"]').val(CKEDITOR.instances['desc2_P'].setData());                
            }
            if(data.tempwa)
            {
                $('[name="desc3_P"]').val(CKEDITOR.instances['desc3_P'].setData(data.tempwa.trim()));       
            }
            else
            {
                $('[name="desc3_P"]').val(CKEDITOR.instances['desc3_P'].setData());                
            }
            // $('[name="telp"]').val(data.telp.trim());
            $('[name="telp"]').val(data.no_wa.trim());
            $('#modal_form_content').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Content'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function saveH()
{
    var messageLength = CKEDITOR.instances['alamat'].getData().replace(/<[^>]*>/gi, '').length;
    var messageLength2 = CKEDITOR.instances['desc1_P'].getData().replace(/<[^>]*>/gi, '').length;
    var messageLength3 = CKEDITOR.instances['desc2_P'].getData().replace(/<[^>]*>/gi, '').length;
    var messageLength3 = CKEDITOR.instances['desc3_P'].getData().replace(/<[^>]*>/gi, '').length;

    if ($('#formPref').validator('validate').has('.has-error').length) {
        alert('Mohon lengkapi semua Data...!');
    }
    else if(!messageLength || !messageLength2 || !messageLength3)
    {
        alert( 'Mohon lengkapi alamat / deskripsi indo / deskripsi eng' );
        return;
    }
    else
    {
        $('#btnSavecontent').text('saving...'); //change button text
        $('#btnSavecontent').attr('disabled',true); //set button disable 

        url = siteurlzz+'/master_Content/ajax_updateContent';
        var postvarx = {'idpt': $('[name="idpt"]').val(),
                        'idpry': $('[name="idpry"]').val(),
                        'status_proyek': $('[name="status_proyek"]').val(),
                        'tipe': $('[name="tipe"]').val(),
                        'lokasi': $('[name="lokasi"]').val(),
                        'desc1':CKEDITOR.instances['desc1_P'].getData(),
                        'desc2':CKEDITOR.instances['desc2_P'].getData(),
                        'alamat':CKEDITOR.instances['alamat'].getData(),
                        // 'telp': $('[name="telp"]').val(),
                        'no_wa': $('[name="telp"]').val(),
                        'iframe': $('[name="gmaps"]').val(),
                        'urut': $('[name="urut"]').val(),
                        'tempwa':CKEDITOR.instances['desc3_P'].getData()
                    };

        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: postvarx,
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form_content').modal('hide');
                    reload_table();
                }
                else
                {
                    alert(data.msg);
                }
                
                $('#btnSavecontent').text('save'); //change button text
                $('#btnSavecontent').attr('disabled',false); //set button enable 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#btnSavecontent').text('save'); //change button text
                $('#btnSavecontent').attr('disabled',false); //set button enable 
     
            }
        });
    }
}

function add_gambar()
{    
    save_method = 'add';
    $('#form_gambar')[0].reset(); // reset form on modals
    $('.help-block').empty(); // clear error string
    $('.form-group').removeClass('has-error'); // clear error removeClass
    $('#modal_form_gambar').modal('show'); // show bootstrap modal
    $('.modal-title').text('Form Gambar'); // Set Title to Bootstrap modal title
    $('#img-upload').attr('src', '').attr('title', '').attr('class', '').attr('onclick', '');
    $(".gallery").html("");
    var postvarx = {'ptpry':$('[name="idptgambar"]').val().trim()+$('[name="idprygambar"]').val().trim()};
    $.post(siteurlzz+'/master_Content/cek_jns',postvarx,function(data){
        $('#jns').html(data);
        document.getElementById("jns").disabled = false;
        document.getElementById("gmbrsingle").style.display = "none"; 
        document.getElementById("gmbrmultiple").style.display = "none"; 
        document.getElementById("videourl").style.display = "none"; 
        document.getElementById("deskusp").style.display = "none"; 
        document.getElementById("jdlvr").style.display = "none"; 
    });
}

function edit_gambar(id,jns)
{
    if(jns=='JG006')
    {
        save_method= 'add';
    }
    else
    {
        save_method = 'update';    
    }
    $('#form_gambar')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="imgInp"]').val("");
    $('#img-upload').attr('src', '');
    document.getElementById("gmbrsingle").style.display = "none"; 
    document.getElementById("gmbrmultiple").style.display = "none"; 
    document.getElementById("videourl").style.display = "none"; 
    document.getElementById("deskusp").style.display = "none"; 

    var postvarx = {'id' : id.trim()};
    $.ajax({
        url : siteurlzz+'/master_Content/ajax_edit_gambar',
        type: "POST",
        data: postvarx,
        dataType: "JSON",
        success: function(data)
        {
            $('[name="idgambar"]').val(data.id_gambar.trim());
            $.ajaxSetup({async:false});
            $.post(siteurlzz+'/master_Content/cek_jns',{'ptpry':'XXXXXXXXXX'},function(data2){
                $('#jns').html(data2);
            });
            $('[name="jns"]').val(data.jenis_gambar.trim());
            document.getElementById("jns").disabled = true;
            $.ajaxSetup({async:true});
            if(data.jenis_gambar=='JG001'||data.jenis_gambar=='JG002'||data.jenis_gambar=='JG003')
            {
                document.getElementById("gmbrsingle").removeAttribute("style");
                if(data.jenis_gambar=='JG001')
                {
                    document.getElementById("kethp").removeAttribute("style");
                    document.getElementById("kethp").style.color = "green";
                    document.getElementById("ketlogo").style.display = "none";
                    document.getElementById("ketusp").style.display = "none";
                    document.getElementById("ketss").style.display = "none";
                    document.getElementById("ketsuf").style.display = "none";
                    document.getElementById("jdlvr").style.display = "none";
                }
                else
                {
                    document.getElementById("kethp").style.display = "none";
                    document.getElementById("ketlogo").removeAttribute("style");
                    document.getElementById("ketlogo").style.color = "green";
                    document.getElementById("ketusp").style.display = "none";
                    document.getElementById("ketss").style.display = "none";
                    document.getElementById("ketsuf").style.display = "none";
                    document.getElementById("jdlvr").style.display = "none";
                }
            }
            else if(data.jenis_gambar=='JG005')
            {
                document.getElementById("kethp").style.display = "none";
                document.getElementById("ketlogo").style.display = "none";
                document.getElementById("ketusp").style.display = "none";
                document.getElementById("ketss").style.display = "none";
                document.getElementById("ketsuf").style.display = "none";
                document.getElementById("videourl").removeAttribute("style");
                $('[name="urlvideo"]').val(data.path_gambar.trim());
                document.getElementById("jdlvr").style.display = "none";
            }
            else if(data.jenis_gambar=='JG006')
            {
                document.getElementById("gmbrsingle").removeAttribute("style");
                document.getElementById("deskusp").removeAttribute("style");                
                document.getElementById("kethp").style.display = "none";
                document.getElementById("ketlogo").style.display = "none";
                document.getElementById("ketusp").removeAttribute("style");
                document.getElementById("ketusp").style.color = "green";
                document.getElementById("ketss").style.display = "none";
                document.getElementById("ketsuf").style.display = "none";
                document.getElementById("jdlvr").style.display = "none";
            }
            // else if($('[name="jns"]').val()=='JG009')
            // {
            //     document.getElementById("kethp").style.display = "none";
            //     document.getElementById("ketlogo").style.display = "none";
            //     document.getElementById("ketusp").style.display = "none";
            //     document.getElementById("ketusp").style.color = "green";
            //     document.getElementById("ketss").style.display = "none";
            //     document.getElementById("ketsuf").style.display = "none";
            //     document.getElementById("gmbrsingle").style.display = "none";
            //     document.getElementById("gmbrmultiple").style.display = "none";
            //     document.getElementById("videourl").removeAttribute("style");
            //     $('[name="urlvideo"]').val(data.path_gambar.trim());
            //     document.getElementById("deskusp").removeAttribute("style");
            //     $('[name="uspdeskid"]').val(data.desc1.trim());
            //     $('[name="uspdesken"]').val(data.desc2.trim());
            //     document.getElementById("jdlvr").removeAttribute("style");
            //     $('[name="uspjdlkid"]').val(data.desc3.trim());
            //     $('[name="uspjdlen"]').val(data.desc4.trim());
            // }
            else
            {
                document.getElementById("gmbrmultiple").removeAttribute("style");
                if(data.jenis_gambar=='JG004')
                {
                    document.getElementById("kethp").style.display = "none";
                    document.getElementById("ketlogo").style.display = "none";
                    document.getElementById("ketusp").style.display = "none";
                    document.getElementById("ketss").removeAttribute("style");
                    document.getElementById("ketss").style.color = "green";
                    document.getElementById("ketsuf").style.display = "none";
                    document.getElementById("jdlvr").style.display = "none";
                }
                else
                {
                    document.getElementById("kethp").style.display = "none";
                    document.getElementById("ketlogo").style.display = "none";
                    document.getElementById("ketusp").style.display = "none";
                    document.getElementById("ketss").style.display = "none";
                    document.getElementById("ketsuf").removeAttribute("style");
                    document.getElementById("ketsuf").style.color = "green";
                    document.getElementById("jdlvr").style.display = "none";
                }
            }

            $('#modal_form_gambar').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Form Gambar'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function editket(id,jns)
{
    save_method = 'update';
    $('#form_gambar')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="imgInp"]').val("");
    $('#img-upload').attr('src', '');
    document.getElementById("gmbrsingle").style.display = "none"; 
    document.getElementById("gmbrmultiple").style.display = "none"; 
    document.getElementById("videourl").style.display = "none"; 
    document.getElementById("deskusp").style.display = "none";

    var postvarx = {'id' : id.trim()};
    $.ajax({
        url : siteurlzz+'/master_Content/ajax_edit_gambar',
        type: "POST",
        data: postvarx,
        dataType: "JSON",
        success: function(data)
        {
            $('[name="idgambar"]').val(data.id_gambar.trim());
            var postvarx = {'ptpry':'XXXXXXXXXX'};
            $.ajaxSetup({async:false});
            $.post(siteurlzz+'/master_Content/cek_jns',postvarx,function(data){
                $('#jns').html(data);
            });
            $('[name="jns"]').val(data.jenis_gambar.trim());
            document.getElementById("jns").disabled = true;
            $.ajaxSetup({async:true});
            document.getElementById("deskusp").removeAttribute("style");
            $('[name="uspdeskid"]').val(data.desc1.trim());
            $('[name="uspdesken"]').val(data.desc2.trim());

            $('#modal_form_gambar').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Form Gambar'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function delete_gambar(ptpry,jns)
{
    if(confirm('Apakah Anda akan menghapus gambar ini ?'))
    {
        var postvarx = {'ptpry': ptpry,
                        'jns': jns,
                        };
        // ajax delete data to database
        $.ajax({
            url : siteurlzz+'/master_Content/ajax_delete_gambar/',
            type: "POST",
            data: postvarx,
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    //if success reload ajax table
                    $('#modal_form_gambar').modal('hide');
                    setTimeout(function()
                    {
                        reload_table_detail();
                    },500);  
                }
                else
                {
                    alert(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        }); 
    }
}

function hapusgambar(id_gambar)
{
    if(confirm('Apakah Anda akan menghapus gambar ini ?'))
    {
        var postvarx = {'id_gambar': id_gambar};
        // ajax delete data to database
        $.ajax({
            url : siteurlzz+'/master_Content/del/',
            type: "POST",
            data: postvarx,
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    $("#headercontent").hide(1000);
                    $("#detailcontent").show(1000);
                    $("#gambardoc").hide(1000);
                    setTimeout(function()
                    {
                        reload_table_detail();
                    },500);  
                }
                else
                {
                    alert(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
    }
}

function docvr(ptpry,jns)
{
    $("#detailcontent").hide(1000);
    $("#gambardoc").hide(1000);
    $("#docvr").show(1000);
    $('[name="judulvr"]').val(ptpry);
    // $('[name="idptgambar"]').val(pt);
    // $('[name="idprygambar"]').val(pry);
    table_vr.ajax.reload(null,false); //reload datatable ajax 
}

function back_vr()
{
    $("#detailcontent").show(1000);
    $("#gambardoc").hide(1000);
    $("#docvr").hide(1000);
    table_gambar.ajax.reload(null,false); //reload datatable ajax 
}

function add_vr(id,jns)
{
    save_method = 'add';
    $('#form_gambar')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="imgInp"]').val("");
    $('#img-upload').attr('src', '');
    $.ajaxSetup({async:false});
    $.post(siteurlzz+'/master_Content/cek_jns',{'ptpry':'XXXXXXXXXX'},function(data2){
        $('#jns').html(data2);
    });
    $('[name="jns"]').val('JG009');
    document.getElementById("jns").disabled = true;
    $.ajaxSetup({async:true});
    document.getElementById("kethp").style.display = "none";
    document.getElementById("ketlogo").style.display = "none";
    document.getElementById("ketusp").style.display = "none";
    document.getElementById("ketusp").style.color = "green";
    document.getElementById("ketss").style.display = "none";
    document.getElementById("ketsuf").style.display = "none";
    document.getElementById("gmbrsingle").style.display = "none";
    document.getElementById("gmbrmultiple").style.display = "none";
    document.getElementById("videourl").removeAttribute("style");
    document.getElementById("deskusp").removeAttribute("style");
    document.getElementById("jdlvr").removeAttribute("style");
    $('[name="imgInp"]').val("");
    $('#img-upload').attr('src', '');
    $('[name="urlvideo"]').val("");
    var $el = $('#imgInp2');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap();
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $(".gallery").html("");

    $('#modal_form_gambar').modal('show'); // show bootstrap modal when complete loaded
    $('.modal-title').text('Edit Form Gambar'); // Set title to Bootstrap modal title
}

function delete_allvr(ptpry,jns)
{
    if(confirm('Apakah Anda akan menghapus data ini ?'))
    {
        var postvarx = {'ptpry': ptpry,
                        'jns': jns,
                        };
        // ajax delete data to database
        $.ajax({
            url : siteurlzz+'/master_Content/ajax_delete_allvr/',
            type: "POST",
            data: postvarx,
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    reload_table_detail();
                }
                else
                {
                    alert(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        }); 
    }
}

function edit_vr(id,jns)
{
    save_method = 'update';
    $('#form_gambar')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="imgInp"]').val("");
    $('#img-upload').attr('src', '');
    document.getElementById("kethp").style.display = "none";
    document.getElementById("ketlogo").style.display = "none";
    document.getElementById("ketusp").style.display = "none";
    document.getElementById("ketusp").style.color = "green";
    document.getElementById("ketss").style.display = "none";
    document.getElementById("ketsuf").style.display = "none";
    document.getElementById("gmbrsingle").style.display = "none";
    document.getElementById("gmbrmultiple").style.display = "none";
    document.getElementById("videourl").removeAttribute("style");
    document.getElementById("deskusp").removeAttribute("style");
    document.getElementById("jdlvr").removeAttribute("style");
    $('[name="imgInp"]').val("");
    $('#img-upload').attr('src', '');
    $('[name="urlvideo"]').val("");
    var $el = $('#imgInp2');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap();
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $(".gallery").html("");

    var postvarx = {'id' : id.trim()};
    $.ajax({
        url : siteurlzz+'/master_Content/ajax_edit_vr',
        type: "POST",
        data: postvarx,
        dataType: "JSON",
        success: function(data)
        {
            $.ajaxSetup({async:false});
            $.post(siteurlzz+'/master_Content/cek_jns',{'ptpry':'XXXXXXXXXX'},function(data2){
                $('#jns').html(data2);
            });
            $('[name="jns"]').val('JG009');
            document.getElementById("jns").disabled = true;
            $.ajaxSetup({async:true});
            $('[name="idgambar"]').val(data.id_gambar.trim());
            $('[name="urlvideo"]').val(data.path_gambar.trim());
            $('[name="uspdeskid"]').val(data.desc1.trim());
            $('[name="uspdesken"]').val(data.desc2.trim());
            $('[name="uspjdlkid"]').val(data.desc3.trim());
            $('[name="uspjdlen"]').val(data.desc4.trim());
        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
    

    $('#modal_form_gambar').modal('show'); // show bootstrap modal when complete loaded
    $('.modal-title').text('Edit Form Gambar'); // Set title to Bootstrap modal title
}

function delete_detvr(id)
{
    if(confirm('Apakah Anda akan menghapus data ini ?'))
    {
        var postvarx = {'id': id};
        // ajax delete data to database
        $.ajax({
            url : siteurlzz+'/master_Content/ajax_delete_detvr/',
            type: "POST",
            data: postvarx,
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    reload_table_vr();
                }
                else
                {
                    alert(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        }); 
    }
}