var table;
var siteurlzz = $("#txtsite").val();


$(document).ready(function() {
	// insertLog('Menu Back End Utility - Master User',navigator.userAgent);

    // datatables
    table = $('#table').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "scrollX": true,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": siteurlzz+'/reminder/sort_reminder',
            "type": "POST",
            'data': function(data){
                // Read values
                var tgl_start_reminder = $("#tgl_start_reminder" ).val();
                var tgl_end_reminder = $("#tgl_end_reminder" ).val();
                var tgl_start_expired = $("#tgl_start_expired" ).val();
                var tgl_end_expired = $("#tgl_end_expired" ).val();
                var status = $("#status").val();
      
                // Append to data
                data.tgl_start_reminder = tgl_start_reminder;
                data.tgl_end_reminder = tgl_end_reminder;
                data.tgl_start_expired = tgl_start_expired;
                data.tgl_end_expired = tgl_end_expired;
                data.status = status;
             }
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        { 
            "targets": [ 9 ], //last column
            "orderable": false, //set not orderable
        },
        { 
            "targets": [ 10 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });

   
    function startOfWeek(date)
    {
        var diff = date.getDate() - date.getDay() + (date.getDay() === 0 ? -6 : 1);
    
        return new Date(date.setDate(diff));
    
    }

    function endOfWeek(date){
        var lastday = date.getDate() - (date.getDay() - 1) + 5;
        return new Date(date.setDate(lastday));
    }

var dt = new Date();

        $( "#tgl_start_reminder" ).datepicker({
            format: 'yyyy-mm-dd'
        });

        $( "#tgl_start_reminder" ).datepicker('setDate',startOfWeek(dt));

        $( "#tgl_end_reminder" ).datepicker({
            format: 'yyyy-mm-dd',
        });

        $( "#tgl_end_reminder" ).datepicker('setDate',endOfWeek(dt));

        $( "#tgl_start_expired" ).datepicker({
            format: 'yyyy-mm-dd',
        });

        $( "#tgl_start_expired" ).datepicker('setDate',startOfWeek(dt));

        $( "#tgl_end_expired" ).datepicker({
            format: 'yyyy-mm-dd',
        });

        $( "#tgl_end_expired" ).datepicker('setDate',endOfWeek(dt));

               

        $("#proses").click(function(){
            table.draw();
        })
 
});

function sortData(data){
   table.draw();
    // $.ajax({
    //     method:"post",
    //     data:data,
    //     url:siteurlzz+'/reminder/sort_reminder',
    //     success:function(response){
    //         console.log(response)
    //     },error:function(){
    //         alert("error")
    //     }
    // })
}



$("#export").on("click",function(){
     $.ajax({
        method:"GET",
        // data:data,
        url:siteurlzz+'/reminder/exportreminder',
        success:function(response){
            window.open(siteurlzz+'/reminder/exportreminder','_blank' );
        },error:function(){
            // alert("error")
        }
    })    
})