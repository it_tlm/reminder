var save_method; //for save method string
var table;
var siteurlzz = $("#txtsite").val();
var baseurlxx = $("#txtbase").val();

$(document).ready(function () {
    // setRole('.btnadd',"ADD");
    //datatables
    table = $('#table').DataTable({
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "pageLength": 25,
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": siteurlzz + '/Master_User_Approve/ajax_list',
            "type": "POST"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [0, -1], //last column
                "orderable": false, //set not orderable
            },
        ],
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result).attr('class', 'thumbnail').attr('style', 'max-width:40%;');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change', "#imgInp", function () {
        readURL(this);
    });

    // $('#img-upload').on('click', function() {
    //     if(confirm('Apakah Anda akan menghapus gambar ini?'))
    //     {
    //         $('[name="imgInp"]').val("");
    //         $('#img-upload').attr('src', '');
    //     }
    // });
});

function reload_table() {
    table.ajax.reload(null, false); //reload datatable ajax 
}

function formValidator(form) {
    //vaildate first
    form.validator('validate');

    var hasErr = form.find(".has-error").length;
    return hasErr;
}

function add_user_approve() {
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add New User Approve'); // Set Title to Bootstrap modal title
    document.getElementById("acak_user_approve").disabled = false;
    $.post(siteurlzz + '/Master_User_Approve/ceklevel', {}, function (data) {
        if (data != '9') {
            document.getElementById("master_user_approve").style.display = "none";
        }
        else {
            document.getElementById("master_user_approve").removeAttribute("style");
        }
    });
    $('[name="statuspro"]').val("0");
}

function edit_user_approve(acak_user_approve) {
    // document.getElementById("acak_jenis_produk").disabled = true;
    $.post(siteurlzz + '/Master_User_Approve/ceklevel', {}, function (data) {
        if (data != '9') {
            document.getElementById("master_user_approve").style.display = "none";
        }
        else {
            document.getElementById("master_user_approve").removeAttribute("style");
        }
    });

    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="imgInp"]').removeAttr("required");
    //Ajax Load data from ajax
    var postvarx = {
        'acak_user': $('[name="acak_user"]').val(),
        'nama_staff': $('[name="nama_staff"]').val(),
        'email_staff': $('[name="email_staff"]').val(),
        'phone_staff': $('[name="phone_staff"]').val(),
        'acak_departement': $('[name="acak_departement"]').val(),
        'acak_pt': $('[id="acak_pt"]').val(),
    };
    $.ajax({
        url: siteurlzz + '/Master_User_Approve/ajax_edit/'+acak_user_approve,
        type: "POST",
        data: postvarx,
        dataType: "JSON",
        success: function (data) {
            $('[name="acak_user_approve"]').val(data.acak_user_approve);
            $('[name="acak_user"]').val(data.acak_user);
            $('[name="nama_staff"]').val(data.nama_staff.trim());
            $('[name="email_staff"]').val(data.email_staff.trim());
            $('[name="phone_staff"]').val(data.phone_staff.trim());
            $('[name="acak_departement"]').val(data.acak_departement);
            $('[name="acak_pt"]').val(data.acak_pt);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit User Approve'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function save() {
    if ($('#form').validator('validate').has('.has-error').length) {
        alert('Mohon lengkapi semua Data...!');
    } else {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled', true); //set button disable 
        $('#acak_jenis_produk').attr("disabled", false);
        $('#idpry').attr("disabled", false);

        var url;
        if (save_method == 'add') {
            url = siteurlzz + '/Master_User_Approve/ajax_add';

            var postvarx = {
                'acak_user': $('[name="acak_user"]').val(),
                'nama_staff': $('[name="nama_staff"]').val(),
                'email_staff': $('[name="email_staff"]').val(),
                'phone_staff': $('[name="phone_staff"]').val(),
                'acak_departement': $('[name="acak_departement"]').val(),
                'acak_pt': $('[id="acak_pt"]').val(),
            };

            // ajax adding data to database
            $.ajax({
                url: url,
                type: "POST",
                // data: $('#form').serialize(),
                data: postvarx,
                dataType: "JSON",
                success: function (data) {
                    if (data.status) //if success close modal and reload ajax table
                    {
                        $('#modal_form').modal('hide');
                        reload_table();
                    }
                    else {
                        alert(data.msg);
                    }

                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled', false); //set button enable 
                    $('#idpt').attr("disabled", false);
                    $('#idpry').attr("disabled", false);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error adding / update data');
                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled', false); //set button enable     
                }
            });
        } else {
            url = siteurlzz + '/Master_User_Approve/ajax_update';
           
                var postvarx = {
                    'acak_user_approve':$('[name="acak_user_approve"]').val(),
                    'acak_user': $('[name="acak_user"]').val(),
                    'nama_staff': $('[name="nama_staff"]').val(),
                    'email_staff': $('[name="email_staff"]').val(),
                    'phone_staff': $('[name="phone_staff"]').val(),
                    'acak_departement': $('[name="acak_departement"]').val(),
                    'acak_pt': $('[id="acak_pt"]').val(),
                };
                // ajax adding data to database
                $.ajax({
                    url: url,
                    type: "POST",
                    // data: $('#form').serialize(),
                    data: postvarx,
                    dataType: "JSON",
                    success: function (data) {
                        if (data.status) //if success close modal and reload ajax table
                        {
                            $('#modal_form').modal('hide');
                            reload_table();
                        }
                        else {
                            alert(data.msg);
                        }

                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable 
                        $('#idpt').attr("disabled", false);
                        $('#idpry').attr("disabled", false);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error adding / update data');
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable     
                    }
                });
        }
    }
}

function delete_user_approve(acak_user_approve) {
    if (confirm('Are you sure delete this data?')) {
        // ajax delete data to database
        var postvarx = {
            'acak_user_approve': acak_user_approve
        };
        $.ajax({
            url: siteurlzz + '/Master_User_Approve/ajax_delete/'+acak_user_approve,
            type: "POST",
            data: postvarx,
            dataType: "JSON",
            success: function (data) {
                //if success reload ajax table
                if (data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                }
                else {
                    alert(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error deleting data');
            }
        });

    }
}