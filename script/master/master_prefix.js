var save_method; //for save method string
var table;
var siteurlzz = $("#txtsite").val();
var baseurlxx = $("#txtbase").val();
var berhasil = 0;
var gagal = 0;

$(document).ready(function() {
 
    //datatables
    table = $('#table').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": siteurlzz+'/master_Prefix/ajax_list',
            "type": "POST",
            "data": function(d){
                 d.idx = $('[name="idprefix"]').val().trim();
              }
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });

    table_ = $('#table_').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": siteurlzz+'/master_Prefix/ajax_list2',
            "type": "POST",
            "data": function(d){
                d.idx = $('[name="idprefix"]').val().trim();
            }
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });

    $('[name="idprefix"]').change(function () {
        table.ajax.reload(null,false); //reload datatable ajax 
        table_.ajax.reload(null,false); //reload datatable ajax 
    });

    
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function (e) {
				$('#img-upload').attr('src', e.target.result).attr('class', 'thumbnail').attr('style','max-width:40%;');
			}
			
			reader.readAsDataURL(input.files[0]);
		}
	}

	$(document).on('change',"#imgInp",function(){
		readURL(this);
    });

    var _URL = window.URL || window.webkitURL;
    $("#imgInp").change(function (e) {
        var file, img;
        berhasil = 0;
        gagal = 0;            
        if ((file = this.files[0])) 
        {
            cekdimensi = 'GAGAL';
            img = new Image();
            var objectUrl = _URL.createObjectURL(file);
            img.onload = function () 
            {
                if($('[name="idprefix"]').val().trim()=='HP')
                {
                    var panjang='1350';
                    var tinggi='660';
                    if(this.width == panjang && this.height == tinggi)
                    {
                        berhasil = 1;
                    }
                    else
                    {
                        gagal = 1;
                    }
                }
                else
                {                    
                    gagal = 0;    
                }
                _URL.revokeObjectURL(objectUrl);
            };
            img.src = objectUrl;
        }
    });

});

function getval(sel)
{
    var idH = $('[name="idprefix"]').val().trim();
    var id = (sel.value);
    var i = id + ',' + idH;

    $.ajax({
        url : siteurlzz+'/master_Prefix/checkBahasa/' + id+'/'+idH,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            var arrData = new Array();                                                                                 
				arrData = eval(data);
				if(arrData.length > 0)
				{
                    for(var i=0;i<arrData.length;i++)
					{
                        alert("Pilihan "+arrData[i]['nm_prefixd'].trim()+" sudah ada, silahkan pilih kembali..")
                    }
                    
                    // alert("Pilihan Bahasa Sudah Ada");
                    $('[name="idbhs"]').val();
                    $('#idbhs option[value=""]').prop('selected', true);
                    $('[name="idbhs"]').selectpicker("refresh");

                }
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });

}

function setCmbPrefix(valuee)
{
    $.ajax({
            url : siteurlzz+'/master_Prefix/listH',
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                var setval = '<option value="">--Select Prefix--</option>';
                for(var i=0;i<data.length;i++)
                {
                    setval = setval + '<option value="'+data[i]['id_prefix'].trim()+'">'+data[i]['nm_prefix'].trim()+'</option>';
                }
                $('[name="idprefix"]').html(setval);
                $('[name="idprefix"]').val(valuee.trim());
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error load data Prefix');
            }
        });
}

function addPref()
{
    save_method = 'add';
    $('#formPref')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="idprefixH"]').prop("readonly", false);
    $('#modal_form_pref').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add New Prefix'); // Set Title to Bootstrap modal title
}

function EditPref()
{
    var id = $('[name="idprefix"]').val().trim();
    if(id=='')
    {
        alert("Silahkan pilih data Prefix terlebih dahulu...!");
        return;
    }
    else
    {
        save_method = 'update';
        $('#formPref')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('[name="idprefixH"]').prop("readonly", true);
        //Ajax Load data from ajax
        $.ajax({
            url : siteurlzz+'/master_Prefix/ajax_edit_prefix/' + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
     
                $('[name="idprefixH"]').val(data.id_prefix.trim());
                $('[name="nmprefixH"]').val(data.nm_prefix.trim());
                $('#modal_form_pref').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Prefix'); // Set title to Bootstrap modal title
     
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }        
}

function DeletePref()
{
    var id = $('[name="idprefix"]').val().trim();
    if(id=='')
    {
        alert("Silahkan pilih data Prefix terlebih dahulu...!");
        return;
    }
    else
    {
        if(confirm('Are you sure delete this data?'))
        {
            // ajax delete data to database
            $.ajax({
                url : siteurlzz+'/master_Prefix/ajax_delete_pref/'+id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {

                    if(data.status) //if success close modal and reload ajax table
                    {
                        //if success reload ajax table
                        setCmbPrefix("");
                        reload_table();
                    }
                    else
                    {
                        alert(data.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });     
        }
    }
}
 
function add()
{
    if($('[name="idprefix"]').val().trim()=='')
    {
        alert("Silahkan pilih data Prefix terlebih dahulu...!");
        return
    }
    else{
    	if($('[name="idprefix"]').val().trim()=='HP')
    	{
    		document.getElementById("kethp").removeAttribute("style");
    		document.getElementById("kethp").style.color = "green";

    	}
    	else
    	{
    		document.getElementById("kethp").style.display = "none";
    	}
        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('[name="idprefixd"]').prop("readonly", false);
        $('[name="idx"]').val( $('[name="idprefix"]').val().trim());
        $('#idbhs option[value=""]').prop('selected', true);
        // $('[name="imgInp"]').attr("required","");
        $('#img-upload').attr('src', '');
        $('#img-upload').attr('src', '').attr('title', '').attr('class', '').attr('onclick', '');
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add New Detail Prefix'); // Set Title to Bootstrap modal title
    }
}

function add_prefix_text()
{
    if($('[name="idprefix"]').val().trim()=='')
    {
        alert("Silahkan pilih data Prefix terlebih dahulu...!");
        return
    }
    else{
        save_method = 'add';
        $('#form_prefix_text')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('[name="desc1_P"]').attr("required","");
	    $('[name="desc1_P"]').val(CKEDITOR.instances['desc1_P'].setData());
        $('[name="idprefixdd"]').prop("readonly", false);
        $('[name="idx"]').val( $('[name="idprefix"]').val().trim());
        $('#modal_form_prefix_text').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add New Prefix Text'); // Set Title to Bootstrap modal title
    }
}

function edit(id)
{
	if($('[name="idprefix"]').val().trim()=='HP')
	{
		document.getElementById("kethp").removeAttribute("style");
		document.getElementById("kethp").style.color = "green";

	}
	else
	{
		document.getElementById("kethp").style.display = "none";
	}
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="idprefixd"]').prop("readonly", true);
    $('[name="idx"]').val( $('[name="idprefix"]').val().trim());
    //Ajax Load data from ajax
    $.ajax({
        url : siteurlzz+'/master_Prefix/ajax_edit/' + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 
            $('[name="idprefixd"]').val(data.id_prefixd.trim());
            $('[name="nmprefixd"]').val(data.nm_prefixd.trim());
            $('[name="desc1"]').val(data.isi1.trim());
            $('[name="desc2"]').val(data.isi2.trim());
            // $('[name="desc3"]').val(data.isi3.trim());
            // $('[name="imgInp"]').removeAttr("required");
            if(data.isi3.trim() == "-")
            {
                $('#img-upload').attr('style', 'display:none');
            }else{
                $('#img-upload').attr('src', baseurlxx+data.isi3.trim()).attr('style', 'cursor:pointer;max-width:40%').attr('title','Lihat Gambar').attr('class','img-thumbnail').attr('onclick','window.open(this.src)');
            }
            // $('[name="urut"]').val(data.urut.trim());
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Detail Prefix'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function edit2(id)
{
    save_method = 'update';
    $('#form_prefix_text')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="idprefixdd"]').prop("readonly", true);
    $('[name="idx"]').val( $('[name="idprefix"]').val().trim());
    //Ajax Load data from ajax
    $.ajax({
        url : siteurlzz+'/master_Prefix/ajax_edit2/' + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="idprefixdd"]').val(data.id_prefixdd.trim());
            $('[name="idbhs"]').val(data.id_bhs.trim());
            $('[name="desc1_P"]').val(CKEDITOR.instances['desc1_P'].setData(data.isi1.trim()));
            $('[name="desc2_P"]').val(data.isi2.trim());
            // $('[name="urut_P"]').val(data.urut.trim());
            $('#modal_form_prefix_text').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Prefix Bahasa'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
 
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function reload_table2()
{
    table_.ajax.reload(null,false); //reload datatable ajax 
}

function formValidator(form)
{
    //vaildate first
    form.validator('validate');

    var hasErr = form.find(".has-error").length;
    return hasErr;
}

function saveH()
{
    if ($('#formPref').validator('validate').has('.has-error').length) {
        alert('Mohon lengkapi semua Data...!');
    }else{
        $('#btnSavePref').text('saving...'); //change button text
        $('#btnSavePref').attr('disabled',true); //set button disable 
        var url;
     
        if(save_method == 'add') {
            url = siteurlzz+'/master_Prefix/ajax_addPref';
        } else {
            url = siteurlzz+'/master_Prefix/ajax_updatePref';
        }
    
        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#formPref').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    setCmbPrefix($('[name="idprefixH"]').val().trim());
                    $('#modal_form_pref').modal('hide');
                    setTimeout(function()
                    {
                        reload_table();
                        reload_table2();
                    },500);                    
                }
                else
                {
                    alert(data.msg);
                }
                
                $('#btnSavePref').text('save'); //change button text
                $('#btnSavePref').attr('disabled',false); //set button enable 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#btnSavePref').text('save'); //change button text
                $('#btnSavePref').attr('disabled',false); //set button enable 
     
            }
        });
    }
}

function save()
{ 
    if ($('#form').validator('validate').has('.has-error').length) {
        alert('Mohon lengkapi semua Data...!');
    }else{
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 
        var url;
    	if(gagal!=0)
    	{
    		alert('Resolusi Tidak Sesuai, Perbaiki Dimensi Resolusi Gambar!');
    		$('[name="imgInp"]').val("");
            $('#img-upload').attr('src', '');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
            return;
    	}
    	else
    	{
    		if(save_method == 'add') {
	            url = siteurlzz+'/master_Prefix/ajax_add';
	        } else {
	            url = siteurlzz+'/master_Prefix/ajax_update';
	        }
	    
	        if(save_method == 'add') 
	        {
	            // ajax adding data to database

	            if($('[name="imgInp"]').val().trim()== "")//gaa ada upload edit
				{
	                var postvarx = {'idx':$('[name="idx"]').val(),
	                                'idprefixd':$('[name="idprefixd"]').val(),
	                                'nmprefixd':$('[name="nmprefixd"]').val(),
	                                'desc1':$('[name="desc1"]').val(),
	                                'desc2':$('[name="desc2"]').val(),
	                                // 'desc3':$('[name="desc3"]').val(),
	                                'imgInp':'-',
	                                'urut':$('[name="urut"]').val()
	                            };

					$.ajax({
						url : url,
						type: "POST",
						data: postvarx,
						dataType: "JSON",
	                    success: function(data)
	                    {
	                        if(data.status) //if success close modal and reload ajax table
	                        {
	                            $('#modal_form').modal('hide');
	                            reload_table(); 
	                        }
	                        else
	                        {
	                            alert(data.msg);
	                        }
	            
	                        $('#btnSave').text('save'); //change button text
	                        $('#btnSave').attr('disabled',false); //set button enable 
	                    },
	                    error: function (jqXHR, textStatus, errorThrown)
	                    {
	                        alert('Error adding / update data');
	                        $('#btnSave').text('save'); //change button text
	                        $('#btnSave').attr('disabled',false); //set button enable 
	            
	                    }
					});
				}
				else  // ada uppload edit
				{
	                $.ajaxFileUpload
	                (
	                    {
	                        url:siteurlzz+'/master_Prefix/upload_file/'+$('#imgInp').prop('name')+'/'+$('[name="idx"]').val(),
	                        secureuri:false,
	                        fileElementId:'imgInp',
	                        dataType: 'json',
	                        success: function (data, status)
	                        {
	                            if(typeof(data.msg) != 'undefined')
	                            {
	                                if(data.msg != '')
	                                {
	                                    alert(data.msg);
	                                }
	                                else
	                                {
	                                    var path_gambar = data.path_gambar;
	                                            
	                                    var postvarx = {'idx':$('[name="idx"]').val(),
	                                                    'idprefixd':$('[name="idprefixd"]').val(),
	                                                    'nmprefixd':$('[name="nmprefixd"]').val(),
	                                                    'desc1':$('[name="desc1"]').val(),
	                                                    'desc2':$('[name="desc2"]').val(),
	                                                    // 'desc3':$('[name="desc3"]').val(),
	                                                    'imgInp':path_gambar,
	                                                    'urut':$('[name="urut"]').val()
	                                                };
	                                    // ajax adding data to database
	                                    
	                                    $.ajax({
	                                        url : url,
	                                        type: "POST",
	                                        data:  postvarx,
	                                        dataType: "JSON",
	                                        success: function(data)
	                                        {
	                                            if(data.status) //if success close modal and reload ajax table
	                                            {
	                                                $('#modal_form').modal('hide');
	                                                reload_table(); 
	                                            }
	                                            else
	                                            {
	                                                alert(data.msg);
	                                            }
	                                
	                                            $('#btnSave').text('save'); //change button text
	                                            $('#btnSave').attr('disabled',false); //set button enable 
	                                        },
	                                        error: function (jqXHR, textStatus, errorThrown)
	                                        {
	                                            alert('Error adding / update data');
	                                            $('#btnSave').text('save'); //change button text
	                                            $('#btnSave').attr('disabled',false); //set button enable 
	                                
	                                        }
	                                    });							
	                                }
	                            }
	                                    
	                        },
	                        error: function (data2, status, e)
	                        {
	                            alert(e);
	                        }
	                    }
	                )
	            }


	        }else{

	            //update
	            // ajax adding data to database
	            if($('[name="imgInp"]').val().trim()== "")//gaa ada upload edit
				{
	                var postvarx = {'idx':$('[name="idx"]').val(),
	                                'idprefixd':$('[name="idprefixd"]').val(),
	                                'nmprefixd':$('[name="nmprefixd"]').val(),
	                                'desc1':$('[name="desc1"]').val(),
	                                'desc2':$('[name="desc2"]').val(),
	                                'desc3':$('[name="desc3"]').val(),
	                                'imgInp':'-',
	                                // 'urut':$('[name="urut"]').val()
	                            };

					$.ajax({
						url : url,
						type: "POST",
						data: postvarx,
						dataType: "JSON",
	                    success: function(data)
	                    {
	                        if(data.status) //if success close modal and reload ajax table
	                        {
	                            $('#modal_form').modal('hide');
	                            reload_table(); 
	                        }
	                        else
	                        {
	                            alert(data.msg);
	                        }
	            
	                        $('#btnSave').text('save'); //change button text
	                        $('#btnSave').attr('disabled',false); //set button enable 
	                    },
	                    error: function (jqXHR, textStatus, errorThrown)
	                    {
	                        alert('Error adding / update data');
	                        $('#btnSave').text('save'); //change button text
	                        $('#btnSave').attr('disabled',false); //set button enable 
	            
	                    }
					});
				}
				else  // ada uppload edit
				{
					//alert("A");
					$.ajaxFileUpload
					(
						{
							url:siteurlzz+'/master_Prefix/upload_file/'+$('#imgInp').prop('name')+'/'+$('[name="idx"]').val(),
							secureuri:false,
							fileElementId:'imgInp',
							dataType: 'json',
							success: function (data, status)
							{
								if(typeof(data.msg) != 'undefined')
								{
									if(data.msg != '')
									{
										alert(data.msg);
									}
									else
									{
										var path_gambar = data.path_gambar;
										
										//upload masterplan
										
	                                    var postvarx = {'idx':$('[name="idx"]').val(),
	                                                    'idprefixd':$('[name="idprefixd"]').val(),
	                                                    'nmprefixd':$('[name="nmprefixd"]').val(),
	                                                    'desc1':$('[name="desc1"]').val(),
	                                                    'desc2':$('[name="desc2"]').val(),
	                                                    // 'desc3':$('[name="desc3"]').val(),
	                                                    'imgInp':path_gambar,
	                                                    'urut':$('[name="urut"]').val()
	                                                };
										$.ajax({
											url : url,
											type: "POST",
											data: postvarx,
											dataType: "JSON",
	                                        success: function(data)
	                                        {
	                                            if(data.status) //if success close modal and reload ajax table
	                                            {
	                                                $('#modal_form').modal('hide');
	                                                reload_table(); 
	                                            }
	                                            else
	                                            {
	                                                alert(data.msg);
	                                            }
	                                
	                                            $('#btnSave').text('save'); //change button text
	                                            $('#btnSave').attr('disabled',false); //set button enable 
	                                        },
	                                        error: function (jqXHR, textStatus, errorThrown)
	                                        {
	                                            alert('Error adding / update data');
	                                            $('#btnSave').text('save'); //change button text
	                                            $('#btnSave').attr('disabled',false); //set button enable 
	                                
	                                        }
										});
										
									}
								}
										
							},
							error: function (data2, status, e)
							{
								alert(e);
							}
						}
					)//end ajaxfileupaload
				}
	        }
    	}	               
    }
}

function save_prefix_text()
{
    var messageLength = CKEDITOR.instances['desc1_P'].getData().replace(/<[^>]*>/gi, '').length;

    if ($('#form_prefix_text').validator('validate').has('.has-error').length) {
        alert('Mohon lengkapi semua Data...!');
        
    }else if(!messageLength){
		alert( 'Mohon lengkapi deskripsi..' );
		return
	}else{
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 
        var url;
     
        if(save_method == 'add') {
            url = siteurlzz+'/master_Prefix/ajax_add2';
        } else {
            url = siteurlzz+'/master_Prefix/ajax_update2';
        }


        if(save_method == 'add') 
		{
            var postvarx = {'idx':$('[name="idx"]').val(),
							'idprefixdd':$('[name="idprefixdd"]').val(),
							'idbhs':$('[name="idbhs"]').val(),
							'desc1_P':CKEDITOR.instances['desc1_P'].getData(),
							'desc2_P':$('[name="desc2_P"]').val(),
							// 'urut_P':$('[name="urut_P"]').val()
							};
            // ajax adding data to database
            $.ajax({
                url : url,
                type: "POST",
                data: postvarx,
                dataType: "JSON",
                success: function(data)
                {
                    if(data.status) //if success close modal and reload ajax table
                    {
                        $('#modal_form_prefix_text').modal('hide');
                        reload_table2(); 
                    }
                    else
                    {
                        alert(data.msg);
                    }
        
                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled',false); //set button enable 
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled',false); //set button enable 
        
                }
            });
        }
        else
        {
            var postvarx = {'idx':$('[name="idx"]').val(),
            'idprefixdd':$('[name="idprefixdd"]').val(),
            'idbhs':$('[name="idbhs"]').val(),
            'desc1_P':CKEDITOR.instances['desc1_P'].getData(),
            'desc2_P':$('[name="desc2_P"]').val(),
            // 'urut_P':$('[name="urut_P"]').val()
            };
            // ajax adding data to database
            $.ajax({
            url : url,
            type: "POST",
            data: postvarx,
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form_prefix_text').modal('hide');
                    reload_table2(); 
                }
                else
                {
                    alert(data.msg);
                }

                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 

            }
            });
        }
    }
}

function delete_data(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : siteurlzz+'/master_Prefix/ajax_delete/'+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    reload_table();
                }
                else
                {
                    alert(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
    }
}

function delete_data2(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : siteurlzz+'/master_Prefix/ajax_delete2/'+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    //if success reload ajax table
                    $('#modal_form_prefix_text').modal('hide');
                    reload_table2(); 
                }
                else
                {
                    alert(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
    }
}