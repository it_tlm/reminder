
$(document).ready(function(){
    var siteurl = $('#txtsite').val().trim();
    var baseurl = $('#txtbase').val().trim();

    $("#alertLogin").hide();

    //LOGIN SYSTEM
    $('#frmlogin').on('submit', function(event) {
        event.preventDefault();
        
        var id = $("#txtusername").val().trim();
        var pass = $("#txtpass").val().trim();
        
        if(id == "" || pass == "")
        {
            $("#alertLogin").show();
            return;
        }

        var postvars = {'id':id, 'pass':pass};
        $.post(siteurl+'/login/userlogin', postvars, function(data){
            if(data.STATUS)
            {
                if (typeof(Storage) !== "undefined") {
                    window.location = siteurl+'/home';
                } else {
                    alert('Browser anda tidak mendukung Storage Support');
                }
            }
            else
            {
                alert(data.MSG);
                // $("#alertLogin").show();
               return
            }               
        }, "json");
    });
});