var save_method; //for save method string
var table;
var siteurlzz = $("#txtsite").val();
var baseurlxx = $("#txtbase").val();

$(document).ready(function() {
    // setRole('.btnadd',"ADD");
    //datatables
    table = $('#table').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "pageLength": 25,
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": siteurlzz+'/Master_Proyek/ajax_list',
            "type": "POST"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0, -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result).attr('class', 'thumbnail').attr('style','max-width:40%;');
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change',"#imgInp",function(){
        readURL(this);
    });

    // $('#img-upload').on('click', function() {
    //     if(confirm('Apakah Anda akan menghapus gambar ini?'))
    //     {
    //         $('[name="imgInp"]').val("");
    //         $('#img-upload').attr('src', '');
    //     }
    // });
});
 
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function formValidator(form)
{
    //vaildate first
    form.validator('validate');

    var hasErr = form.find(".has-error").length;
    return hasErr;
}
 
function add_proyek()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add New Proyek'); // Set Title to Bootstrap modal title
    $('[name="imgInp"]').val("");
    $('#img-upload').attr('src', '').attr('title', '').attr('class', '').attr('onclick', '');
    document.getElementById("idpt").disabled = false;
    document.getElementById("idpry").disabled = false;
    $.post(siteurlzz+'/Master_Proyek/ceklevel',{},function(data){
        if (data!='9'){
            document.getElementById("masterpry").style.display = "none";
            document.getElementById("detailpry").style.display = "none";
        }
        else
        {            
            document.getElementById("masterpry").removeAttribute("style");
            document.getElementById("detailpry").removeAttribute("style");
        }
    });
    $('[name="statuspro"]').val("0");
}
 
function edit_Menu(idpry,idpt)
{
    document.getElementById("idpt").disabled = true;
    document.getElementById("idpry").disabled = true;
    $.post(siteurlzz+'/Master_Proyek/ceklevel',{},function(data){
        if (data!='9'){
            document.getElementById("masterpry").style.display = "none";
            document.getElementById("detailpry").style.display = "none";
        }
        else
        {            
            document.getElementById("masterpry").removeAttribute("style");
            document.getElementById("detailpry").removeAttribute("style");
        }
    });

    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="imgInp"]').removeAttr("required");
    //Ajax Load data from ajax
    var postvarx = {'idpry': idpry,
                    'idpt': idpt                    
                    };
    $.ajax({
        url : siteurlzz+'/Master_Proyek/ajax_edit/',
        type: "POST",
        data: postvarx,
        dataType: "JSON",
        success: function(data)
        { 
            $('[name="idpt"]').val(data.id_pt.trim());
            $('[name="nmpt"]').val(data.nm_pt.trim());
            $('[name="idpry"]').val(data.id_pry.trim());
            $('[name="nmpry"]').val(data.nm_pry.trim());
            $('[name="ipsams"]').val(data.svr_sams.trim());
            $('[name="dbsams"]').val(data.dt_sams.trim());
            $('[name="ipsales"]').val(data.svr_sales.trim());
            $('[name="dbsales"]').val(data.dt_sales.trim());
            $('[name="status"]').val(data.aktif);
            $('[name="statuspro"]').val(data.flagproject);
            if(data.path_gambar.trim() == "-")
            {
                $('#img-upload').attr('style', 'display:none');
            }else{
                $('#img-upload').attr('src', baseurlxx+data.path_gambar.trim()).attr('style', 'cursor:pointer;max-width:40%').attr('title','Lihat Gambar').attr('class','img-thumbnail').attr('onclick','window.open(this.src)');
            }
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Proyek'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function save()
{
    if ($('#form').validator('validate').has('.has-error').length) {
        alert('Mohon lengkapi semua Data...!');
    }else{
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 
        $('#idpt').attr("disabled", false);
        $('#idpry').attr("disabled", false);

        var url;
        if(save_method == 'add') {
            url = siteurlzz+'/Master_Proyek/ajax_add';
            if($('[name="imgInp"]').val().trim()== "")//gaa ada upload edit
            {
                var postvarx = {'idpt':$('[name="idpt"]').val(),
                                'nmpt':$('[name="nmpt"]').val(),
                                'idpry':$('[name="idpry"]').val(),
                                'nmpry':$('[name="nmpry"]').val(),
                                'ipsams':$('[name="ipsams"]').val(),
                                'dbsams':$('[name="dbsams"]').val(),
                                'ipsales':$('[name="ipsales"]').val(),
                                'dbsales':$('[name="dbsales"]').val(),
                                'status':$('[name="status"]').val(),
				    'statuspro':$('[name="statuspro"]').val(),

                                'imgInp':'-'
                            };
    
                // ajax adding data to database
                $.ajax({
                    url : url,
                    type: "POST",
                    // data: $('#form').serialize(),
                    data: postvarx,
                    dataType: "JSON",
                    success: function(data)
                    {
                        if(data.status) //if success close modal and reload ajax table
                        {
                            $('#modal_form').modal('hide');
                            reload_table();
                        }
                        else
                        {
                            alert(data.msg);
                        }
             
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled',false); //set button enable 
                        $('#idpt').attr("disabled", false);
                        $('#idpry').attr("disabled", false);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error adding / update data');
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled',false); //set button enable     
                    }
                });
            }
            else
            {
                $.ajaxFileUpload
                    (
                        {
                            url:siteurlzz+'/Master_Proyek/upload_file/'+$('#imgInp').prop('name')+'/'+$('[name="idpt"]').val().trim()+$('[name="idpry"]').val().trim(),
                            secureuri:false,
                            fileElementId:'imgInp',
                            dataType: 'json',
                            success: function (data, status)
                            {
                                if(typeof(data.msg) != 'undefined')
                                {
                                    if(data.msg != '')
                                    {
                                        alert(data.msg);
                                    }
                                    else
                                    {
                                        var path_gambar = data.path_gambar;
                                        var postvarx = {'idpt':$('[name="idpt"]').val(),
                                                        'nmpt':$('[name="nmpt"]').val(),
                                                        'idpry':$('[name="idpry"]').val(),
                                                        'nmpry':$('[name="nmpry"]').val(),
                                                        'ipsams':$('[name="ipsams"]').val(),
                                                        'dbsams':$('[name="dbsams"]').val(),
                                                        'ipsales':$('[name="ipsales"]').val(),
                                                        'dbsales':$('[name="dbsales"]').val(),
                                                        'status':$('[name="status"]').val(),
								'statuspro':$('[name="statuspro"]').val(),
                                                        'imgInp':path_gambar
                                                    };
    
                                        // ajax adding data to database
                                        $.ajax({
                                            url : url,
                                            type: "POST",
                                            // data: $('#form').serialize(),
                                            data: postvarx,
                                            dataType: "JSON",
                                            success: function(data)
                                            {
                                                if(data.status) //if success close modal and reload ajax table
                                                {
                                                    $('#modal_form').modal('hide');
                                                    reload_table();
                                                }
                                                else
                                                {
                                                    alert(data.msg);
                                                }
                                     
                                                $('#btnSave').text('save'); //change button text
                                                $('#btnSave').attr('disabled',false); //set button enable 
                                                $('#idpt').attr("disabled", false);
                                                $('#idpry').attr("disabled", false);
                                            },
                                            error: function (jqXHR, textStatus, errorThrown)
                                            {
                                                alert('Error adding / update data');
                                                $('#btnSave').text('save'); //change button text
                                                $('#btnSave').attr('disabled',false); //set button enable     
                                            }
                                        });
                                    }
                                }
                                        
                            },
                            error: function (data2, status, e)
                            {
                                alert(e);
                            }
                        }
                    )
            }
        } else {
            url = siteurlzz+'/Master_Proyek/ajax_update';
            if($('[name="imgInp"]').val().trim()== "")//gaa ada upload edit
            {
                var postvarx = {'idpt':$('[name="idpt"]').val(),
                                'nmpt':$('[name="nmpt"]').val(),
                                'idpry':$('[name="idpry"]').val(),
                                'nmpry':$('[name="nmpry"]').val(),
                                'ipsams':$('[name="ipsams"]').val(),
                                'dbsams':$('[name="dbsams"]').val(),
                                'ipsales':$('[name="ipsales"]').val(),
                                'dbsales':$('[name="dbsales"]').val(),
                                'status':$('[name="status"]').val(),
				                'statuspro':$('[name="statuspro"]').val(),
                                'imgInp':'-'
                            };
    
                // ajax adding data to database
                $.ajax({
                    url : url,
                    type: "POST",
                    // data: $('#form').serialize(),
                    data: postvarx,
                    dataType: "JSON",
                    success: function(data)
                    {
                        if(data.status) //if success close modal and reload ajax table
                        {
                            $('#modal_form').modal('hide');
                            reload_table();
                        }
                        else
                        {
                            alert(data.msg);
                        }
             
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled',false); //set button enable 
                        $('#idpt').attr("disabled", false);
                        $('#idpry').attr("disabled", false);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error adding / update data');
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled',false); //set button enable     
                    }
                });
            }
            else
            {
                $.ajaxFileUpload
                    (
                        {
                            url:siteurlzz+'/Master_Proyek/upload_file/'+$('#imgInp').prop('name')+'/'+$('[name="idpt"]').val().trim()+$('[name="idpry"]').val().trim(),
                            secureuri:false,
                            fileElementId:'imgInp',
                            dataType: 'json',
                            success: function (data, status)
                            {
                                if(typeof(data.msg) != 'undefined')
                                {
                                    if(data.msg != '')
                                    {
                                        alert(data.msg);
                                    }
                                    else
                                    {
                                        var path_gambar = data.path_gambar;
                                        var postvarx = {'idpt':$('[name="idpt"]').val(),
                                                        'nmpt':$('[name="nmpt"]').val(),
                                                        'idpry':$('[name="idpry"]').val(),
                                                        'nmpry':$('[name="nmpry"]').val(),
                                                        'ipsams':$('[name="ipsams"]').val(),
                                                        'dbsams':$('[name="dbsams"]').val(),
                                                        'ipsales':$('[name="ipsales"]').val(),
                                                        'dbsales':$('[name="dbsales"]').val(),
                                                        'status':$('[name="status"]').val(),
                                                        'statuspro':$('[name="statuspro"]').val(), 
                                                        'imgInp':path_gambar
                                                    };
    
                                        // ajax adding data to database
                                        $.ajax({
                                            url : url,
                                            type: "POST",
                                            // data: $('#form').serialize(),
                                            data: postvarx,
                                            dataType: "JSON",
                                            success: function(data)
                                            {
                                                if(data.status) //if success close modal and reload ajax table
                                                {
                                                    $('#modal_form').modal('hide');
                                                    reload_table();
                                                }
                                                else
                                                {
                                                    alert(data.msg);
                                                }
                                     
                                                $('#btnSave').text('save'); //change button text
                                                $('#btnSave').attr('disabled',false); //set button enable 
                                                $('#idpt').attr("disabled", false);
                                                $('#idpry').attr("disabled", false);
                                            },
                                            error: function (jqXHR, textStatus, errorThrown)
                                            {
                                                alert('Error adding / update data');
                                                $('#btnSave').text('save'); //change button text
                                                $('#btnSave').attr('disabled',false); //set button enable     
                                            }
                                        });
                                    }
                                }
                                        
                            },
                            error: function (data2, status, e)
                            {
                                alert(e);
                            }
                        }
                    )
            }
        }
    }
}
 
function delete_Menu(idpry,idpt)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        var postvarx = {'idpry': idpry,
                        'idpt': idpt                    
                        };
        $.ajax({
            url : siteurlzz+'/Master_Proyek/ajax_delete/',
            type: "POST",
            data: postvarx,
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                }
                else
                {
                    alert(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
    }
}