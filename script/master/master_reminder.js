var save_method; //for save method string
var table;
var siteurlzz = $("#txtsite").val();
var dt = new Date();

$(document).ready(function() {
	// insertLog('Menu Back End Utility - Master User',navigator.userAgent);

    // datatables
    table = $('#table').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "scrollX": true,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": siteurlzz+'/reminder/ajax_list',
            "type": "POST"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        { 
            "targets": [ 9 ], //last column
            "orderable": false, //set not orderable
        },
        { 
            "targets": [ 10 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });

        $("#tgl_reminder").on("change",function(){
            $("#tgl_berlaku_dokumen").val(formatDate(plusDate(new Date($(this).val()),$("#masa_berlaku").val())))
        });

        $("#tgl_reminder").datepicker({
            format: 'yyyy-mm-dd',
        });

        $("#tgl_berlaku_dokumen").val(formatDate(plusDate(new Date($("#tgl_reminder").val()),$("#masa_berlaku").val())))

        $("#masa_berlaku").on("keyup",function(){
            var exp = $(event.target).val();
            var tgl_rem = new Date($("#tgl_reminder").val());
            var res_dt = plusDate(tgl_rem,exp);
            $( "#tgl_berlaku_dokumen" ).val(formatDate(res_dt));
        })

        
        
        
    $("#acak_jenis_produk").on("change",function(){
        var acak_jenis_produk = $("#acak_jenis_produk").val();
        getJenisDokumenByAcakProduk(acak_jenis_produk,null)
    });

    
// upload dokumen
// $("#dokumen").change(function(){
//     file = $("#dokumen").prop('files')[0];
//     form_data = new FormData($('#form')[0]);
//     form_data.append('dokumen',file);
//     $.ajax({
//         method:"POST",
//         data:form_data,
//         url:siteurlzz+'/reminder/test',
//         cache: false,
//         contentType: false,
//         processData: false,
//         success:function(data){
//             console.log(data)
//         },
//         error:function(err){
//             alert("error")
//         }
//     })
// })

});



function plusDate(rem_date,exprd){
    if(exprd == 0 || exprd == ""){
        tmp_exprd = 0;
    }else{
        tmp_exprd = exprd;
    }
    return new Date(rem_date.setDate(rem_date.getDate() + parseInt(tmp_exprd)));
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

function addDate(date){

}

function getJenisDokumenByAcakProduk(acak_jenis_produk,acak_jenis_dokumen){
    option = "<option value=''>Select Jenis Dokumen</option>";
    $.ajax({
        url:siteurlzz + '/Master_jenis_produk/getJenisDokumenByAcakProduk/',
        method:"GET",
        data:{'acak_jenis_produk':acak_jenis_produk},
        success:function(response){
            val = JSON.parse(response);
            for(key in val){
                if(acak_jenis_dokumen  == val[key].acak_jenis_dok){
                    option += "<option value="+val[key].acak_jenis_dok+" selected>"+val[key].nama_jenis_dok+"</option>"
                }else{
                    option += "<option value="+val[key].acak_jenis_dok+">"+val[key].nama_jenis_dok+"</option>"
                }
            }
            $("#acak_jenis_dokumen").html(option)
            // document.getElementById("acak_jenis_dok").innerHtml = option;
            
        },
        error:function(error){
            console.log(error)
        }
    })
    $("#acak_jenis_dokumen").innerHtml = option;
}

function add_data()
{
    save_method = 'add';
    $('#divpass').show();
    // $('[name="password"]').prop("disabled", false);
    $('#form')[0].reset(); // reset form on modals
    $("#tgl_berlaku_dokumen").val(formatDate(plusDate(new Date($("#tgl_reminder").val()),$("#masa_berlaku").val())))
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    // $('[name="iduser"]').prop("readonly", false);
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Reminder'); // Set Title to Bootstrap modal title
}
 
function edit_reminder(id)
{
    save_method = 'update';
     $('#divpass').hide();
    // $('[name="acak_reminder"]').prop("disabled", true);
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="acakreminder"]').prop("readonly", true);
    //Ajax Load data from ajax
    $.ajax({
        url : siteurlzz+'/reminder/ajax_edit/' + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        { 
            getJenisDokumenByAcakProduk(data.acak_jenis_produk,data.acak_jenis_dokumen);


            $('[name="acak_reminder"]').val(data.acak_reminder.trim());
            $('[name="no_dokumen"]').val(data.no_dokumen.trim());
            $('[name="acak_departemen"]').val(data.acak_departemen.trim());
            $('[name="tgl_reminder"]').val(data.tgl_reminder);
            $('[name="masa_berlaku"]').val(data.masa_berlaku);
            $('[name="acak_proyek"]').val(data.acak_proyek.trim());
            $('[name="tgl_berlaku"]').val(data.tgl_berlaku);
            $('[name="acak_jenis_produk"]').val(data.acak_jenis_produk);
            $('[name="acak_jenis_dokumen"]').val(data.acak_jenis_dokumen);
            $('[name="keterangan"]').val(data.keterangan);
            $('[name="acak_admin"]').val(data.acak_admin);
            $('[name="acak_proyek"]').val(data.acak_proyek);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Reminder'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
 
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}


 function formValidator(form){
        //vaildate first
        form.validator('validate');

        var hasErr = form.find(".has-error").length;
        return hasErr;
    }


function save()
{
    if ($('#form').validator('validate').has('.has-error').length) {
        alert('Mohon lengkapi semua Data...!');
    }else{
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 
        var url;
     
        if(save_method == 'add') {
            url = siteurlzz+'/reminder/ajax_add';
        } else {
            url = siteurlzz+'/reminder/ajax_update';
        }
        

        // handle file
        file = $("#dokumen").prop('files')[0];
        form_data = new FormData($('#form')[0]);
        form_data.append('dokumen',file);

        // handle form
        tmp_data = {};
        // ajax adding data to database
        if(save_method == 'add') 
		{
            $.ajax({
                url : url,
                type: "POST",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                dataType: "JSON",
                success: function(data)
                {
                    if(data.status) //if success close modal and reload ajax table
                    {
                    
                        $('#modal_form').modal('hide');
                        reload_table();
                    }else{
                        $("#no_dokumen").next().html(data.no_dokumen).css({"color":"red"})
                    }
        
                    $('#btnSave').text('Simpan').addClass('glyphicon glyphicon-floppy-saved'); //change button text
                    $('#btnSave').attr('disabled',false); //set button enable 
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                   
                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled',false); //set button enable 
        
                }
            });

        }else{

            $.ajax({
                url : url,
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function(data)
                {
                    if(data.status) //if success close modal and reload ajax table
                    {
                        
                        $('#modal_form').modal('hide');
                        reload_table();
                    }
        
                    $('#btnSave').text('Simpan').addClass('glyphicon glyphicon-floppy-saved'); //change button text
                    $('#btnSave').attr('disabled',false); //set button enable 
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled',false); //set button enable 
        
                }
            });

        }
    }
}

function approve_pic(id){
    if(confirm('Anda Yakin Akan Mengaprove data ini?'))
    {
        $.ajax({
            url : siteurlzz+'/reminder/ajax_approve_pic/'+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status){
                    reload_table();
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error approve data');
            }
        })
    }
}

function approve_admin(id){
    if(confirm('Anda Yakin Akan Mengaprove data ini?'))
    {
        $.ajax({
            url : siteurlzz+'/reminder/ajax_approve_admin/'+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status){
                    reload_table();
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error approve data');
            }
        })
    }
}
 
// function delete_User(id)
// {
//     if(confirm('Are you sure delete this data?'))
//     {
//         // ajax delete data to database
//         $.ajax({
//             url : siteurlzz+'/master_User/ajax_delete/'+id,
//             type: "POST",
//             dataType: "JSON",
//             success: function(data)
//             {
//                 //if success reload ajax table
//                 $('#modal_form').modal('hide');
//                 reload_table();
//             },
//             error: function (jqXHR, textStatus, errorThrown)
//             {
//                 alert('Error deleting data');
//             }
//         });
 
//     }
// }