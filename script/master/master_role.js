var status; //for save method string
var table;
var siteurlzz = $("#txtsite").val();
$(document).ready(function() {
	// insertLog('Menu Back End Utility - Master Role User',navigator.userAgent);
    

     $('[name="cmbuser"]').change(function () {
         getDataRole();
    });
    getDataRole();
    setButton(1);

    
});

function setButton(param){
    status = param;
    if(param==1){
        $('#btnsave').hide();
        $('#btncancel').hide();
        $('#btncheckall').hide();
        $('#btnuncheckall').hide();
        $('#btnedit').show();
        $('select[name="cmbuser"]').attr("disabled", false);
    }else if(param==2){
        $('#btnsave').show();
        $('#btncancel').show();
        $('#btncheckall').show();
        $('#btnuncheckall').show();
        $('#btnedit').hide();
        $('select[name="cmbuser"]').attr("disabled", true);
    }
    $('.selectpicker').selectpicker('refresh');
}

function checkall(param){
    var icon = '<i class="fas fa-times" style="color:red"></i>';
    if(param==1){
        var icon = '<i class="fas fa-check" style="color:green"></i>';
    }
    
    var count_elements = $('.clslist').length;
    for(var i=0;i<count_elements;i++){
        var index = i+1;
        $('#read'+index).val(param);
        $('#spanread'+index).html(icon);
        $('#add'+index).val(param);
        $('#spanadd'+index).html(icon);
        $('#write'+index).val(param);
        $('#spanwrite'+index).html(icon);
        $('#delete'+index).val(param);
        $('#spandelete'+index).html(icon);
    }   
}

function click_r(index,id_name){
    if(status==1){
        
        alert("Silahkan tekan tombol ubah terlebih dahulu...!");
        return
    }
    var val = $('#'+id_name+index).val();
    var ok = '<i class="fas fa-check" style="color:green"></i>';
    var rem = '<i class="fas fa-times" style="color:red"></i>';
    if(val==0){
        $('#'+id_name+index).val("1");
        $('#span'+id_name+index).html(ok);
    }else{
        $('#'+id_name+index).val("0");
        $('#span'+id_name+index).html(rem);
    }
}

function edit_data(){
    var userid = $('[name="cmbuser"]').val();
    if(userid==''){
        alert("Pilih data user terlebih dahulu...!");
    }else{
        setButton(2);
    }
    
}

function sync_data(){


    if(confirm('Apakah yakin ingin melanjutkan proses Sync Data Menu?')){
        var postvars = {'param':''};
        $.post(siteurlzz+'/master_Role/proses_SyncM',postvars,function(data){
            if(data=="OK"){
                getDataRole();
                alert("Data sudah selesai diproses..!");
            }
            
        });
    }
}

function cancel(){
    setButton(1);
    getDataRole();
}

function save(){

    
    if(confirm('Are you sure save this all data?'))
    {
        var userid = $('[name="cmbuser"]').val();
        var array_final = new Array();
        var count_elements = $('.clslist').length;
        if(count_elements>0){
            for(var i=0;i<count_elements;i++){
                var index = String(i+1);
                var idm = $('#idm'+index).val();
                var read = $('#read'+index).val();
                var add = $('#add'+index).val();
                var write = $('#write'+index).val();
                var delete_ = $('#delete'+index).val();
                var array_temp = {'idm':idm,'read':read,'add':add,'write':write,'delete_':delete_};
                array_final[i] = array_temp;
            }
            //alert(array_final[0]['nominal']);
            var postvars = {'data':JSON.stringify(array_final),"userid":userid};
            $.post(siteurlzz+'/master_Role/save_all',postvars,function(data){
                if(data=="OK"){
                    setButton(1);
                    getDataRole();
                    alert("Data berhasil disimpan semua...!");
                }else
                {
                    alert("Anda tidak memiliki akses untuk proses ini...!");   
                }
            });
        }else{
            alert("Tidak ada data yang akan disimpan...!");
        } 
    }
}

function getDataRole()
{
    var userid = $('[name="cmbuser"]').val();
    if(userid==''){
        $('#divrole').html("");
    }else{
        $.ajax({
            url : siteurlzz+'/master_Role/listRole/'+userid,
            type: "POST",
            success: function(data)
            {
                //if success reload ajax table
                $('#divrole').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error load data Role');
            }
        });
    }
}