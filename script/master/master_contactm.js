var save_method; //for save method string
var table;
var siteurlzz = $("#txtsite").val();
var baseurlxx = $("#txtbase").val();

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": siteurlzz+'/Master_ContactM/ajax_list',
            "type": "POST",
            "data": function(d){
                 d.idx = $('[name="idprefixd"]').selectpicker('val');
              }
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });

    $('[name="idprefixd"]').change(function () {
        table.ajax.reload(null,false); //reload datatable ajax 
    });


});

function getval(sel)
{
    var idH = $('[name="idprefixd"]').val().trim();
    var id = (sel.value);
    var i = id + ',' + idH;

    $.ajax({
        url : siteurlzz+'/Master_ContactM/checkBahasa/' + id+'/'+idH,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            var arrData = new Array();                                                                                 
				arrData = eval(data);
				if(arrData.length > 0)
				{
                    for(var i=0;i<arrData.length;i++)
					{
                        alert("Pilihan "+arrData[i]['nm_prefixd'].trim()+" sudah ada, silahkan pilih kembali..")
                    }
                    
                    // alert("Pilihan Bahasa Sudah Ada");
                    $('[name="idbhs"]').val();
                    $('#idbhs option[value=""]').prop('selected', true);
                    $('[name="idbhs"]').selectpicker("refresh");

                }
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });

}

function add_prefix_text()
{
    if($('[name="idprefixd"]').val() =='')
    {
        alert("Silahkan pilih data Prefix terlebih dahulu...!");
        return
    }
    else{
        save_method = 'add';
        $('#form_prefix_text')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('[name="idprefixdd"]').prop("readonly", false);
        $('[name="idx"]').val($('[name="idprefixd"]').val().trim());
        $('#modal_form_prefix_text').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add New Prefix Text'); // Set Title to Bootstrap modal title
    }
}

function edit(id)
{
    save_method = 'update';
    $('#form_prefix_text')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[name="idprefix"]').prop("readonly", true);
    $('[name="idx"]').val( $('[name="idprefix"]').val());
    //Ajax Load data from ajax
    $.ajax({
        url : siteurlzz+'/Master_ContactM/ajax_edit/' + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="idprefixdd"]').val(data.id_prefixdd.trim());
            $('[name="idbhs"]').val(data.id_bhs.trim());
            $('[name="desc1_P"]').val(data.isi1.trim());
            $('[name="desc2_P"]').val(data.isi2.trim());
            $('#modal_form_prefix_text').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Prefix Bahasa'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
 
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function formValidator(form)
{
    //vaildate first
    form.validator('validate');

    var hasErr = form.find(".has-error").length;
    return hasErr;
}

function save_prefix_text()
{
    if ($('#form_prefix_text').validator('validate').has('.has-error').length) {
        alert('Mohon lengkapi semua Data...!');
        
	}else{
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 
        var url;
        
        if(save_method == 'add') {
            url = siteurlzz+'/Master_ContactM/ajax_add';
        } else {
            url = siteurlzz+'/Master_ContactM/ajax_update';
        }


        if(save_method == 'add') 
		{
            var postvarx = {'idx':$('[name="idx"]').val(),
							'idprefixdd':$('[name="idprefixdd"]').val(),
							'idbhs':$('[name="idbhs"]').val(),
							'desc1_P':$('[name="desc1_P"]').val(),
                            'desc2_P':$('[name="desc2_P"]').val()
							};
            // ajax adding data to database
            $.ajax({
                url : url,
                type: "POST",
                data: postvarx,
                dataType: "JSON",
                success: function(data)
                {
                    if(data.status) //if success close modal and reload ajax table
                    {
                        $('#modal_form_prefix_text').modal('hide');
                        reload_table(); 
                    }
                    else
                    {
                        alert(data.msg);
                    }
        
                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled',false); //set button enable 
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled',false); //set button enable 
        
                }
            });
        }
        else
        {
            var postvarx = {'idx':$('[name="idx"]').val(),
            'idprefixdd':$('[name="idprefixdd"]').val(),
            'idbhs':$('[name="idbhs"]').val(),
            'desc1_P':$('[name="desc1_P"]').val(),
            'desc2_P':$('[name="desc2_P"]').val()
            };
            // ajax adding data to database
            $.ajax({
            url : url,
            type: "POST",
            data: postvarx,
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form_prefix_text').modal('hide');
                    reload_table(); 
                }
                else
                {
                    alert(data.msg);
                }

                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 

            }
            });
        }
    }
}

function delete_data(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : siteurlzz+'/Master_ContactM/ajax_delete/'+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    //if success reload ajax table
                    $('#modal_form_prefix_text').modal('hide');
                    reload_table(); 
                }
                else
                {
                    alert(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
    }
}