var siteurlzz = $("#txtsite").val();


$(document).ready(function(){

    $(".toggle-password1").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $('[name="passwordlama"]');
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    $(".toggle-password2").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $('[name="passwordbaru"]');
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    $(".toggle-password3").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $('[name="repasswordbaru"]');
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
});

function save()
{
    if ($('#form').validator('validate').has('.has-error').length) {
        alert('Mohon lengkapi semua Data...!');
    }else{
        if($('[name="passwordbaru"]').val()!=$('[name="repasswordbaru"]').val()){
            alert("Password baru harus sama dengan confirm password baru...!");
            return
        }
        $('#btnSave').text('changing...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 
        var url;
     
        url = siteurlzz+'/Ubah_Pass/ajax_change';
    
        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            success: function(data)
            {
                if(data=="OK") //if success close modal and reload ajax table
                {
                    alert("Password berhasil diubah...!");
                    window.location.replace(siteurlzz+'/Login');

                }else if(data=="FAILED_PASSAWAL"){
                    alert("Password lama salah...!");
                }else if(data=="FAILED_UPDATE"){
                    alert("Update password gagal...!");
                }
                $('#btnSave').text('change'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#btnSave').text('change'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
     
            }
        });
    }
}
function cancel(){
    window.location.replace(siteurlzz+'/Home');
}